import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import * as sip from "sip.js";
import {
    Web,
    Invitation,
    Inviter,
    InviterOptions,
    Referral,
    Registerer,
    RegistererOptions,
    Session,
    SessionState,
    UserAgent,
    UserAgentOptions,
    InvitationAcceptOptions
} from "sip.js";



@Injectable({
    providedIn: 'root'
})
export class SipService {




    private callsHistory: Array<any> = [];
    public callsHistory$: BehaviorSubject<any> = new BehaviorSubject<any>([]);


    private _status: any = {
        in_call: false,
        mute: false,
        hold: false,
        registered: false,
        online: false,
    }
    public status$: BehaviorSubject<any> = new BehaviorSubject<any>(this._status);


    private _slots: Array<any> = [
        {
            name: 'Slot 1',
            type: null,
            session: null,
            did: null,
            status: 'empty',
            duration: {
                ringStart: null,
                ringEnd: null,
                callStart: null,
                callEnd: null,
            },
            mute: false,
            hold: false,
            answered: false,
            media_element: null
        },
        {
            name: 'Slot 2',
            type: null,
            session: null,
            did: null,
            status: 'empty',
            duration: {
                ringStart: null,
                ringEnd: null,
                callStart: null,
                callEnd: null,
            },
            mute: false,
            hold: false,
            answered: false,
            media_element: null
        },
        {
            name: 'Slot 3',
            type: null,
            session: null,
            did: null,
            status: 'empty',
            duration: {
                ringStart: null,
                ringEnd: null,
                callStart: null,
                callEnd: null,
            },
            mute: false,
            hold: false,
            answered: false,
            media_element: null
        }
    ];
    public slots$: BehaviorSubject<any> = new BehaviorSubject<any>(this._slots);
    public currentSlot: any = 0;


    private userAgent: UserAgent;
    private registerer: sip.Registerer;


    constructor() {
        this.loadAudio();
        if (localStorage.getItem('history')) {
            this.callsHistory = JSON.parse(localStorage.getItem('history') || '[]');
            this.callsHistory$.next(this.callsHistory);
        }
        this.InitConfiguration();
    }

    setRingStart(session_id: any, date: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                this._slots[index].duration.ringStart = date;
            }
        }
    }

    setRingEnd(session_id: any, date: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                if (this._slots[index].answered) {
                    return;
                }
                this._slots[index].duration.ringEnd = date;
            }
        }
    }

    holdOthers(session_id: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session != session_id) {
                if (this._slots[index].answered) {
                    this.holdSlot(this._slots[index].session)
                }
            }
        }
    }


    setCallStart(session_id: any, date: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                this._slots[index].duration.callStart = date;
                this._slots[index].answered = true;
            }
        }
    }

    setCallEnd(session_id: any, date: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                if (!this._slots[index].answered) {
                    return;
                }
                this._slots[index].duration.callEnd = date;
            }
        }
    }


    muteSlot(session_id: any) {
        let session: any = this.userAgent._sessions[session_id];
        if (!(session instanceof Session)) {
            throw new Error("Invalid Sesstion");
        }

        if (!(session.sessionDescriptionHandler)) {
            throw new Error("Session description handler undefined.");
        }

        if (!(session.sessionDescriptionHandler instanceof Web.SessionDescriptionHandler)) {
            throw new Error("Session's session description handler not instance of Web SessionDescriptionHandler.");
        }

        if (!session.sessionDescriptionHandler.peerConnection) {
            throw new Error("Peer connection closed.");
        }

        session.sessionDescriptionHandler.peerConnection.getSenders().forEach((sender) => {
            if (sender.track) {
                sender.track.enabled = false;
            }
        });
        this.updateSlotAttribute(session_id, 'mute', true);
    }

    unMuteSlot(session_id: any) {
        let session: any = this.userAgent._sessions[session_id];
        if (!(session instanceof Session)) {
            throw new Error("Invalid Sesstion");
        }

        if (!(session.sessionDescriptionHandler)) {
            throw new Error("Session description handler undefined.");
        }

        if (!(session.sessionDescriptionHandler instanceof Web.SessionDescriptionHandler)) {
            throw new Error("Session's session description handler not instance of Web SessionDescriptionHandler.");
        }

        if (!session.sessionDescriptionHandler.peerConnection) {
            throw new Error("Peer connection closed.");
        }

        session.sessionDescriptionHandler.peerConnection.getSenders().forEach((sender) => {
            if (sender.track) {
                sender.track.enabled = true;
            }
        });
        this.updateSlotAttribute(session_id, 'mute', false);
    }

    transferSlot(session_id: any, phoneNumber: any) {
        let session: any = this.userAgent._sessions[session_id];
        if (session instanceof Session) {
            const target: sip.URI = UserAgent.makeURI(`sip:${phoneNumber}@10.10.10.19`)!;
            session.refer(target);
            this.updateSlotAttribute(session_id, 'hold', false);
            session.bye();
        }
    }

    holdSlot(session_id: any) {
        let session: any = this.userAgent._sessions[session_id];
        if ((session instanceof Session)) {
            session.invite({ sessionDescriptionHandlerModifiers: [Web.holdModifier] });
            this.updateSlotAttribute(session_id, 'hold', true);
        }
    }

    unHoldSlot(session_id: any) {
        let session: any = this.userAgent._sessions[session_id];
        if ((session instanceof Session)) {
            session.invite({ sessionDescriptionHandlerModifiers: [] });
            this.updateSlotAttribute(session_id, 'hold', false);
        }
    }


    setSlot(session_id: any, status: any, did: any, type: any) {

        // check current slot if empty use it
        if (!this._slots[this.currentSlot].session) {
            this._slots[this.currentSlot].did = did;
            this._slots[this.currentSlot].status = status;
            this._slots[this.currentSlot].type = type;
            this._slots[this.currentSlot].session = session_id;
            return;
        }

        if (this._slots[0].status == 'empty') {
            this._slots[0].did = did;
            this._slots[0].status = status;
            this._slots[0].type = type;
            this._slots[0].session = session_id;
            return;
        }

        if (this._slots[1].status == 'empty') {
            this._slots[1].did = did;
            this._slots[1].status = status;
            this._slots[1].type = type;
            this._slots[1].session = session_id;
            return;
        }

        if (this._slots[2].status == 'empty') {
            this._slots[2].did = did;
            this._slots[2].status = status;
            this._slots[2].type = type;
            this._slots[2].session = session_id;
            return;
        }

        // reject if slots are full
        let incomingSession: any = this.userAgent._sessions[session_id];
        if ((incomingSession instanceof sip.Invitation)) {
            incomingSession.reject();
        }
        if ((incomingSession instanceof sip.Inviter)) {
            console.log('slots are full')
            return;
        }

    }

    emptySlot(session_id: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                const data = this._slots[index];
                // console.log(JSON.parse(JSON.stringify(data)));
                this._slots[index].did = null;
                this._slots[index].session = null;
                this._slots[index].type = null;
                this._slots[index].status = 'empty';
                this._slots[index].answered = false;
                this._slots[index].mute = false;
                this._slots[index].hold = false;
                this._slots[index].media_element = null;
                this._slots[index].duration.ringStart = null;
                this._slots[index].duration.ringEnd = null;
                this._slots[index].duration.callStart = null;
                this._slots[index].duration.callEnd = null;
            }
        }
    }

    updateSlot(session_id: any, status: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                this._slots[index].status = status;
            }
        }
    }

    updateSlotAttribute(session_id: any, key: any, vvalue: any) {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                this._slots[index][key] = vvalue;
                // console.log('update attribute:', key, ' with value:', vvalue);
            }
        }
    }

    getSlot(session_id: any): any {
        for (let [index, value] of this._slots.entries()) {
            if (this._slots[index].session == session_id) {
                return this._slots[index];
            }
        }
    }


    createHistory(session_id: any) {
        const slot = this.getSlot(session_id);
        if (slot.answered) {
            this.callsHistory.splice(0, 0,
                {
                    id: 1,
                    from: slot.did,
                    status: 'answered',
                    date: new Date().toLocaleString(),
                },
            );
        } else {
            this.callsHistory.splice(0, 0,
                {
                    id: 1,
                    from: slot.did,
                    status: 'miss',
                    date: new Date().toLocaleString(),
                },
            );
        }
        localStorage.setItem('history', JSON.stringify(this.callsHistory));

    }


    public answerCall(session_id: any) {
        let incomingSession: any = this.userAgent._sessions[session_id];
        if (!(incomingSession instanceof sip.Invitation)) {
            throw new Error("session is not inviter");
        }
        // Handle incoming INVITE request.
        let constrainsDefault: MediaStreamConstraints = {
            audio: true,
            // audio: {
            //     deviceId:
            // },
            video: false,
        }

        const options: InvitationAcceptOptions = {
            sessionDescriptionHandlerOptions: {
                constraints: constrainsDefault,
            },
        }

        incomingSession.accept(options)
    }


    public endCall(session_id: any) {
        console.log(this.userAgent._sessions);
        let incomingSession: any = this.userAgent._sessions[session_id];
        if ((incomingSession instanceof sip.Invitation)) {
            incomingSession.bye();
        }
        if ((incomingSession instanceof sip.Inviter)) {
            if (this.getSlot(session_id).answered) {
                incomingSession.bye();
            } else {
                // if not answered
                incomingSession.cancel();
            }
        }

    }
    remoteStream = new MediaStream();
    setupRemoteMedia(session: Session) {
        const mediaElement = document.createElement('audio');
        document.body.append(mediaElement)

        if (!(session.sessionDescriptionHandler)) {
            throw new Error("Session description handler undefined.");
        }

        if (!(session.sessionDescriptionHandler instanceof Web.SessionDescriptionHandler)) {
            throw new Error("Session's session description handler not instance of Web SessionDescriptionHandler.");
        }

        if (!session.sessionDescriptionHandler.peerConnection) {
            throw new Error("Peer connection closed.");
        }

        session.sessionDescriptionHandler.peerConnection.getReceivers().forEach((receiver) => {
            if (receiver.track) {
                this.remoteStream.addTrack(receiver.track);
            }
        });
        mediaElement.srcObject = this.remoteStream;
        mediaElement.play();
        this.updateSlotAttribute(session.id, 'media_element', mediaElement);

    }
    cleanupMedia(session: Session) {
        let slot = this.getSlot(session.id);
        if (slot.media_element) {
            slot.media_element.srcObject = null;
            slot.media_element.pause();
            slot.media_element.remove();
        }
    }


    public createCall(phoneNumber: any) {
        const target: any = UserAgent.makeURI(`sip:${phoneNumber}@10.10.10.19`);
        const inviter = new Inviter(this.userAgent, target, {
            sessionDescriptionHandlerOptions: {
                constraints: { audio: true, video: false }
            }
        });


        // Handle outgoing session state changes
        inviter.stateChange.addListener((newState) => {
            switch (newState) {
                case SessionState.Establishing:
                    // Session is establishing.
                    console.log('Establishing')
                    this._status['session_state'] = 'Establishing';
                    this.status$.next(this._status);
                    break;
                case SessionState.Established:
                    console.log('Established');
                    this.holdOthers(inviter.id);
                    this.setRingEnd(inviter.id, new Date().toLocaleString());
                    this.setCallStart(inviter.id, new Date().toLocaleString());
                    // Session has been established.
                    this.setupRemoteMedia(inviter);
                    this.updateSlot(inviter.id, 'active')
                    break;
                case SessionState.Terminated:
                    // Session has terminated.
                    console.log('Terminated')
                    this.cleanupMedia(inviter);
                    this.setRingEnd(inviter.id, new Date().toLocaleString());
                    this.setCallEnd(inviter.id, new Date().toLocaleString());
                    this.createHistory(inviter.id);
                    this.emptySlot(inviter.id);
                    break;
                default:
                    break;
            }
        });

        // Send initial INVITE request
        inviter.invite()
            .then(() => {
                // INVITE sent
                this.setSlot(inviter.id, 'ringing', phoneNumber, 'outbound');
                this.setRingStart(inviter.id, new Date().toLocaleString());

                console.warn('INVITE sent');
            })
            .catch((error: Error) => {
                // INVITE did not send
                console.error('INVITE did not send');
            });
    }


    InitConfiguration() {
        let online = localStorage.getItem('online') ?? "1";
        if (online != "1") {
            return;
        }

        const transportOptions = {
            server: "wss://pbx2.horizon.iq:8089/ws"
        };

        let user: string = localStorage.getItem('sip_user') || '';
        let password: string = localStorage.getItem('sip_password') || '';
        const uri = UserAgent.makeURI(`sip:${user}@10.10.10.19`);
        const userAgentOptions: UserAgentOptions = {
            logLevel: 'error',
            authorizationPassword: password,
            authorizationUsername: user,
            transportOptions,
            uri,
        };
        this.userAgent = new UserAgent(userAgentOptions);
        // setInterval(() => {
        //     // console.log(this.userAgent._sessions)
        //     console.log(this.userAgent._sessions[this._slots[0].session])
        // }, 1000);

        // setTimeout(() => {
        //     for (let i in this.userAgent._sessions) {

        //         // const options = {
        //         //     sessionDescriptionHandlerModifiers: [Web.holdModifier]
        //         //     }
        //         // this.userAgent._sessions[i].invite(options);
        //     }
        //     console.log('end')

        // }, 10000);


        this.userAgent.delegate = {
            onInvite: (invitation: Invitation) => {

                var did = invitation.remoteIdentity.uri.user;


                this._status['src'] = did;
                this.status$.next(this._status);

                this._status['session_state'] = 'Incomming';
                this.status$.next(this._status);

                this.setSlot(invitation.id, 'ringing', did, 'inbound');
                this.setRingStart(invitation.id, new Date().toLocaleString());

                // An Invitation is a Session
                const incomingSession: Invitation = invitation;

                // Setup incoming session delegate
                incomingSession.delegate = {
                    // Handle incoming REFER request.
                    onRefer(referral: Referral): void {
                        // ...
                    }
                };

                // Handle incoming session state changes.
                incomingSession.stateChange.addListener((newState: SessionState) => {
                    switch (newState) {
                        case SessionState.Establishing:
                            // Session is establishing.

                            console.log('Establishing')
                            this._status['session_state'] = 'Establishing';
                            this.status$.next(this._status);
                            break;
                        case SessionState.Established:
                            this.holdOthers(invitation.id);
                            this.setRingEnd(invitation.id, new Date().toLocaleString());
                            this.setCallStart(invitation.id, new Date().toLocaleString());
                            // Session has been established.
                            console.log('Established')
                            this._status['session_state'] = 'Established';
                            this.status$.next(this._status);
                            this.setupRemoteMedia(invitation);
                            this.updateSlot(invitation.id, 'active')
                            break;
                        case SessionState.Terminated:
                            // Session has terminated.
                            this._status['session_state'] = 'Terminated';
                            this.callsHistory$.next(this.callsHistory);
                            this.status$.next(this._status);
                            console.log('Terminated')
                            this.cleanupMedia(invitation);
                            this.setRingEnd(invitation.id, new Date().toLocaleString());
                            this.setCallEnd(invitation.id, new Date().toLocaleString());
                            this.createHistory(invitation.id);
                            this.emptySlot(invitation.id);

                            break;
                        default:
                            break;
                    }
                });

                // Handle incoming INVITE request.
                let constrainsDefault: MediaStreamConstraints = {
                    audio: true,
                    // audio: {
                    //     deviceId:
                    // },
                    video: false,
                }

                const options: InvitationAcceptOptions = {
                    sessionDescriptionHandlerOptions: {
                        constraints: constrainsDefault,
                    },
                }

                // incomingSession.accept(options)
                // console.log(this)
            },
            onConnect: () => {
                // console.log('connected');
            },
            onDisconnect: () => {
                // console.log('disconnected');
            },
            onNotify: (notification: sip.Notification) => {
                // console.log(notification);
            }
        };


        this.userAgent.transport.stateChange.addListener((d: sip.TransportState) => {
            console.log(d);
        });



        /*
        * Create a Registerer to register user agent
        */
        const registererOptions: RegistererOptions = { /* ... */ };
        this.registerer = new Registerer(this.userAgent, registererOptions);

        /*
         * Start the user agent
         */
        this.userAgent.start().then(() => {

            // Register the user agent
            this.registerer.register().then(data => {
                console.log(data);
                this._status.registered = true;
                this.status$.next(this._status);
                console.log(this.userAgent._sessions);


            });
        });




    }



    // Device Detection
    // ================
    async DetectDevices(): Promise<any> {
        try {
            await navigator.mediaDevices.getUserMedia({ audio: true });
            let deviceInfos = await navigator.mediaDevices.enumerateDevices();
            // deviceInfos will not have a populated lable unless to accept the permission
            // during getUserMedia. This normally happens at startup/setup
            // so from then on these devices will be with lables.
            let HasVideoDevice = false;
            let HasAudioDevice = false;
            let HasSpeakerDevice = false; // Safari and Firefox don't have these
            let AudioinputDevices = [];
            let VideoinputDevices = [];
            let SpeakerDevices = [];
            for (var i = 0; i < deviceInfos.length; ++i) {
                if (deviceInfos[i].kind === "audioinput") {
                    HasAudioDevice = true;
                    AudioinputDevices.push(deviceInfos[i]);
                }
                else if (deviceInfos[i].kind === "audiooutput") {
                    HasSpeakerDevice = true;
                    SpeakerDevices.push(deviceInfos[i]);
                }
                else if (deviceInfos[i].kind === "videoinput") {
                    HasVideoDevice = true;
                    VideoinputDevices.push(deviceInfos[i]);
                }
            }
            return {
                AudioinputDevices,
                SpeakerDevices
            }

            // console.log('AudioinputDevices:', AudioinputDevices);
            // console.log('VideoinputDevices:', VideoinputDevices);
            // console.log('SpeakerDevices:', SpeakerDevices);
        } catch (e) {
            console.error("Error enumerating devices", e);
            return [];
        }


    }

    disconnect() {
        // console.log('salom', this.userAgent._subscriptions);
        // this.registerer.unregister().then(() => {
        //     console.log('unregester')
        //     this.userAgent.transport.disconnect().then(() => {
        //         console.log('transport disconnect')
        //         this.userAgent.stop().then(() => {
        //             console.log('userAgent stop')
        //         });
        //     });
        // });
        localStorage.setItem('online', '0');
        window.location.reload();



        // this.userAgent.stop().then(data => {
        //     console.log('sip disconnected');
        // });
    }

    connect() {
        // this.userAgent.transport.connect().then(data => {
        //     console.log('sip connected');
        // });
        localStorage.setItem('online', '1');
        window.location.reload();
        // this.InitConfiguration();
    }



    audioBlobs: any = {
        Ringtone: { url: '/assets/media/Ringtone_1.mp3', blob: null, player: null },
        CallWaiting: { url: '/assets/media/Tone_CallWaiting.mp3', blob: null, player: null },
        Busy_US: { url: '/assets/media/Tone_Busy-US.mp3', blob: null, player: null },
        Congestion_US: { url: '/assets/media/Tone_Congestion-US.mp3', blob: null, player: null },
        EarlyMedia_European: { url: '/assets/media/Tone_EarlyMedia-European.mp3', blob: null, player: null },
    };


    async playAudio() {

    }

    async createPlayer(key: any, loop = false) {

    }

    async loadAudio() {
        this.audioBlobs.Ringtone.blob = await fetch(this.audioBlobs.Ringtone.url).then(r => r.blob());
        this.audioBlobs.CallWaiting.blob = await fetch(this.audioBlobs.CallWaiting.url).then(r => r.blob());
        this.audioBlobs.Busy_US.blob = await fetch(this.audioBlobs.Busy_US.url).then(r => r.blob());
        this.audioBlobs.Congestion_US.blob = await fetch(this.audioBlobs.Congestion_US.url).then(r => r.blob());
        this.audioBlobs.EarlyMedia_European.blob = await fetch(this.audioBlobs.EarlyMedia_European.url).then(r => r.blob());


        // console.log(this.audioBlobs);
        // var rinnger = new Audio(URL.createObjectURL(this.audioBlobs.EarlyMedia_European.blob));
        // rinnger.preload = "auto";
        // rinnger.loop = false;
        // rinnger.oncanplaythrough = function(e) {
        //     rinnger.play().then(function(){
        //         // Audio Is Playing
        //     }).catch(function(e){
        //         console.warn("Unable to play audio file.", e);
        //     }); 
        // }
    }


}
