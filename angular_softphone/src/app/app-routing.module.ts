import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SASComponent } from './components/sas/sas.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sas', component: SASComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
