import { Component } from '@angular/core';
import { SipService } from '../services/sip.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-app';

  constructor(
    private sip: SipService
  ) {
  }
}
