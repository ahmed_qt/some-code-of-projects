import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import { ToastModule } from "primeng/toast";
import { ButtonModule } from "primeng/button";
import { ConfirmationService, MessageService } from 'primeng/api';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import { FormsModule } from '@angular/forms';
import {OrganizationChartModule} from 'primeng/organizationchart';
import {SplitterModule} from 'primeng/splitter';
import { SASComponent } from './components/sas/sas.component';
import {DialogModule} from 'primeng/dialog';
import { HeaderComponent } from './components/header/header.component';
import { SidePanelComponent } from './components/side-panel/side-panel.component';
import { ContactsHistoryPanelComponent } from './components/contacts-history-panel/contacts-history-panel.component';
import { StatusBarComponent } from './components/status-bar/status-bar.component';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import { DateAgoPipe } from './pipes/date-ago.pipe';
import { SipService } from '../services/sip.service';
import { TimeagoModule } from 'ngx-timeago';
import {ListboxModule} from 'primeng/listbox';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SASComponent,
    HeaderComponent,
    SidePanelComponent,
    ContactsHistoryPanelComponent,
    StatusBarComponent,
    DateAgoPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ConfirmPopupModule,
    FormsModule,
    ButtonModule,
    ToastModule,
    PasswordModule,
    InputTextModule,
    OrganizationChartModule,
    SplitterModule,
    DialogModule,
    OverlayPanelModule,
    TimeagoModule.forRoot(),
    ListboxModule,
    DropdownModule
  ],
  providers: [ConfirmationService, MessageService, SipService],
  bootstrap: [AppComponent]
})
export class AppModule { }
