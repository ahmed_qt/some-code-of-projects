import { Component, ElementRef, OnInit, AfterViewInit, ViewChild } from '@angular/core';
// import { SimpleUser, SimpleUserOptions } from "sip.js/lib/platform/web";
// import { Invitation, Inviter, Registerer, SessionState, UserAgent, UserAgentOptions } from 'sip.js/lib/api'
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  // @ViewChild('localVideo') localVideo:any;
  @ViewChild('remoteVideo') remoteVideo: any;

  username: string = '1060';
  password: string = 'aa';

  // userAgent: UserAgent;

  public display: boolean = false;

  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  confirm(event: Event) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: 'Are you sure that you want to proceed?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.load();
        this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted' });
      },
      reject: () => {

        this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected' });
      }
    });
  }

  load() {
    // const transportOptions = {
    //   server: "wss://10.10.10.19:8089/ws"
    // };

    // const uri = UserAgent.makeURI("sip:1060@10.10.10.19");
    // const userAgentOptions: UserAgentOptions = {
    //   authorizationPassword: 'aa',
    //   authorizationUsername: '1060',
    //   delegate: {
    //     onInvite: (invitation: Invitation) => this.onInvite(invitation),
    //     onRegister: (registration: any) => this.onRegister(registration),

    //   },
    //   transportOptions,
    //   uri
    // };
    // this.userAgent = new UserAgent(userAgentOptions);
    // const registerer = new Registerer(this.userAgent);
    // this.userAgent.start().then(() => {
    //   registerer.register();

    //   const target: any = UserAgent.makeURI("sip:403@10.10.10.19");
    //   const inviter = new Inviter(this.userAgent, target, {
    //     sessionDescriptionHandlerOptions: {
    //       constraints: { audio: true, video: false }
    //     }
    //   });


    //   // Handle outgoing session state changes
    //   inviter.stateChange.addListener((newState) => {
    //     switch (newState) {
    //       case SessionState.Establishing:
    //         // Session is establishing
    //         console.warn('Establishing');
    //         break;
    //       case SessionState.Established:
    //         // Session has been established
    //         console.warn('Established');
    //         break;
    //       case SessionState.Terminated:
    //         // Session has terminated
    //         console.warn('Terminated');
    //         break;
    //       default:
    //         break;
    //     }
    //   });


    //   // Send initial INVITE request
    //   inviter.invite()
    //     .then(() => {
    //       // INVITE sent
    //       console.warn('INVITE sent');
    //     })
    //     .catch((error: Error) => {
    //       // INVITE did not send
    //       console.error('INVITE did not send');
    //     });



    // });
  }

  ngOnInit(): void {

  }

  // countSessions(id: any): any {
  //   let rtn = 0;
  //   if(this.userAgent == null) {
  //       console.warn("userAgent is null");
  //       return 0;
  //   }
  //   let agent: any = this.userAgent;

  //   agent.sessions.array.forEach((i: any, session: any) => {
  //     if(id != session.id) rtn ++;
  //   });
  //   return rtn;
  // }

  // onRegister(registration: any): void {
  //   console.error('registered');
  //   console.info(registration);
  // }

  // onInvite(session: Invitation): void {
  //   // session.
  //   // Defined In Next Steps
  //   this.display = true;
  //   console.log(this)

  //   console.log(this.display);
  //   console.log("new call");
  //   var callerID = session.remoteIdentity.displayName; // Mariam Rushdi
  //   var did = session.remoteIdentity.uri.user; // 403

  //   console.log('---------------------------');
  //   console.log("New Incoming Call!", callerID + " <" + did + ">");

  //   // var CurrentCalls = this.countSessions(session.id);
  //   // console.log("Current Call Count:", CurrentCalls);

  // }


  ngAfterViewInit(): void {


  }


}
