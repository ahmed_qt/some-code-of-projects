import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { OverlayPanel } from 'primeng/overlaypanel';
import { SipService } from '../../../services/sip.service';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { TimeagoIntl } from 'ngx-timeago';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.9s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ]
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild('bell') bell: ElementRef;
  @ViewChild('op') op: OverlayPanel;


  display: boolean = false;
  transferDisplay: boolean = false;

  showDialog() {
      this.display = !this.display;
  }

  public NNUMPAD_NUMBER: string = '';


  public NUMBPAD_KEYBOARD: any = [
    {value: '1', text: ''},
    {value: '2', text: 'A B C'},
    {value: '3', text: 'D E F'},
    {value: '4', text: 'G H I'},
    {value: '5', text: 'J K L'},
    {value: '6', text: 'M N O'},
    {value: '7', text: 'P Q R S'},
    {value: '8', text: 'T U V'},
    {value: '9', text: 'W X Y Z'},
    {value: '*', text: ''},
    {value: '0', text: '+'},
    {value: '#', text: ''},
  ]


  statusArray = [
    {name: 'Ready', value: 'ready'},
    {name: 'Not Ready', value: 'not_ready'},
  ]

  selectedStatus: any;

  popNumpadNumber() {
    this.NNUMPAD_NUMBER = String(this.NNUMPAD_NUMBER).slice(0, -1);
  }

  pushNumpadNumber(data: any) {
    this.NNUMPAD_NUMBER = String(this.NNUMPAD_NUMBER) + data;
  }

  constructor(
    public sip: SipService
  ) { 
    let online = localStorage.getItem('online') ?? "1";
    if (online != "1") {
        this.selectedStatus = this.statusArray[1]
    } else {
      this.selectedStatus = this.statusArray[0]
    }

  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.sip.status$.subscribe(data => {
      setTimeout(() => {
        this.display = true;
      }, 1);
    })

    // this.sip.status$.subscribe(data => {
    //   console.log(data);
    //   console.log('update from status$')
    //   if (data['session_state'] == 'Terminated') {
    //     this.display = false;
    //     return;
    //   }
    //   if (data['registered']) {
    //     // this.op.show(null, this.bell.nativeElement);
    //     this.display = true;
    //   }
    // })
  }


  hold(slot: any) {
    if (slot?.hold) {
      this.sip.unHoldSlot(slot?.session);
    } else {
      this.sip.holdSlot(slot?.session);
    }
  }

  mute(slot: any) {
    if (slot?.mute) {
      this.sip.unMuteSlot(slot?.session);
    } else {
      this.sip.muteSlot(slot?.session);
    }
  }

  transferSession: any;


  showTransferDialog(slot: any) {
    this.transferSession = slot?.session;
    this.transferDisplay = true;
    this.sip.holdSlot(slot?.session);


  }

  hideTransferDialog() {
    this.sip.unHoldSlot(this.transferSession);
    this.transferSession = null;
  }

  transfer(number: any) {
    this.sip.transferSlot(this.transferSession, number)
    this.transferDisplay = false;
  }

  onStatusChange() {
    if (this.selectedStatus.value == 'ready') {
      this.sip.connect();
    } else {
      this.sip.disconnect();
    }
  }


}
