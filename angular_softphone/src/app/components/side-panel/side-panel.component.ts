import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { SipService } from '../../../services/sip.service';

@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.scss']
})
export class SidePanelComponent implements OnInit {

  display: boolean = false;

  outputDevices: Array<any> = [];
  inputDevices: Array<any> = [];

  selectedOutputDevice: any;
  selectedInputDevice: any;

  isFirefox = false;
  sipUser: any = '';
  sipPassword: any = '';

  constructor(
    private sip: SipService,
    private messageService: MessageService
  ) {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
      this.isFirefox = true;
    }
    this.sip.DetectDevices().then((data: any) => {
      this.outputDevices = data.SpeakerDevices;
      this.inputDevices = data.AudioinputDevices;
      this.loadSettings();
    });
  }

  loadSettings() {
    this.sipUser = localStorage.getItem('sip_user') ?? '';
    this.sipPassword = localStorage.getItem('sip_password') ?? '';
    if(!this.isFirefox) {
      this.selectedOutputDevice= localStorage.getItem('output_device') ?? null;
    }
    this.selectedInputDevice= localStorage.getItem('input_device') ?? null;
    console.log(this.selectedOutputDevice);
    console.log(this.selectedInputDevice);
  }

  ngOnInit(): void {

  }

  showSettings() {
    this.display = true;
  }

  saveSettings() {
    // console.log(this.selectedInputDevice);
    localStorage.setItem('sip_user', this.sipUser);
    localStorage.setItem('sip_password', this.sipPassword);
    if (!this.isFirefox) {
      localStorage.setItem('output_device', this.selectedOutputDevice);
    }
    localStorage.setItem('input_device', this.selectedInputDevice);
    this.messageService.add({ severity: 'success', summary: 'Settings Saved', detail: 'Settings Saved' });
  }

  onChange() {
    // console.log(this.selectedOutputDevice);
    // console.log(this.selectedInputDevice);
  }

}
