import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { SipService } from '../../../services/sip.service';

@Component({
  selector: 'app-contacts-history-panel',
  templateUrl: './contacts-history-panel.component.html',
  styleUrls: ['./contacts-history-panel.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.9s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ]
})
export class ContactsHistoryPanelComponent implements OnInit, AfterViewInit {

  @ViewChild('chatbox') public chatbox: ElementRef;

  public recentCalls = [
    {
      id: 1,
      from: '+9647719729219',
      status: 'answered',
      date: '2021-04-08 09:30:19',
    },
    {
      id: 2,
      from: '+9647719729219',
      status: 'miss',
      date: '2021-04-08 06:30:19',
    },
    {
      id: 3,
      from: '+9647719729219',
      status: 'answered',
      date: '2021-04-07 06:30:19',
    },
    {
      id: 4,
      from: 'Salom',
      status: 'miss',
      date: '2021-03-08 06:30:19',
    }
  ];

  public current_active: any = null;

  constructor(
    public sip: SipService
  ) {
    sip.callsHistory$.subscribe(data => {
      this.recentCalls = data;
    });
    // this.current_active = this.recentCalls[0];

   }

   ngAfterViewInit() {
    // try {
    //   // this.chatbox.nativeElement.scrollTop = this.chatbox.nativeElement.scrollHeight;
    //   try {
    //     // this.chatbox.nativeElement.scrollTop = this.chatbox.nativeElement.scrollHeight;
    //     this.chatbox.nativeElement.scroll({
    //       top: this.chatbox.nativeElement.scrollHeight,
    //       left: 0,
    //       behavior: 'smooth'
    //     });
  
    //   } catch (error) {
    //       console.log(error)
    //   }
    // } catch (error) {
      
    // }
   }

    ngOnInit(): void {

    //   setInterval(() => {
    //     console.log(this.chatbox)
       
    // }, 1000)
    // setInterval(() => {
    //   this.recentCalls.splice(0,0,{
    //     id: 4,
    //     from: '100',
    //     status: 'answered',
    //     date: new Date().toLocaleString(),
    //   });
    // }, 5000)
  }

}
