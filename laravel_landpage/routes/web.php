<?php

use App\Http\Controllers\DoctorsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DoctorsController::class, 'home']);
Route::get('/search', [DoctorsController::class, 'searchIndex']);
Route::get('/_s', [DoctorsController::class, 'search']);
Route::get('/about', [DoctorsController::class, 'about']);
Route::post('/search', [DoctorsController::class, 'search']);
Route::get('/doctor/{id}', [DoctorsController::class, 'doctor']);


Route::get('/articles/medical-advices', [DoctorsController::class, 'medicalAdvices']);
Route::get('/articles/news', [DoctorsController::class, 'news']);
Route::get('/articles/orthodontic', [DoctorsController::class, 'orthodontic']);
Route::get('/articles/periodontology', [DoctorsController::class, 'periodontology']);
Route::get('/articles/pediatric-dentistry', [DoctorsController::class, 'pediatricDentistry']);
Route::get('/articles/oral-microbiology', [DoctorsController::class, 'oralMicrobiology']);
Route::get('/articles/prosthodontics', [DoctorsController::class, 'prosthodontics']);
Route::get('/articles/preventive-dentistry', [DoctorsController::class, 'preventiveDentistry']);
Route::get('/articles/laser-dentistry', [DoctorsController::class, 'laserDentistry']);
Route::get('/articles/general-dentistry', [DoctorsController::class, 'generalDentistry']);
Route::get('/articles/oral-and-maxillofacial-surgery', [DoctorsController::class, 'oralAndMaxillofacialSurgery']);
Route::get('/articles/dental-implant', [DoctorsController::class, 'dentalImplant']);
Route::get('/articles/oral-maxillofacial-radiology', [DoctorsController::class, 'oralMaxillofacialRadiology']);
Route::get('/articles/endodontics', [DoctorsController::class, 'endodontics']);
Route::get('/available-soon', [DoctorsController::class, 'availableSoon']);
