<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    protected $table = 'dentwe_specialties';
    public $incrementing = true;
    public $timestamps = true;
}
