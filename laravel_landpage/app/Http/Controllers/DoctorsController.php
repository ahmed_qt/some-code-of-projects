<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Specialty;
use App\Models\User;
use App\Models\UserCourse;
use App\Models\UserEducation;
use App\Models\UserMembership;
use Illuminate\Http\Request;

class DoctorsController extends Controller
{
    //

    public function home() {
        return redirect('/search');
    }

    public function about() {
        return view('about');
    }

    public function medicalAdvices() {
        return view('articles.medical-advices');
    }

    public function news() {
        return view('articles.news');
    }

    public function orthodontic() {
        return view('articles.orthodontic');
    }

    public function periodontology() {
        return view('articles.periodontology');
    }

    public function pediatricDentistry() {
        return view('articles.pediatric-dentistry');
    }

    public function oralMicrobiology() {
        return view('articles.oral-microbiology');
    }

    public function prosthodontics() {
        return view('articles.prosthodontics');
    }

    public function preventiveDentistry() {
        return view('articles.preventive-dentistry');
    }

    public function laserDentistry() {
        return view('articles.laser-dentistry');
    }

    public function generalDentistry() {
        return view('articles.general-dentistry');
    }

    public function oralAndMaxillofacialSurgery() {
        return view('articles.oral-and-maxillofacial-surgery');
    }

    public function dentalImplant() {
        return view('articles.dental-implant');
    }

    public function oralMaxillofacialRadiology() {
        return view('articles.oral-maxillofacial-radiology');
    }

    public function endodontics() {
        return view('articles.endodontics');
    }

    public function availableSoon() {
        return view('articles.available-soon');
    }

    public function searchIndex(Request $request) {

        $specialties = Specialty::all()->pluck('name');
        $countries = Country::all();

        $query = $request->get('query');
        $specialty = $request->get('specialty');
        $country = $request->get('country');

        $country = Country::where('code', $country)->first();


        $title = $request->get('title', null);
        $genders = $request->get('genders', null);

        $title = array_filter(explode(',', $title));
        $genders = array_filter(explode(',', $genders));


        $data = User::Active()->where('acl_group_id', 2);
        $data = $data->where('id', '==', -1);

        $data = $data->paginate(9);


        return view('search')->with([
            'data' => $data,
            'specialties' => $specialties,
            'countries' => $countries,
            'query' => $query,
            'specialty' => $specialty,
            'country' => $country,
            'title' => $title,
            'genders' => $genders,
        ]);
    }

    public function search(Request $request) {

        $specialties = Specialty::all()->pluck('name');
        $countries = Country::all();

        $query = $request->get('query');
        $specialty = $request->get('specialty');
        $country = $request->get('country');

        $country = Country::where('code', $country)->first();


        $title = $request->get('title', null);
        $genders = $request->get('genders', null);

        $title = array_filter(explode(',', $title));
        $genders = array_filter(explode(',', $genders));


        $data = User::Active()->where('acl_group_id', 2);
        $data = $data->where('id', '!=', 1);

        $data = $data->where(function($q) use ($query) {
            $q->where('first_name', 'like', "%$query%");
            $q->orWhere('last_name', 'like', "%$query%");
            $q->orWhere('full_name', 'like', "%$query%");
        });

        if (count($genders))
            $data = $data->whereIn('gender', $genders);

        if (count($title))
            $data = $data->whereIn('title', $title);

        if ($specialty)
            $data = $data->where('specialty', 'like', "%$specialty%");

        if ($country)
            $data = $data->where('country', 'like', "%$country->code%");


        $data = $data->paginate(9);


        return view('search')->with([
            'data' => $data,
            'specialties' => $specialties,
            'countries' => $countries,
            'query' => $query,
            'specialty' => $specialty,
            'country' => $country,
            'title' => $title,
            'genders' => $genders,
        ]);
    }


    public function doctor($id) {
        if ($id == 1) {
            abort(404);
        }
        $user = User::Active()->where('acl_group_id', 2)->find($id);
        if ($user == null) {
            abort(404);
        }

        $country = Country::where('code', $user->country)->first();

        $data = array();
        $data['id'] = $user->id;
        $data['phone'] = $user->phone;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['full_name'] = $user->full_name;
        $data['avatar'] = $user->avatar;
        $data['country'] = $country;
        $data['title'] = $user->title;
        $data['specialty'] = $user->specialty;
        $data['languages'] = $user->languages;
        $data['brief_description'] = $user->brief_description;
        $data['educations'] = UserEducation::where('user_id',  $user->id)->get();
        $data['courses'] = UserCourse::select('course_title')->where('user_id',  $user->id)->get();
        $data['memeberships_awards'] = UserMembership::select('name')->where('user_id',  $user->id)->get();

        return view('doctor')->with(['data' => $data]);
    }
}
