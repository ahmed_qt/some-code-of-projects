{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!-- ========================= 
            Google Map
    =========================  -->
    <section class="google-map py-0">
      <iframe frameborder="0" height="500" width="100%"
        src="https://maps.google.com/maps?q=Sakala 7-2, 10141 Tallinn, Estonia&amp;t=m&amp;z=10&amp;output=embed&amp;iwloc=near"></iframe>
    </section><!-- /.GoogleMap -->

    <!-- ==========================
        contact layout 1
    =========================== -->
    <section class="contact-layout1 pt-0 mt--100">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="contact-panel d-flex flex-wrap">
              <section class="testimonials-layout2 pt-40 pb-40">
                <div class="container">
                  <div class="testimonials-wrapper">
                    <div class="row">
                      <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="heading-layout2">
                          <h3 class="heading__title">Dentwe</h3>
                        </div><!-- /.heading -->
                      </div><!-- /.col-lg-5 -->
                      <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="slider-with-navs">
                          <!-- Testimonial #1 -->
                          <div class="testimonial-item">
                            <h3 class="testimonial__title">“We welcome you to Dentwe, the world's largest Dentists network with more than 170 countries and territories.
                              Our Vision is to Create economic opportunities for every dentists and dental students with facilitate finding them by their patients in the global labor market.
                              Our Mission is to connect dentists from around the world and sharing the experience individually to help them be more productive and achieve all of their work goals. Moreover to make easy search for every dental clinic by everyone.”
                            </h3>
                          </div><!-- /. testimonial-item -->
                         
                        </div><!-- /.slick-carousel -->
                      
                      </div><!-- /.col-lg-7 -->
                    </div><!-- /.row -->
                  </div><!-- /.testimonials-wrapper -->
                </div><!-- /.container -->
              </section><!-- /.testimonials layout 2 -->
            </div>
          </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.contact layout 1 -->




</body>

</html>
@endsection