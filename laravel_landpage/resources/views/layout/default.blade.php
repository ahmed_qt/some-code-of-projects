<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="description" content="Dentwe Medcity - Dental Healthcare">
    <link href="assets/images/favicon/favicon.png" rel="icon">
    <title>Dentwe OÜ  - Dental Healthcare</title>

    <!-- Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;500;600;700&family=Roboto:wght@400;700&display=swap">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
    <link rel="stylesheet" href="{{ asset('assets/css/libraries.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <style>

        @media screen and (max-width: 768px) {
            .search-container {
                width: 100%;
                flex-direction: column;

            }

            .search-control {
                width: 100%;
                display: flex;
                margin: 5px;
            }

            .search-control > :first-child {
                flex-grow: 1;
                margin: 5px;
            }
        }

        .search-container {
            width: 100%;
        }

        .search-control {
            width: 100%;
            flex: 25%;
            margin: 5px;
        }

        .search-control > :first-child {
            width: 100%;
            margin: 5px;
        }



    </style>
    @yield('meta')
</head>


<body>

    <div class="wrapper">
{{--        <div class="preloader">--}}
{{--            <div class="loading"><span></span><span></span><span></span><span></span></div>--}}
{{--        </div><!-- /.preloader -->--}}
        @include('layout.base._header')

        @yield('content')

        @include('layout.base._footer')
    </div><!-- /.wrapper -->


    <script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>
