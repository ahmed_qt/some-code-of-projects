<!-- =========================
        Header
    =========================== -->
<header class="header header-layout1">
    <div class="header-topbar">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="d-flex align-items-center justify-content-between">
                        <ul class="contact__list d-flex flex-wrap align-items-center list-unstyled mb-0">
                            <div style="height: 60px"></div>
{{--                            <li>--}}
{{--                                <button class="miniPopup-emergency-trigger" type="button">24/7--}}
{{--                                    Emergency</button>--}}
{{--                                <div id="miniPopup-emergency" class="miniPopup miniPopup-emergency text-center">--}}
{{--                                    <div class="emergency__icon">--}}
{{--                                        <i class="icon-call3"></i>--}}
{{--                                    </div>--}}
{{--                                    <a href="tel:+34653567655" class="phone__number">--}}
{{--                                        <i class="icon-phone"></i> <span>+34 653567655</span>--}}
{{--                                    </a>--}}
{{--                                    <p>Please feel free to contact our friendly reception staff with any general--}}
{{--                                        or medical enquiry.--}}
{{--                                    </p>--}}
{{--                                    --}}{{-- <a href="appointment.html" class="btn btn__secondary btn__link btn__block">--}}
{{--                                                <span>Make Appointment</span> <i class="icon-arrow-right"></i>--}}
{{--                                            </a> --}}
{{--                                </div><!-- /.miniPopup-emergency -->--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="icon-phone"></i><a href="tel:+34653567655">Emergency Line: (0034)--}}
{{--                                    653567655</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="icon-location"></i><a href="{{url('contact-us')}}">Location: Sakala 7-2, 10141 Tallinn,--}}
{{--                                    Estonia</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="icon-email"></i><a href="mailto:info@dentwe.com">info@dentwe.com</a>--}}
{{--                            </li>--}}
                        </ul><!-- /.contact__list -->
                        <div class="d-flex">
                            <ul class="list-unstyled mb-0 mr-30">
                                <li><a href="https://app.dentwe.com" style="color: white; text-decoration: underline">Login</a></li>
                            </ul>
                            {{-- <ul class="social-icons list-unstyled mb-0 mr-30">
                                <li><a href="https://www.facebook.com/dentwe.dentist.9/"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/dent.we/"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="https://twitter.com/dentwe2"><i class="fab fa-twitter"></i></a></li>
                            </ul><!-- /.social-icons --> --}}
{{--                            <form class="header-topbar__search" method="get" action="{{url('/search')}}">--}}
{{--                                <input type="text" name="query" class="form-control" value="{{request()->get('query')}}" placeholder="Search...">--}}
{{--                                <button class="header-topbar__search-btn"><i class="fa fa-search"></i></button>--}}
{{--                            </form>--}}
                        </div>
                    </div>
                </div><!-- /.col-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->
    <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{-- <img src="{{ asset('assets/images/logo/logo-light.png')}}" class="logo-light" alt="logo"> --}}
                {{-- <img src="{{ asset('assets/images/logo/logo-dark.png')}}" class="logo-dark" alt="logo"> --}}
                <img src="https://app.dentwe.com/assets/images/logo/logo.png" class="logo-dark" height="60" alt="logo">
            </a>

            <button class="navbar-toggler" type="button">
                <span class="menu-lines"><span></span></span>
            </button>
            <div class="collapse navbar-collapse" id="mainNavigation">
                <ul class="navbar-nav ml-auto">
                    <li class="nav__item has-dropdown">
                        <a  class="nav__item-link {{ (request()->is('/')) ? 'active' : '' }}">Home</a>

                    </li><!-- /.nav-item -->

                    <li class="nav__item has-dropdown">
                        <a  class="nav__item-link">Events</a>

                    </li><!-- /.nav-item -->

                    <li class="nav__item has-dropdown">
                        <a  class="nav__item-link">Shop</a>

                    </li><!-- /.nav-item -->


                    <li class="nav__item has-dropdown">
                        <a  class="nav__item-link">Jobs</a>

                    </li><!-- /.nav-item -->


                    <li class="nav__item">
                        <a href="{{url('/search')}}" class="nav__item-link {{ (request()->is('search')) ? 'active' : '' }}">Doctors</a>
                    </li><!-- /.nav-item -->

                    {{-- <li class="nav__item">
                        <a href="#" class="nav__item-link">Contacts</a>
                    </li><!-- /.nav-item --> --}}

                    <li class="nav__item">
                        <a href="{{url('about')}}" class="nav__item-link  {{ (request()->is('about')) ? 'active' : '' }}">About</a>
                    </li><!-- /.nav-item -->

                    <li class="nav__item d-block d-xl-none">
                        <a href="https://app.dentwe.com" class="nav__item-link">Login</a>
                    </li><!-- /.nav-item -->
                </ul><!-- /.navbar-nav -->
                <button class="close-mobile-menu d-block d-lg-none"><i class="fas fa-times"></i></button>
            </div><!-- /.navbar-collapse -->
            <div class="d-none  d-xl-flex align-items-center position-relative ml-30">

{{--                <a href="https://app.dentwe.com" class="btn btn__primary btn__rounded ml-30">--}}
{{--                    <i class="icon-user"></i>--}}
{{--                    <span>Login</span>--}}
{{--                </a>--}}
            </div>
        </div><!-- /.container -->
    </nav><!-- /.navabr -->
</header><!-- /.Header -->
