{{-- Extends layout --}}
@extends('layout.default')

@section('meta')
<meta property="og:title" content="{{$data['first_name'] . ' ' . $data['last_name'] . ' - Dentwe'}}" />
<meta property="og:type" content="social" />
<meta property="og:url" content="{{env('APP_URL')}}/doctor/{{$data['id']}}" />
@if ($data['avatar'])
    <meta property="og:image" content="{{ env('AVATAR_STORAGE_PATH') . $data['avatar'] }}" />
@else
    <meta property="og:image" content="{{asset('/assets/images/team/placeholder.jpg')}}" />
@endif


<meta name="twitter:title" content="{{$data['first_name'] . ' ' . $data['last_name'] . ' - Dentwe'}}">
<meta name="twitter:description" content="">
@if ($data['avatar'])
    <meta name="twitter:image" content="{{ env('AVATAR_STORAGE_PATH') . $data['avatar'] }}">

@else
    <meta name="twitter:image" content="{{asset('/assets/images/team/placeholder.jpg')}}">
@endif
{{-- <meta name="twitter:card" content="summary_large_image"> --}}
@endsection

{{-- Content --}}
@section('content')
    <!-- ========================
               page title
            =========================== -->
    <section class="page-title page-title-layout5">
        <div class="bg-img"><img src="{{ asset('assets/images/backgrounds/6.jpg') }}" alt="background"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="pagetitle__heading">{{ $data['first_name'] . ' ' . $data['last_name'] }}</h1>
                    <nav>
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/search') }}">Doctors</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                {{ $data['first_name'] . ' ' . $data['last_name'] }}</li>
                        </ol>
                    </nav>
                </div><!-- /.col-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.page-title -->

    <section class="pt-120 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <aside class="sidebar has-marign-right">
                        <div class="widget widget-member">
                            <div class="member mb-0">
                                <div class="member__img">
                                    @if ($data['avatar'])
                                        <img src="{{ env('AVATAR_STORAGE_PATH') . $data['avatar'] }}" alt="member img">
                                    @else
                                        <img src="{{ asset('/assets/images/team/placeholder.jpg') }}" alt="member img">
                                    @endif
                                </div><!-- /.member-img -->
                                <div class="member__info">
                                    <h5 class="member__name"><a
                                            href="doctors-single-doctor1.html">{{ $data['first_name'] . ' ' . $data['last_name'] }}</a>
                                    </h5>
                                    <p class="member__job">{{ $data['specialty'] }}</p>
                                    <p class="member__desc">{{ $data['brief_description'] }}</p>
                                    <div class="mt-20 d-flex flex-wrap justify-content-between align-items-center">
                                        <ul class="social-icons list-unstyled mb-0">
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{env('APP_URL')}}/doctor/{{$data['id']}}" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                            </li>
                                            <li><a href="https://twitter.com/intent/tweet?url={{env('APP_URL')}}/doctor/{{$data['id']}}&hashtags=dentwe" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#" class="phone"><i class="fas fa-phone-alt"></i></a></li>
                                        </ul><!-- /.social-icons -->
                                    </div>
                                </div><!-- /.member-info -->
                            </div><!-- /.member -->
                        </div><!-- /.widget-member -->
                        <div class="widget widget-help bg-overlay bg-overlay-primary-gradient">
                            <div class="bg-img"><img src="{{ asset('assets/images/banners/5.jpg') }}"
                                    alt="background"></div>
                            <div class="widget-content">
                                <div class="widget__icon">
                                    <i class="icon-call3"></i>
                                </div>
                                <h4 class="widget__title">Emergency Cases</h4>
                                <p class="widget__desc">Please feel welcome to contact our friendly reception staff with
                                    any general
                                    or medical enquiry call us.
                                </p>
                                <a href="tel:+34653567655" class="phone__number">
                                    <i class="icon-phone"></i> <span>{{ $data['phone'] }}</span>
                                </a>
                            </div><!-- /.widget-content -->
                        </div><!-- /.widget-help -->
                        <div class="widget widget-schedule">
                            <div class="widget-content">
                                <div class="widget__icon">
                                    <i class="icon-charity2"></i>
                                </div>
                                <h4 class="widget__title">Opening Hours</h4>
                                <ul class="time__list list-unstyled mb-0">
                                    <li><span>Monday - Friday</span><span>8.00 - 7:00 pm</span></li>
                                    <li><span>Saturday</span><span>9.00 - 10:00 pm</span></li>
                                    <li><span>Sunday</span><span>10.00 - 12:00 pm</span></li>
                                </ul>
                            </div><!-- /.widget-content -->
                        </div><!-- /.widget-schedule -->
                        {{-- <div class="widget widget-reports">
                            <a href="#" class="btn btn__primary btn__block">
                                <i class="icon-pdf-file"></i>
                                <span>2020 Patient Reports</span>
                            </a>
                        </div> --}}
                    </aside><!-- /.sidebar -->
                </div><!-- /.col-lg-4 -->
                <div class="col-sm-12 col-md-12 col-lg-8">
                    <div class="text-block mb-50">
                        <h5 class="text-block__title">Biography</h5>
                        <p class="text-block__desc mb-20 font-weight-bold color-secondary">
                            {{ $data['brief_description'] }}</p>

                    </div><!-- /.text-block -->
                    <ul class="details-list list-unstyled mb-60">
                        <li>
                            <h5 class="details__title">Full Name</h5>
                            <div class="details__content">
                                <p class="mb-0">{{ $data['full_name'] }}</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="details__title">Country</h5>
                            <div class="details__content">
                                <p class="mb-0">{{ $data['country']['name'] }}</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="details__title">Speciality</h5>
                            <div class="details__content">
                                <p class="mb-0">{{ $data['specialty'] }}</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="details__title">Title</h5>
                            <div class="details__content">
                                <p class="mb-0">{{ $data['title'] }}</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="details__title">Areas of Expertise</h5>
                            <div class="details__content">
                                <ul class="list-items list-items-layout2 list-unstyled mb-0">
                                    @foreach ($data['courses'] as $row)
                                        <li>{{ $row['course_title'] }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>

                    </ul>


                    <div class="text-block mb-50">
                        <h5 class="text-block__title">Awards And Honours</h5>

                    </div><!-- /.text-block -->
                    <div class="fancybox-layout2">
                        <div class="row">
                            @foreach ($data['memeberships_awards'] as $row)
                                <div class="col-sm-6">
                                    <!-- fancybox item #1 -->
                                    <div class="fancybox-item d-flex">
                                        <div class="fancybox__icon">
                                            <i class="icon-diploma"></i>
                                        </div><!-- /.fancybox__icon -->
                                        <div class="fancybox__content" style="display: flex; align-items: center;">
                                            <h4 class="fancybox__title">{{ $row['name'] }}</h4>
                                        </div><!-- /.fancybox-content -->
                                    </div><!-- /.fancybox-item -->
                                </div><!-- /.col-sm-6 -->
                            @endforeach

                        </div><!-- /.row -->
                    </div><!-- /.fancybox-layout2 -->
                    <div class="text-block mb-40">
                        <h5 class="text-block__title">Medical Education</h5>

                    </div><!-- /.text-block -->
                    <div class="timeline-wrapper mb-60">
                        @foreach ($data['educations'] as $row)
                            <!-- Timeline Item #1 -->
                            <div class="timeline-item d-flex">
                                <span class="timeline__year">{{ date('Y', strtotime($row['end_date'])) }}</span>
                                <div class="timeline__body">
                                    <h4 class="timeline__title">
                                        {{ $row['college'] . ' (' . $row['country'] . ')' . '  -  (' . $row['degree'] . ')' }}
                                    </h4>
                                    <p class="timeline__desc mb-0">{{ $row['awarding_body'] }}
                                    </p>
                                </div><!-- /.timeline__body -->
                            </div><!-- /.timeline-item -->
                        @endforeach

                    </div><!-- /.timeline-wrapper -->

                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
            <div class="row" style="margin-top: 100px">

                <!-- ============================
            contact info
        ============================== -->
                <section class="contact-info py-0">
                    <div class="container">
                        <div class="row row-no-gutter boxes-wrapper">
                            <div class="col-sm-12 col-md-4">
                                <div class="contact-box d-flex">
                                    <div class="contact__icon">
                                        <i class="icon-call3"></i>
                                    </div><!-- /.contact__icon -->
                                    <div class="contact__content">
                                        <h2 class="contact__title">Emergency Clinics</h2>
                                        <p class="contact__desc">Please feel free to contact our friendly reception staff
                                            with
                                            any general or
                                            medical enquiry.</p>
                                        <a href="tel:+34653567655" class="phone__number">
                                            <i class="icon-phone"></i> <span>{{ $data['phone'] }}</span>
                                        </a>
                                    </div><!-- /.contact__content -->
                                </div><!-- /.contact-box -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-sm-12 col-md-4">
                                <div class="contact-box d-flex">
                                    <div class="contact__icon">
                                        <i class="icon-health-report"></i>
                                    </div><!-- /.contact__icon -->
                                    <div class="contact__content">
                                        <h2 class="contact__title">Doctors Timetable</h2>
                                        <p class="contact__desc">Qualified doctors available six days a week, view our
                                            timetable to make an
                                            appointment.</p>
                                        <a href="#" class="btn btn__white btn__outlined btn__rounded">
                                            <span>View Timetable</span><i class="icon-arrow-right"></i>
                                        </a>
                                    </div><!-- /.contact__content -->
                                </div><!-- /.contact-box -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-sm-12 col-md-4">
                                <div class="contact-box d-flex">
                                    <div class="contact__icon">
                                        <i class="icon-heart2"></i>
                                    </div><!-- /.contact__icon -->
                                    <div class="contact__content">
                                        <h2 class="contact__title">Opening Hours</h2>
                                        <ul class="time__list list-unstyled mb-0">
                                            <li><span>Monday - Friday</span><span>8.00 - 7:00 pm</span></li>
                                            <li><span>Saturday</span><span>9.00 - 10:00 pm</span></li>
                                            <li><span>Sunday</span><span>10.00 - 12:00 pm</span></li>
                                        </ul>
                                    </div><!-- /.contact__content -->
                                </div><!-- /.contact-box -->
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section><!-- /.contact-info -->

            </div>
        </div><!-- /.container -->
    </section>
@endsection
