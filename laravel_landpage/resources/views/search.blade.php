{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')


    <section id="app" class="shop-grid">

        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div style="font-size: 20px; padding: 10px; text-align: center">Dentwe Helps you to find best dentist in your city, let's explore.</div>
            </div>
            <div class="row">
                <div class="search-container sorting-options d-flex justify-content-center align-items-center mb-30">

                    <v-select class="search-control mr-1" placeholder="Select a specilty" v-model="selectedSpecialty"
                        :options="specialties"></v-select>


                    <v-select class="search-control" label="name" class="mr-1" placeholder="Select a country" v-model="selectedCountry"
                        :options="countries"></v-select>

                    <div class="search-control widget-search">
                        <div class="widget__content">
                            <div class="widget__form-search">
                                <input v-model="query" type="text" class="form-control" placeholder="Or search by name">
                                <button class="btn" type="submit"><i class="icon-search"></i></button>
                            </div>
                        </div><!-- /.widget-content -->
                    </div>


                    <div class="search-control">
                        <button @click="search()" class="btn btn__primary  ml-2" style="width: 250px; height: 55px; border-radius: 4px;">
                            <i class="icon-search"></i>
                            <span>Search</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-9 order-2 order-md-1">


                    <div class="row">
                        <!-- Product item #1 -->
                        @foreach ($data as $doctor)
                            <div class="col-sm-6 col-md-6 col-lg-4">
                                <div class="product-item">
                                    <div class="product__img">
                                        @if ($doctor->avatar)
                                            <img src="{{ env('AVATAR_STORAGE_PATH') . $doctor->avatar }}" alt="Product"
                                            loading="lazy">
                                        @else
                                            <img src="{{ asset('/assets/images/team/placeholder.jpg') }}" alt="Product"
                                            loading="lazy">
                                        @endif

                                        <div class="product__action">
                                            <a href="{{ url('/doctor/' . $doctor->id) }}"
                                                class="btn btn__primary btn__rounded">
                                                <span>Read More</span>
                                            </a>
                                        </div><!-- /.product-action -->
                                    </div><!-- /.product-img -->
                                    <div class="product__info">
                                        <h4 class="product__title"><a
                                                href="{{ url('/doctor/' . $doctor->id) }}">{{ $doctor->first_name . ' ' . $doctor->last_name }}</a>
                                        </h4>
                                    </div><!-- /.product-content -->
                                </div><!-- /.product-item -->
                            </div><!-- /.col-lg-4 -->

                        @endforeach

                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                            {{-- <x-pagination :data="$data"></x-pagination> --}}
                            {{ $data->links('pagination::bootstrap-4') }}

                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->
                </div><!-- /.col-lg-9 -->
                <div class="col-sm-12 col-md-4 col-lg-3 order-1 order-md-2">
                    <aside class="sidebar-layout2">


                        <div class="widget widget-categories">
                            <h5 class="widget__title">Title</h5>
                            <div class="widget-content">
                                <ul class="list-unstyled mb-0">




                                    <li v-for="option of options">
                                        <b-form-checkbox v-model="selectedTitle" :key="option.value" :value="option.value">
                                            @{{ option.text }}
                                        </b-form-checkbox>
                                    </li>

                                </ul>
                            </div><!-- /.widget-content -->
                        </div><!-- /.widget-categories -->



                        <div class="widget widget-categories">
                            <h5 class="widget__title">Gender</h5>
                            <div class="widget-content">
                                <ul class="list-unstyled mb-0">
                                    <li v-for="option of genders">
                                        <b-form-checkbox v-model="selectedGender" :key="option.value" :value="option.value">
                                            @{{ option.text }}
                                        </b-form-checkbox>
                                    </li>

                                </ul>
                            </div><!-- /.widget-content -->
                        </div><!-- /.widget-categories -->





                    </aside><!-- /.sidebar -->
                </div><!-- /.col-lg-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.shop -->


    <script src="{{ mix('js/app.js') }}"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                selectedTitle: @json($title),
                selectedGender: @json($genders),
                options: [{
                        text: 'Dr',
                        value: 'Dr'
                    },
                    {
                        text: 'Consultant',
                        value: 'Consultant'
                    },
                    {
                        text: 'Professor',
                        value: 'Professor'
                    },
                    {
                        text: 'Assistant Prof',
                        value: 'Assistant Prof'
                    },
                    {
                        text: 'Specialist',
                        value: 'Specialist'
                    },
                    {
                        text: 'General Practitioner',
                        value: 'General Practitioner'
                    },
                    {
                        text: 'Trainee Dentist',
                        value: 'Trainee Dentist'
                    },
                ],
                genders: [{
                        text: 'Male',
                        value: 'male'
                    },
                    {
                        text: 'Female',
                        value: 'female'
                    },

                ],
                selectedSpecialty: '{{$specialty}}',
                selectedCountry: @json($country),
                specialties: @json($specialties),
                countries: @json($countries),
                query: '{{$query}}',

            },
            methods: {
                search() {
                    // to do: queryparams and search
                    const data = {

                        query: this.query,
                        title: this.selectedTitle.join(','),
                        genders: this.selectedGender.join(','),
                    };

                    if (this.selectedSpecialty) {
                        data['specialty'] = this.selectedSpecialty;
                    }

                    if (this.selectedCountry) {
                        data['country'] = this.selectedCountry.code;
                    }
                    this.post_to_url('/_s', data, 'get')
                },
                post_to_url(path, params, method) {
                    method = method || "post";

                    var form = document.createElement("form");
                    form.setAttribute("method", method);
                    form.setAttribute("action", path);

                    for (var key in params) {
                        if (params.hasOwnProperty(key)) {
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", key);
                            hiddenField.setAttribute("value", params[key]);

                            form.appendChild(hiddenField);
                        }
                    }

                    document.body.appendChild(form);
                    form.submit();
                }
            }
        });
    </script>
@endsection
