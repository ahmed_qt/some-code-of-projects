@if ($paginator->hasPages())
<nav class="pagination-area">
    <ul class="pagination justify-content-center">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                    {{-- <a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 disabled"><i class="ki ki-bold-double-arrow-back icon-xs"></i></a> --}}
                    {{-- <a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 disabled"><i class="ki ki-bold-arrow-back icon-xs"></i></a> --}}

                @else
                
                    {{-- <a href="{{$paginator->toArray()['first_page_url']}}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1"><i class="ki ki-bold-double-arrow-back icon-xs"></i></a> --}}
                    <li><a href="{{ $paginator->toArray()['first_page_url'] }}"><i class="fa fa-angle-double-left"></i></a></li>

                    <li><a href="{{ $paginator->previousPageUrl() }}"><i class="fa fa-angle-left"></i></a></li>
                    {{-- <a href="{{ $paginator->previousPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1"><i class="ki ki-bold-arrow-back icon-xs"></i></a> --}}

                @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                        <a class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 disabled">{{ $element }}</a>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li><a class="current" href="#">{{ $page }}</a></li>
                            {{-- <a href="#" class="btn btn-icon btn-sm active border-0 btn-hover-primary mr-2 my-1">{{ $page }}</a> --}}
                        @else
                            <li><a class="" href="{{$url}}">{{ $page }}</a></li>
                            {{-- <a href="{{$url}}" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">{{ $page }}</a> --}}
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())

                    <li><a href="{{ $paginator->nextPageUrl() }}"><i class="fa fa-angle-right"></i></a></li>
                    <li><a href="{{ $paginator->toArray()['last_page_url'] }}"><i class="fa fa-angle-double-right"></i></a></li>
                    {{-- <a href="{{ $paginator->nextPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1"><i class="ki ki-bold-arrow-next icon-xs"></i></a> --}}
                    {{-- <a href="{{ $paginator->toArray()['last_page_url'] }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1"><i class="ki ki-bold-double-arrow-next icon-xs"></i></a> --}}

                @else
                    {{-- <a  class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 disabled"><i class="ki ki-bold-arrow-next icon-xs"></i></a> --}}
                    {{-- <a  class="btn btn-icon btn-sm btn-light-primary mr-2 my-1"><i class="ki ki-bold-double-arrow-next icon-xs"></i></a> --}}

                @endif
    </ul>
</nav>
@endif
