{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                       
                       
                        <div class="post__body pb-0 pt-5">
                            <h1 class="post__title mb-30">
                                Oral Microbiology
                            </h1>
                            
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <p>Oral microbiology is the study of the microorganisms (microbiota) of the oral cavity and their interactions between oral microorganisms or with the host. The environment present in the human mouth is suited to the growth of characteristic microorganisms found there. It provides a source of water and nutrients, as well as a moderate temperature. Resident microbes of the mouth adhere to the teeth and gums to resist mechanical flushing from the mouth to stomach where acid-sensitive microbes are destroyed by hydrochloric acid.</p>
                                <p>Anaerobic bacteria in the oral cavity include: Actinomyces, Arachnia (Propionibacterium propionicus), Bacteroides, Bifidobacterium, Eubacterium, Fusobacterium, Lactobacillus, Leptotrichia, Peptococcus, Peptostreptococcus, Propionibacterium, Selenomonas, Treponema, and Veillonella. Genera of fungi that are frequently found in the mouth include Candida, Cladosporium, Aspergillus, Fusarium, Glomus, Alternaria, Penicillium, and Cryptococcus, among others. Bacteria accumulate on both the hard and soft oral tissues in biofilms. Bacterial adhesion is particularly important for oral bacteria.</p>
                                <p>Oral bacteria have evolved mechanisms to sense their environment and evade or modify the host. Bacteria occupy the ecological niche provided by both the tooth surface and mucosal epithelium. Factors of note that have been found to affect the microbial colonization of the oral cavity include the pH, oxygen concentration and availability at specific oral surfaces, mechanical forces acting upon oral surfaces, salivary and fluid flow through the oral cavity, and age. However, a highly efficient innate host defense system constantly monitors the bacterial colonization and prevents bacterial invasion of local tissues. A dynamic equilibrium exists between dental plaque bacteria and the innate host defense system. Of particular interest is the role of oral microorganisms in the two major dental diseases: dental caries and periodontal disease. Additionally, research has correlated poor oral heath and the resulting ability of the oral microbiota to invade the body to affect cardiac health as well as cognitive function.</p>
                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
