{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                       
                       
                        <div class="post__body pb-0 pt-5">
                            <h1 class="post__title mb-30">
                                Orthodontic
                            </h1>
                            
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <p>is a dentistry specialty that addresses the diagnosis, prevention, and correction of mal-positioned teeth and jaws, and misaligned bite patterns. It may also address the modification of facial growth, known as dentofacial orthopedics.</p>
                                <p>Abnormal alignment of the teeth and jaws is very common. Nearly 50% of the developed world's population, according to the American Association of Orthodontics, has malocclusions severe enough to benefit from orthodontic treatment: although this figure decreases to less than 10% according to the same AAO statement when referring to medically necessary orthodontics. However, conclusive scientific evidence for the health benefits of orthodontic treatment is lacking, although patients with completed orthodontic treatment have reported a higher quality of life than that of untreated patients undergoing orthodontic treatment. Treatment may require several months to a few years, and entails using dental braces and other appliances to gradually adjust tooth position and jaw alignment. In cases where the malocclusion is severe, jaw surgery may be incorporated in the treatment plan. Treatment usually begins before a person reaches adulthood, insofar as pre-adult bones may be adjusted more easily before adulthood.</p>
                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
