{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                       
                       
                        <div class="post__body pb-0 pt-5">
                            <h1 class="post__title mb-30">
                                Endodontics
                            </h1>
                            
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <p>is the dental specialty concerned with the study and treatment of the dental pulp. </p>
                                <p>Endodontics encompasses the study (practice) of the basic and clinical sciences of normal dental pulp, the etiology, diagnosis, prevention, and treatment of diseases and injuries of the dental pulp along with associated periradicular conditions.</p>
                                <p>In clinical terms, endodontics involves either preserving part, or all of the dental pulp in health, or removing all of the pulp in irreversible disease. This includes teeth with irreversibly inflamed and infected pulpal tissue. Not only does endodontics involve treatment when a dental pulp is present, but also includes preserving teeth which have failed to respond to non-surgical endodontic treatment, or for teeth that have developed new lesions, e.g., when root canal re-treatment is required, or periradicular surgery.</p>
                                <p>Endodontic treatment is one of the most common procedures. If the dental pulp (containing nerves, arterioles, venules, lymphatic tissue, and fibrous tissue) becomes diseased or injured, endodontic treatment is required to save the tooth.</p>
                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
