{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                        <h1 class="post__title mb-30">
                            10 advices to Keep Your Teeth Healthy
                        </h1>
                        <div class="post__img">
                            <a href="#">
                                <img src="{{asset('assets/images/articles/medical-advices.jpeg')}}" style="max-width: 300" alt="post image" loading="lazy">
                            </a>
                        </div><!-- /.post-img -->
                        <div class="post__body pb-0">
                            <div class="post__meta-cat">
                                <a href="#">Consulting</a><a href="#">Sales</a>
                            </div><!-- /.blog-meta-cat -->
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <h5>1. Don't go to bed without brushing your teeth</h5>
                                <p>General recommendation is to brush at least twice a day. but, many of us continue to neglect brushing our teeth at night. But brushing before bed gets rid of the germs and plaque that accumulate throughout the day.</p>

                                <h5>2. Brush properly</h5>
                                <p>doing a poor job of brushing your teeth is almost as bad as not brushing at all. Take your time, moving the toothbrush in gentle, circular motions to remove plaque.</p>


                                <h5>3. Don't neglect your tongue</h5>
                                <p>plaque can also build up on your tongue. Not only can this lead to bad mouth odor, but it can lead to other oral health problems. Gently brush your tongue every time you brush your teeth.</p>


                                <h5>4. Use a fluoride toothpaste</h5>
                                <p>fluoride has come under scrutiny by those worried about how it impacts other areas of health, this substance remains a mainstay in oral health. This is because fluoride is a leading defense against tooth decay. It works by fighting germs that can lead to decay, as well as providing a protective barrier for your teeth.</p>


                                <h5>5. Treat flossing as important as brushing</h5>
                                <p>Flossing is not just for getting little pieces of food or broccoli that may be getting stuck in between your teeth, as Jonathan Schwartz, DDS. points out. “It’s really a way to stimulate the gums, reduce plaque, and help lower inflammation in the area.”</p>


                                <h5>6. Consider mouthwash</h5>
                                <p>Mouthwash helps in three ways: It reduces the amount of acid in the mouth, cleans hard-to-brush areas in and around the gums, and re-mineralizes the teeth. “Mouthwashes are useful as an adjunct tool to help bring things into balance,” he explains. “I think in children and older people, where the ability to brush and floss may not be ideal, a mouthwash is particularly helpful.”</p>



                                <h5>7. Drink more water</h5>
                                <p>Water continues to be the best beverage for your overall health — including oral health.This can help wash out some of the negative effects of sticky and acidic foods and beverages in between brushes.</p>


                                <h5>8. Eat crunchy fruits and vegetables</h5>
                                <p>Eating fresh, crunchy produce not only contains more healthy fiber, but it’s also the best choice for your teeth. </p>


                                <h5>9. Limit sugary and acidic foods</h5>
                                <p>Ultimately, sugar converts into acid in the mouth, which can then erode the enamel of your teeth. These acids are what lead to cavities. Acidic fruits, teas, and coffee can also wear down tooth enamel. While you don’t necessarily have to avoid such foods altogether, it doesn’t hurt to be mindful.</p>

                                <h5>10. See your dentist at least twice a year</h5>
                                <p>At minimum, you should see your dentist for cleanings and checkups twice a year. Not only can a dentist remove calculus and look for cavities, but they will also be able to spot potential issues and offer treatment solutions.</p>


                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
