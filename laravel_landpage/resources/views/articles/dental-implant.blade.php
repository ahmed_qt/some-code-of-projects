{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                       
                       
                        <div class="post__body pb-0 pt-5">
                            <h1 class="post__title mb-30">
                                Dental Implant 
                            </h1>
                            
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <p>A dental implant (also known as an endosseous implant or fixture) is a prosthesis that interfaces with the bone of the jaw or skull to support a dental prosthesis such as a crown, bridge, denture, or facial prosthesis or to act as an orthodontic anchor. The basis for modern dental implants is a biologic process called osseointegration, in which materials such as titanium or zirconia form an intimate bond to bone. The implant fixture is first placed so that it is likely to osseointegrate, then a dental prosthetic is added. A variable amount of healing time is required for osseointegration before either the dental prosthetic (a tooth, bridge or denture) is attached to the implant or an abutment is placed which will hold a dental prosthetic/crown.</p>
                                <p>Success or failure of implants depends on the health of the person receiving the treatment, drugs which affect the chances of osseointegration, and the health of the tissues in the mouth. The amount of stress that will be put on the implant and fixture during normal function is also evaluated. Planning the position and number of implants is key to the long-term health of the prosthetic since biomechanical forces created during chewing can be significant. The position of implants is determined by the position and angle of adjacent teeth, by lab simulations or by using computed tomography with CAD/CAM simulations and surgical guides called stents. The prerequisites for long-term success of osseointegrated dental implants are healthy bone and gingiva. Since both can atrophy after tooth extraction, pre-prosthetic procedures such as sinus lifts or gingival grafts are sometimes required to recreate ideal bone and gingiva.</p>
                                <p>The final prosthetic can be either fixed, where a person cannot remove the denture or teeth from their mouth, or removable, where they can remove the prosthetic. In each case an abutment is attached to the implant fixture. Where the prosthetic is fixed, the crown, bridge or denture is fixed to the abutment either with lag screws or with dental cement. Where the prosthetic is removable, a corresponding adapter is placed in the prosthetic so that the two pieces can be secured together.</p>
                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
