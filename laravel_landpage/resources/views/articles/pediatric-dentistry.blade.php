{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                       
                       
                        <div class="post__body pb-0 pt-5">
                            <h1 class="post__title mb-30">
                                Pediatric dentistry
                            </h1>
                            
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <p>is the branch of dentistry dealing with children from birth through adolescence.  Pediatric dentists promote the dental health of children as well as serve as educational resources for parents. It is recommended by the American Academy of Pediatric Dentistry (AAPD) and the American academy of pediatric  (AAP) that a dental visit occurs after the presence of the first tooth or by a child's first birthday. The AAPD has said that it is important to establish a comprehensive and accessible ongoing relationship between the dentist and patient – referring to this as the patient's "dental home".This is because early oral examination aids in the detection of the early stages of tooth decay . Early detection is essential to maintain oral health, modify aberrant habits, and treat as needed and as simply as possible. Additionally, parents are given a program of preventive home care (brushing, flossing and fluorides), a caries risk assessment, information on finger, thumb, and pacifier habits, and may include advice on preventing injuries to the mouth and teeth of children, diet counseling, and information on growth and development.</p>
                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
