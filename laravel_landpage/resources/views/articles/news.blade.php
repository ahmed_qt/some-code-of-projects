{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                        <h1 class="post__title mb-30">
                            Special mouthguard may be alternative to CPAP for sleep apnea
                        </h1>
                        <div class="post__img">
                            <a href="#">
                                <img src="{{asset('assets/images/articles/news.jpg')}}" style="max-width: 300" alt="post image" loading="lazy">
                            </a>
                        </div><!-- /.post-img -->
                        <div class="post__body pb-0">
                            <div class="post__meta-cat">
                                <a href="#">Consulting</a><a href="#">Sales</a>
                            </div><!-- /.blog-meta-cat -->
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <p style="font-size: 18px; color: #202532;">Folks with sleep apnea are typically prescribed a CPAP (continuous positive airway pressure) is a machine that uses mild air pressure to keep breathing airways open while you sleep, to help them get a good night's sleep, but there's an alternative to the clunky, noisy devices that is growing in popularity.
                                    Oral appliances similar to mouthguards—called mandibular advancement devices (MADs)—have been shown to work as well as CPAP in treating sleep apnea for many patients, experts say.
                                    "The oral appliances have been around for less time than CPAP, but they've grown a lot in popularity over the last couple of years and they can be as effective as CPAP for the right patient," Wang said.
                                    Obstructive sleep apnea occurs when your throat muscles relax to the point of collapse and block your airway during sleep. When this happens, your body becomes starved for oxygen and you periodically wake up for a few seconds to gasp for air.
                                    People with severe sleep apnea can wake more than 30 times as hour while trying to get their rest, according to the National Sleep Foundation. Daytime sleepiness is the most prominent side effect, but sleep apnea also has been linked to serious health problems such as high blood pressure, heart disease and diabetes.
                                    Sleep apnea patients given a CPAP (continuous pressure airway pressure) machine wear a mask through the night that keeps the airway propped open.
                                    But CPAP devices can be annoying to use, said Dr. Mitchell Levine, president-elect of the American Academy of Dental Sleep Medicine.
                                    The masks can make people feel claustrophobic, the noise of the machine can disturb bed partners, and people using CPAP can suffer from dry eyes, bloody noses and sores on their face, Levine said.
                                    As a result, some patients are turning to oral devices to help with their sleep apnea.
                                    
                                    </p>

                               

                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
