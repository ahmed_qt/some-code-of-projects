{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <section class="blog blog-single pt-5 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="post-item mb-0">
                       
                       
                        <div class="post__body pb-0 pt-5">
                            <h1 class="post__title mb-30">
                                Laser dentistry
                            </h1>
                            
                            <div class="post__meta d-flex align-items-center mb-20">
                                <span class="post__meta-date">Jan 20, 2022</span>
                            </div><!-- /.blog-meta -->
                            
                            <div class="post__desc">
                                <p>is the use of lasers to treat a number of different dental conditions. It became commercially used in clinical dental practice for procedures involving tooth tissue in 1989.</p>
                                <p>Laser dentistry potentially offers a more comfortable treatment option for a number of dental procedures involving hard or soft tissue compared to drills and other non-laser tools.</p>
                                <p>LASER stands for “light amplification by the stimulated emission of radiation.” The instrument creates light energy in a very narrow and focused beam. This laser light produces a reaction when it hits tissue, allowing it to remove or shape the tissue.</p>
                                <p>Laser dentistry is used in a variety of procedures, including:</p>
                                <ul>
                                    <li>treating <a href="https://www.healthline.com/health/sensitive-teeth">hypersensitivity</a></li>
                                    <li>treating <a href="https://www.healthline.com/symptom/tooth-decay">tooth decay</a></li>
                                    <li>treating <a href="https://www.healthline.com/health/gingivitis">gum disease</a></li>
                                    <li>whitening teeth</li>
                                </ul>
                            
                            </div><!-- /.blog-desc -->
                        </div>
                    </div><!-- /.post-item -->
                    <div class="d-flex flex-wrap justify-content-between border-top border-bottom pt-30 pb-30 mb-40">
                        <div class="blog-share d-flex flex-wrap align-items-center">
                            <strong class="mr-20 color-heading">Share</strong>
                            <ul class="list-unstyled social-icons d-flex mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div><!-- /.blog-share -->

                    </div>



                </div><!-- /.col-lg-8 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection
