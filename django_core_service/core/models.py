from django.db import models
from django.utils import timezone

# Create your models here.
class PrtgSensor(models.Model):
    sensor = models.CharField(max_length=255, null=True)
    objid = models.IntegerField(unique=True)
    status = models.CharField(max_length=255, null=True)
    message = models.TextField(null=True)
    probe = models.CharField(max_length=255, null=True)
    group = models.CharField(max_length=255, null=True)
    device = models.CharField(max_length=255, null=True)
    lastvalue = models.CharField(max_length=255, null=True)
    opsgenie_incident_id = models.CharField(max_length=255, null=True)
    opsgenie_request_id = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(default=timezone.now, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    finished = models.IntegerField(default=0)

    class Meta:
        db_table = 'prtg_sensors'


class OpsgenieCallsLog(models.Model):
    service = models.CharField(max_length=255, null=True)
    description = models.TextField(null=True)
    url = models.CharField(max_length=255, null=True)
    headers = models.TextField(null=True)
    data = models.JSONField(null=True)
    method = models.CharField(max_length=255, null=True)
    status_code = models.IntegerField(null=True)
    response = models.JSONField(null=True)
    created_at = models.DateTimeField(default=timezone.now, null=True)
    updated_at = models.DateTimeField(default=timezone.now, null=True)
    last_try = models.DateTimeField(null=True)
    class Meta:
        db_table = 'opsgenie_calls_log'