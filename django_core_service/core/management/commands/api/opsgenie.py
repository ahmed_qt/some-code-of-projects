import requests
from core.models import OpsgenieCallsLog
from django.utils import timezone
import time

OPSGENIE_API_KEY = ''


HEADERS = {
    'content-type':'application/json',
    'Authorization': 'GenieKey ' + OPSGENIE_API_KEY
}

SERVICES = {
    'prtg_server': ''
}

TEAMS = {
    'noc_team': '',
    'database_team': '',
}


class OpsGenieAPI():

    def db_log(self, service, description, url, data, method, status_code, response):
        opsgenie_call = OpsgenieCallsLog()
        opsgenie_call.service = service
        opsgenie_call.description = description
        opsgenie_call.url = url
        opsgenie_call.headers = HEADERS
        opsgenie_call.data = data
        opsgenie_call.method = method
        opsgenie_call.status_code = status_code
        opsgenie_call.response = response
        opsgenie_call.save()


    def __init__(self):
        self.base_url = 'https://api.opsgenie.com/v1/'
        self.session = requests.Session()
        self.session.headers = HEADERS

    def get_incident_request_id(self, request_id, service):
        # receive incident by requestId
        get_request_url = self.base_url + 'incidents/requests/' + request_id
        request_incident_response = self.session.get(get_request_url)
        self.db_log(
            service=service,
            description='receive incident by requestId',
            url=get_request_url,
            data={},
            method='GET',
            status_code=request_incident_response.status_code,
            response=request_incident_response.json()
        )
        return request_incident_response.json()

    def create_incident(self, service, message, description, tags):
        route = 'incidents/create'
        impactedServices = []
        responders = []

        if service == 'prtg':
            impactedServices = [SERVICES['prtg_server']]
            responders = [
                {
                    'id': TEAMS['noc_team'],
                    'type': 'team'
                }
            ]

        data = {
            'message': message,
            'description': description,
            'responders': responders,
            'impactedServices': impactedServices,
            'tags': tags
        }


        try:

            # create incident
            create_incident_url = self.base_url + route
            create_incident_response = self.session.post(create_incident_url, json=data)

            self.db_log(
                service=service,
                description=message,
                url=create_incident_url,
                data=data,
                method='POST',
                status_code=create_incident_response.status_code,
                response=create_incident_response.json()
            )
            return create_incident_response.json()
        except Exception as e:
            print(e)

    def delete_all_incidents(self):
        incidents = self.session.get('https://api.opsgenie.com/v1/incidents/').json()
        for i in incidents['data']:
            print(self.session.delete('https://api.opsgenie.com/v1/incidents/' + i['id']).json())


    def close_incident(self,service, incident_id):
        route = 'incidents/' + incident_id + '/close'

        data = {'note':'Action executed via Incident API'}

        close_incident_url = self.base_url + route
        close_incident_response = self.session.post(close_incident_url, json=data)
        self.db_log(
            service=service,
            description='closeing incident',
            url=close_incident_url,
            data=data,
            method='POST',
            status_code=close_incident_response.status_code,
            response=close_incident_response.json()
        )
        return close_incident_response.json()