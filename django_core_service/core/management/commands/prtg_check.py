from django.core.management.base import BaseCommand
from django.utils import timezone
from core.models import PrtgSensor

import logging
logger = logging.getLogger('main')

from .api.opsgenie import OpsGenieAPI

import requests
import time
import urllib3

class Command(BaseCommand):
    help = 'Opsgenie Prtg Integration'

    def handle(self, *args, **kwargs):

        # create opsgenie client
        opsgenie_client = OpsGenieAPI()
        # opsgenie_client.delete_all_incidents()
        # return

        urllib3.disable_warnings()
        ids = [] # list of inserted ids so we can use where not in () mysql query to get sensors and close them on opsgenie
        x = requests.get('https://..../api/table.json?content=alerts&columns=objid,sensor,status,message,probe,group,device,lastvalue&filter_status=5&username=developer&password=Developer1234', verify=False).json()
        for i in x['alerts']:
            result = PrtgSensor.objects.filter(objid=i['objid'])
            if result.exists():
                ids.append(i['objid'])
                sensor = result[0]
                if sensor.finished == 1:
                    sensor.finished = 0
                    sensor.save()
                   
                # print(str(i['objid']) + ' - ' + str(i['sensor']) + "....(checked)") 
                logger.info(str(i['objid']) + ' - ' + str(i['sensor']) + "....(checked)")
            else:
                
                sensor = PrtgSensor()
                sensor.objid = i['objid']
                sensor.sensor = i['sensor']
                sensor.status = i['status']
                sensor.message = i['message_raw']
                sensor.probe = i['probe']
                sensor.group = i['group']
                sensor.device = i['device']
                sensor.lastvalue = i['lastvalue']
                sensor.created_at = timezone.now()
                sensor.updated_at = timezone.now()
                sensor.finished = 0
                res = opsgenie_client.create_incident(
                    service='prtg',
                    message=sensor.message,
                    description='',
                    tags=[sensor.probe, sensor.group, sensor.device]
                )
                sensor.opsgenie_request_id = res['requestId']

                sensor.save()
                
                logger.info('updated the database....(updated)')
        
        # change finished status for removed sensors
        sensors = PrtgSensor.objects.filter(opsgenie_incident_id__isnull=False)
        for i in sensors:
            is_there = 0
            for prtg_item in x['alerts']:
                if i.objid == prtg_item['objid']:
                    is_there = 1
            if not is_there:
                opsgenie_client.close_incident(
                        service='prtg',
                        incident_id=i.opsgenie_incident_id
                )
                logger.info('closing incident ' + i.opsgenie_incident_id)
                # i.opsgenie_request_id = None
                # i.opsgenie_incident_id = None
                # i.finished = 1
                # i.save()
                i.delete()

