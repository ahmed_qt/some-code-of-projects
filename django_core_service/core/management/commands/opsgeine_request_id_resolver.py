from django.core.management.base import BaseCommand
from django.utils import timezone
from core.models import PrtgSensor

import logging
logger = logging.getLogger('main')

from .api.opsgenie import OpsGenieAPI

import requests
import time
import urllib3

class Command(BaseCommand):
    help = 'Opsgenie request id resolver for prtg'

    def handle(self, *args, **kwargs):
        opsgenie_client = OpsGenieAPI()
        results = PrtgSensor.objects.filter(opsgenie_incident_id__isnull=True, opsgenie_request_id__isnull=False)
        if results.exists():
            for i in results:
                res = opsgenie_client.get_incident_request_id(
                    request_id=i.opsgenie_request_id,
                    service='prtg',
                )
                if res['data']['success']:
                    i.opsgenie_incident_id = res['data']['incidentId']
                    i.save()
                    logger.info('resolved requestId ' + i.opsgenie_request_id)
