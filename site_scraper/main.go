package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"

	gojsonq "github.com/thedevsaddam/gojsonq/v2"
)

type Item struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	URL         string `json:"url"`
	Thumbnail   string `json:"thumbnail"`
	Number      string `json:"number"`
	MarketPrice string `json:"marketPrice"`
	MedianPrice string `json:"medianPrice"`
}

var globalItems []Item
var auth smtp.Auth
var listName string
var listUrl string

func Scrape() {
	// Request the HTML page.
	res, err := http.Get(listUrl)
	if err != nil {
		log.Println(err)
		return
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Printf("status code error: %d %s\n", res.StatusCode, res.Status)
		return
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Println(err)
		return
	}

	// Find the review items
	items := []Item{}

	doc.Find(".priceGuideTable.tablesorter > tbody > tr").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		id := s.AttrOr("id", "0")
		title := s.Find(".product .cellWrapper .productDetail a").Text()

		url := s.Find(".product .cellWrapper .productDetail a").AttrOr("href", "")
		thumbnail := s.Find(".product .cellWrapper .thumbnail a img").AttrOr("src", "")
		thumbnail = strings.Replace(thumbnail, "_25w.jpg", "_400w.jpg", -1)

		number := s.Find(".number div.cellWrapper").Text()
		number = strings.TrimSpace(number)

		marketPrice := s.Find(".marketPrice div.cellWrapper").Text()
		marketPrice = strings.TrimSpace(marketPrice)

		medianPrice := s.Find(".medianPrice div.cellWrapper").Text()
		medianPrice = strings.TrimSpace(medianPrice)

		item := Item{}
		item.ID = id
		item.Title = title
		item.URL = url
		item.Thumbnail = thumbnail
		item.Number = number
		item.MarketPrice = marketPrice
		item.MedianPrice = medianPrice

		items = append(items, item)
	})

	if len(globalItems) > 0 {
		data, _ := json.Marshal(globalItems)
		dataStr := string(data)
		jq := gojsonq.New().FromString(dataStr)

		for _, v := range items {
			fmt.Println("checking for ", v.Title)
			fmt.Println("id = ", v.ID)
			resData := jq.Reset().Where("id", "=", v.ID).Get()
			if jq.Error() != nil {
				log.Fatal(jq.Errors())
			} else {
				newItems := resData.([]interface{})
				if len(newItems) > 0 {
					newItem := newItems[0].(map[string]interface{})
					// fmt.Println(newItem)
					if v.MarketPrice == newItem["marketPrice"] {
						fmt.Println("pass")
					} else {
						// send(v.Title, v.Thumbnail, newItem["marketPrice"].(string), v.MarketPrice)
						telegramBot(v.Title, v.Thumbnail, newItem["marketPrice"].(string), v.MarketPrice)
						log.Println("warning........................")
					}
				}
			}

		}
	} else {
		fmt.Println("nothing to do...")
	}

	outputJSON, _ := json.MarshalIndent(items, "", "")
	err = ioutil.WriteFile(listName+".json", outputJSON, 0644)
	if err != nil {
		panic(err)
	}
}

func send(title string, img string, oldPrice string, newPrice string) {
	from := ""
	pass := ""
	to := ""

	subject := "Subject: TCGPlayer Price Guide bot!\n"

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	html := fmt.Sprintf("<html><body><h1>%s</h1><img src=\"%s\"/><h1>new price: %s</h1><h1>old price: %s</h1></body></html>", title, img, newPrice, oldPrice)

	msg := []byte(subject + mime + html)

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}

	log.Print("sent \n")
}

func telegramBot(title string, img string, oldPrice string, newPrice string) {
	apiToken := ""
	chatId := ""
	message := fmt.Sprintf("%s\nnew price: <b>%s</b>\nold price: <b>%s</b>", title, newPrice, oldPrice)
	siteUrl := fmt.Sprintf("https://api.telegram.org/bot%s/sendPhoto", apiToken)

	data := url.Values{}
	data.Add("chat_id", chatId)
	data.Add("parse_mode", "HTML")
	data.Add("caption", message)
	data.Add("photo", img)
	fmt.Println(data.Encode())

	siteUrl = siteUrl + "?" + data.Encode()
	fmt.Println(siteUrl)

	client := &http.Client{}
	req, err := http.NewRequest("GET", siteUrl, nil)
	if err != nil {
		log.Println(err)
		return
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return
	}

	f, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return
	}
	resp.Body.Close()
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(string(f))

}

func main() {

	if len(os.Args) < 3 {
		fmt.Println("please enter url")
		os.Exit(0)
	}

	listName = os.Args[1]
	listUrl = os.Args[2]

	for {

		if !fileExists(listName + ".json") {
			err := ioutil.WriteFile(listName+".json", nil, 0644)
			if err != nil {
				panic(err)
			}
		}

		b, err := ioutil.ReadFile(listName + ".json")
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal(b, &globalItems)
		if err != nil {
			globalItems = nil
		}

		fmt.Println("requesting data...")
		Scrape()
		fmt.Println("sleeping for 12 Hours...")
		time.Sleep(12 * time.Hour)
	}
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
