package dto

type NewPackageDTO struct {
	Name  string `json:"name" form:"name" validate:"required,min=3"`
	Price int64  `json:"price" form:"price" validate:"required,gt=0"`
	Type  string `json:"type" form:"type" validate:"required"`
}
