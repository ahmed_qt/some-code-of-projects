package models

import "gopkg.in/guregu/null.v4/zero"

type AclGroup struct {
	Base
	Name       zero.String `gorm:"type:varchar(255);not null" json:"name" example:"content_creator"`
	Permission []AclItem   `gorm:"many2many:acl_group_items;" json:"permissions"`
}
