package models

import (
	"encoding/json"
	"gopkg.in/guregu/null.v4"
	"gopkg.in/guregu/null.v4/zero"
	"polypay/s3"
)

// User model
type User struct {
	Base
	Username    null.String `gorm:"size:255;uniqueindex;" json:"username"`
	Email       null.String `gorm:"size:255;uniqueindex;" json:"email"`
	FirstName   null.String `gorm:"size:255;" json:"firstName"`
	LastName    null.String `gorm:"size:255;" json:"lastName"`
	DisplayName null.String `gorm:"size:255;" json:"displayName"`

	VerificationCode null.String `json:"-"`
	EmailVerifiedAt  null.Time   `json:"-"`

	Description   null.String `gorm:"size:255" json:"description"`
	Phone         null.String `gorm:"size:15" json:"phone"`
	StreetAddress null.String `gorm:"size:255" json:"streetAddress"`
	City          null.String `gorm:"size:70" json:"city"`
	PostalCode    null.String `gorm:"size:10" json:"postalCode"`
	Country       null.String `gorm:"size:70" json:"country"`

	Password          string    `gorm:"size:255;not null" json:"-"`
	PermissionGroupID null.Int  `json:"permissionGroupID"`
	PermissionGroup   *AclGroup `validate:"-" gorm:"<-false;foreignKey:PermissionGroupID" json:"permissionGroup"`

	FacebookID null.String `gorm:"size:255;" json:"-"`
	GoogleID   null.String `gorm:"size:255;" json:"-"`

	ProfilePicture null.String `gorm:"type:text" validate:"required"  json:"profilePicture"`
	ProfileCover   null.String `gorm:"type:text" validate:"required"  json:"profileCover"`

	IsContentCreator zero.Bool `gorm:"default:false" json:"isContentCreator"`

	// channel fields
	ChannelName             null.String `json:"channelName"`
	ChannelSelfIntroduction null.String `json:"channelSelfIntroduction"`
	ChannelWebsite          null.String `json:"channelWebsite"`
	ChannelPicture          null.String `gorm:"type:text" validate:"required"  json:"channelPicture"`
	ChannelCover            null.String `gorm:"type:text" validate:"required"  json:"channelCover"`

	StripeAccountID null.String `gorm:"type:text" json:"stripeAccountID"`
}

func (u *User) LoadPictures() error {
	if u.ProfilePicture.Valid {
		url, err := s3.GenerateS3SignedUrl(u.ProfilePicture.String, "avatars")
		if err != nil {
			return err
		}
		u.ProfilePicture = url
	}

	if u.ProfileCover.Valid {
		url, err := s3.GenerateS3SignedUrl(u.ProfileCover.String, "covers")
		if err != nil {
			return err
		}
		u.ProfileCover = url
	}

	if u.ChannelPicture.Valid {
		url, err := s3.GenerateS3SignedUrl(u.ChannelPicture.String, "channels_avatars")
		if err != nil {
			return err
		}
		u.ChannelPicture = url
	}

	if u.ChannelCover.Valid {
		url, err := s3.GenerateS3SignedUrl(u.ChannelCover.String, "channels_covers")
		if err != nil {
			return err
		}
		u.ChannelCover = url
	}

	return nil
}

func (u User) MarshalJSON() ([]byte, error) {
	u.LoadPictures()

	type Alias User
	res := &struct {
		*Alias
	}{Alias: (*Alias)(&u)}

	return json.Marshal(res)
}
