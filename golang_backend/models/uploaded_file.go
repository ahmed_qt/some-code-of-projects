package models

import (
	"encoding/json"
	"gopkg.in/guregu/null.v4"
	"polypay/s3"
)

type UploadedFile struct {
	Base

	UserID null.Int `json:"userID"`
	User   *User    `validate:"-" gorm:"<-false;foreignKey:UserID" json:"user"`

	File null.String `gorm:"type:text" validate:"required"  json:"file"`
}

func (u *UploadedFile) LoadFile() error {
	if u.File.Valid {
		url, err := s3.GenerateS3SignedUrl(u.File.String, "files")
		if err != nil {
			return err
		}
		u.File = url
	}
	return nil
}

func (u UploadedFile) MarshalJSON() ([]byte, error) {
	u.LoadFile()

	type Alias UploadedFile
	res := &struct {
		*Alias
	}{Alias: (*Alias)(&u)}

	return json.Marshal(res)
}
