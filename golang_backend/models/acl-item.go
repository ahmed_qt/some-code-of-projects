package models

import "gopkg.in/guregu/null.v4/zero"

type AclItem struct {
	Base
	Permission zero.String `gorm:"type:varchar(255);not null" json:"permission" example:"prm_any"`
	Category   zero.String `gorm:"type:varchar(255);not null" json:"-"`
}
