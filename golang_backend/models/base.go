package models

import (
	"gorm.io/gorm"
	"time"
)

type Base struct {
	ID int64 `gorm:"primarykey;type:int(10);autoIncrement;" json:"id"`
	TimestampsBase
}

type TimestampsBase struct {
	CreatedAt time.Time      `gorm:"type:datetime" json:"createdAt"`
	UpdatedAt time.Time      `gorm:"type:datetime" json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"type:datetime;index" json:"deletedAt"`
}
