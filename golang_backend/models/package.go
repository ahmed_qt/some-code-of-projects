package models

import (
	"gopkg.in/guregu/null.v4"
)

type Package struct {
	Base
	Name  null.String `gorm:"size:255;" json:"name"`
	Price null.Int    `json:"price"`

	UserID null.Int `json:"userID"`
	User   *User    `validate:"-" gorm:"<-false;foreignKey:UserID" json:"user"`

	Type            null.String `json:"type"`
	StripePriceID   null.String `json:"stripePriceID"`
	StripeProductID null.String `json:"stripeProductID"`
}
