package models

import (
	"fmt"
	"log"
	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	dbConn *gorm.DB
)

func InitDB() *gorm.DB {
	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))

	dbConn, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("failed to open connection to db: %v\n", err)
	}

	return dbConn
}

// GetDB ... return the DB connection
func GetDB() (*gorm.DB, error) {
	if dbConn == nil {
		return nil, fmt.Errorf("GetDB: dbSession == nil unexpectedly")
	}

	return dbConn, nil
}

// Ping is used to check the heartbeat of the database
func Ping() error {
	sqlDB, err := dbConn.DB()
	if err != nil {
		return err
	}

	return sqlDB.Ping()
}

// Close is used to close db
// SHOULD ONLY BE CALLED IN MAIN!!!
func Close() error {
	sqlDB, err := dbConn.DB()
	if err != nil {
		return err
	}

	return sqlDB.Close()
}
