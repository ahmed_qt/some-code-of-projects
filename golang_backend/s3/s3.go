package s3

import (
	"bytes"
	"fmt"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
	_ "github.com/joho/godotenv/autoload"
	"gopkg.in/guregu/null.v4"
)

var (
	sess *session.Session
)

func InitSESS() {
	var err error
	sess, err = session.NewSession(&aws.Config{
		Region:      aws.String(os.Getenv("AWS_S3_REGION")),
		Credentials: credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY"), os.Getenv("AWS_SECRET_KEY"), ""),
		Endpoint:    aws.String(""), // for testing using linode server
	})

	if err != nil {
		panic(err)
	}

}

func GetSESS() *session.Session {
	return sess
}

func Upload(fileHeader *multipart.FileHeader, bucketFolder string) (null.String, error) {
	f, _ := fileHeader.Open()
	var size int64 = fileHeader.Size

	buffer := make([]byte, size)
	f.Read(buffer)
	defer f.Close()

	uuid, err := uuid.NewRandom()
	if err != nil {
		return null.StringFromPtr(nil), err
	}

	filename := uuid.String() + filepath.Ext(fileHeader.Filename)

	S3Bucket := os.Getenv("AWS_S3_BUCKET")
	sess := GetSESS()
	if err != nil {
		return null.StringFromPtr(nil), err
	}

	resp, err := s3.New(sess).PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(S3Bucket),
		Key:                aws.String(bucketFolder + "/" + filename),
		ACL:                aws.String("private"), //https://docs.aws.amazon.com/AmazonS3/latest/userguide/acl-overview.html#CannedACL
		Body:               bytes.NewReader(buffer),
		ContentLength:      aws.Int64(size),
		ContentType:        aws.String(http.DetectContentType(buffer)),
		ContentDisposition: aws.String("attachment"),
	})
	if err != nil {
		return null.StringFromPtr(nil), err
	}
	output := filename

	if err != nil {
		return null.StringFromPtr(nil), err
	}

	fmt.Println(awsutil.Prettify(resp))

	return null.StringFrom(output), nil
}

func GenerateS3SignedUrl(filename, bucketFolder string) (null.String, error) {
	S3Bucket := os.Getenv("AWS_S3_BUCKET")
	sess := GetSESS()

	// Create S3 service client
	svc := s3.New(sess)

	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(S3Bucket),
		Key:    aws.String(bucketFolder + "/" + filename),
	})

	url, err := req.Presign(2 * time.Hour)
	if err != nil {
		return null.StringFromPtr(nil), err
	}

	return null.StringFrom(url), nil
}
