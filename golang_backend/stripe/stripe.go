package stripe

import (
	"github.com/stripe/stripe-go/v73"
	"os"
)

func InitStripe() {
	stripe.Key = os.Getenv("STRIPE_KEY")
}

func IsStripeAccountVerified(a *stripe.Account) bool {
	if a.ChargesEnabled && a.PayoutsEnabled {
		return true
	}
	return false
}
