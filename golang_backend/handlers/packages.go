package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/stripe/stripe-go/v73"
	"github.com/stripe/stripe-go/v73/account"
	"github.com/stripe/stripe-go/v73/price"
	"github.com/stripe/stripe-go/v73/product"
	"gopkg.in/guregu/null.v4"
	"net/http"
	"polypay/dto"
	"polypay/models"
	"polypay/utils"
)

func PackagesView(c echo.Context) error {
	stripeLinked := false
	user := c.Get("user").(models.User)
	if user.StripeAccountID.Valid {
		// get account
		_, err := account.GetByID(user.StripeAccountID.String, nil)
		if err == nil {
			stripeLinked = true
		}
	}

	if !stripeLinked {
		return c.Redirect(http.StatusFound, "/partner")
	}

	db, err := models.GetDB()
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	pkgs := []models.Package{}
	if err := db.Where("user_id = ?", c.Get("user_id").(int64)).Find(&pkgs).Error; err != nil {
		fmt.Println(err)
		return c.Render(http.StatusUnauthorized, "error.html", map[string]any{
			"error": err,
		})
	}

	return c.Render(http.StatusOK, "packages.html", map[string]any{"packages": pkgs})
}

func NewPackageView(c echo.Context) error {
	stripeLinked := false
	user := c.Get("user").(models.User)
	if user.StripeAccountID.Valid {
		// get account
		_, err := account.GetByID(user.StripeAccountID.String, nil)
		if err == nil {
			stripeLinked = true
		}
	}

	if !stripeLinked {
		return c.Redirect(http.StatusFound, "/partner")
	}

	return c.Render(http.StatusOK, "new_package.html", map[string]any{})
}

func CreatePackage(c echo.Context) error {
	newPackageDTO := dto.NewPackageDTO{}

	if err := c.Bind(&newPackageDTO); err != nil {
		return c.Render(http.StatusBadRequest, "error.html", map[string]any{
			"error": err,
		})
	}

	if err := utils.GetValidator().Struct(newPackageDTO); err != nil {
		return c.Render(http.StatusBadRequest, "new_package.html", map[string]any{
			"error": err,
		})
	}

	db, err := models.GetDB()
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	user := c.Get("user").(models.User)

	productParams := &stripe.ProductParams{
		Name: stripe.String(newPackageDTO.Name),
	}
	productParams.SetStripeAccount(user.StripeAccountID.String)

	p, _ := product.New(productParams)

	var interval string
	var intervalCount int64

	switch newPackageDTO.Type {
	case "1month":
		interval = "month"
		intervalCount = 1
	case "2month":
		interval = "month"
		intervalCount = 2
	case "3month":
		interval = "month"
		intervalCount = 3
	default:
		interval = "month"
		intervalCount = 1
	}
	priceParams := &stripe.PriceParams{
		Currency: stripe.String(string(stripe.CurrencyUSD)),
		Product:  stripe.String(p.ID),
		Recurring: &stripe.PriceRecurringParams{
			Interval:      stripe.String(interval),
			IntervalCount: stripe.Int64(intervalCount),
		},
		UnitAmount: stripe.Int64(newPackageDTO.Price * 100),
	}
	priceParams.SetStripeAccount(user.StripeAccountID.String)

	np, _ := price.New(priceParams)
	fmt.Println(np)

	// save package to db
	pkg := models.Package{}
	pkg.Name = null.StringFrom(newPackageDTO.Name)
	pkg.Price = null.IntFrom(newPackageDTO.Price)
	pkg.Type = null.StringFrom(fmt.Sprintf("%d%s", intervalCount, interval))
	pkg.UserID = null.IntFrom(c.Get("user_id").(int64))
	pkg.StripeProductID = null.StringFrom(p.ID)
	pkg.StripePriceID = null.StringFrom(np.ID)

	if err := db.Create(&pkg).Error; err != nil {
		return c.Render(http.StatusUnauthorized, "new_packages.html", map[string]any{
			"error": err,
		})
	}

	return c.Redirect(http.StatusFound, "/packages")
}
