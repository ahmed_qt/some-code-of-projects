package handlers

import (
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"net/http"
	"polypay/dto"
	"polypay/middlewarers"
	"polypay/models"
	"polypay/utils"
)

func LoginView(c echo.Context) error {
	if middlewarers.IsAuthenticated(c) {
		return c.Redirect(http.StatusFound, "home")
	}

	return c.Render(http.StatusOK, "login.html", nil)
}

func Login(c echo.Context) error {
	userDTO := dto.UserDTO{}

	if err := c.Bind(&userDTO); err != nil {
		return c.Render(http.StatusBadRequest, "error.html", map[string]any{
			"error": err,
		})
	}

	if err := utils.GetValidator().Struct(userDTO); err != nil {
		return c.Render(http.StatusBadRequest, "login.html", map[string]any{
			"error": err,
		})
	}

	db, err := models.GetDB()
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	user := models.User{}
	if err := db.Where("email = ?", userDTO.Email).First(&user).Error; err != nil {
		return c.Render(http.StatusUnauthorized, "login.html", map[string]any{
			"error": "invalid credentials",
		})
	}

	if !user.IsContentCreator.Bool {
		return c.Render(http.StatusUnauthorized, "login.html", map[string]any{
			"error": "invalid credentials",
		})
	}

	if !utils.ComparePasswordAndHash(userDTO.Password, user.Password) {
		return c.Render(http.StatusUnauthorized, "login.html", map[string]any{
			"error": "invalid credentials",
		})
	}

	sess, _ := session.Get("session", c)
	sess.Values["user_id"] = user.ID
	if err := sess.Save(c.Request(), c.Response()); err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	return c.Redirect(http.StatusFound, "home")
}

func Logout(c echo.Context) error {
	if !middlewarers.IsAuthenticated(c) {
		return c.Redirect(http.StatusFound, "login")
	}

	sess, _ := session.Get("session", c)
	delete(sess.Values, "user_id")
	_ = sess.Save(c.Request(), c.Response())
	return c.Redirect(http.StatusFound, "login")
}
