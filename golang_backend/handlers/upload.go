package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gopkg.in/guregu/null.v4"
	"net/http"
	"polypay/models"
	"polypay/s3"
	"polypay/utils"
)

func UploadView(c echo.Context) error {
	db, err := models.GetDB()
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	uploadedFiles := []models.UploadedFile{}
	if err := db.Where("user_id = ?", c.Get("user_id").(int64)).Find(&uploadedFiles).Error; err != nil {
		fmt.Println(err)
		return c.Render(http.StatusUnauthorized, "error.html", map[string]any{
			"error": err,
		})
	}

	return c.Render(http.StatusOK, "upload.html", map[string]any{"uploadedFiles": uploadedFiles})
}

func NewUploadView(c echo.Context) error {
	return c.Render(http.StatusOK, "new_upload.html", map[string]any{})
}

func Upload(c echo.Context) error {
	file, err := c.FormFile("file")
	if err != nil {
		return c.Render(http.StatusBadRequest, "error.html", map[string]any{
			"error": err,
		})
	}

	if !utils.ValidateMimeType(file, []string{"video/mp4"}) {
		return c.Render(http.StatusBadRequest, "new_upload.html", map[string]any{
			"error": "invalid file mimetype != video/mp4",
		})
	}

	// upload to s3
	output, err := s3.Upload(file, "videos")
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	db, err := models.GetDB()
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	uploadedFile := models.UploadedFile{}
	uploadedFile.File = output
	uploadedFile.UserID = null.IntFrom(c.Get("user_id").(int64))

	if err := db.Create(&uploadedFile).Error; err != nil {
		return c.Render(http.StatusUnauthorized, "new_upload.html", map[string]any{
			"error": err,
		})
	}

	return c.Redirect(http.StatusFound, "/upload")
}
