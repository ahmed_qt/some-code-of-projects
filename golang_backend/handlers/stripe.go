package handlers

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

func StripeRefresh(c echo.Context) error {
	return PartnerLink(c)
}

func StripeReturn(c echo.Context) error {
	return c.Redirect(http.StatusFound, "/partner")
}
