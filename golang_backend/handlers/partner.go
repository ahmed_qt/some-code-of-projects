package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/stripe/stripe-go/v73"
	"github.com/stripe/stripe-go/v73/account"
	"github.com/stripe/stripe-go/v73/accountlink"
	"github.com/stripe/stripe-go/v73/loginlink"
	"gopkg.in/guregu/null.v4"
	"net/http"
	"os"
	"polypay/models"
)

func PartnerView(c echo.Context) error {
	stripeVerified := false
	user := c.Get("user").(models.User)
	if user.StripeAccountID.Valid {
		// get account
		a, _ := account.GetByID(user.StripeAccountID.String, nil)
		if a.ChargesEnabled && a.PayoutsEnabled {
			stripeVerified = true
		}
	}

	return c.Render(http.StatusOK, "partner.html", map[string]any{"stripeVerified": stripeVerified})
}

func PartnerLink(c echo.Context) error {
	// create stripe connect onboard account then update it the id in mysql users table
	// return a link to stripe for the user to complete his/her info

	db, err := models.GetDB()
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	user := c.Get("user").(models.User)

	if !user.StripeAccountID.Valid {
		params := &stripe.AccountParams{Type: stripe.String(string(stripe.AccountTypeExpress))}
		result, _ := account.New(params)
		user.StripeAccountID = null.StringFrom(result.ID)

		if err := db.Updates(&user).Error; err != nil {
			return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
				"error": err,
			})
		}
	}

	// create stripe account link url
	params := &stripe.AccountLinkParams{
		Account:    stripe.String(user.StripeAccountID.String),
		RefreshURL: stripe.String(fmt.Sprintf("%s/stripe/refresh", os.Getenv("APP_URL"))),
		ReturnURL:  stripe.String(fmt.Sprintf("%s/stripe/return", os.Getenv("APP_URL"))),
		Type:       stripe.String("account_onboarding"),
	}
	accountLinkResult, _ := accountlink.New(params)

	return c.Redirect(http.StatusFound, accountLinkResult.URL)
}

func PartnerLogin(c echo.Context) error {
	user := c.Get("user").(models.User)

	if !user.StripeAccountID.Valid {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": "account is not linked with stripe",
		})
	}

	_, err := account.GetByID(user.StripeAccountID.String, nil)
	if err != nil {
		return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
			"error": err,
		})
	}

	// create login url for user
	params := &stripe.LoginLinkParams{
		Account: stripe.String(user.StripeAccountID.String),
	}
	ll, _ := loginlink.New(params)

	return c.Redirect(http.StatusFound, ll.URL)

}
