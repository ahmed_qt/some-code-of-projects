package handlers

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

func ProfileView(c echo.Context) error {
	return c.Render(http.StatusOK, "profile.html", map[string]any{})
}
