package utils

import (
	"encoding/json"
	"fmt"
)

func Print(obj interface{}) {
	str, _ := json.MarshalIndent(&obj, "", "\t")
	fmt.Println(string(str))
}
