package utils

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"mime/multipart"
	"net/http"
)

var validate *validator.Validate

func InitValidator() {
	validate = validator.New()
}

func GetValidator() *validator.Validate {
	return validate
}

func ValidateMimeType(fileHeader *multipart.FileHeader, mimetypes []string) bool {
	f, err := fileHeader.Open()
	if err != nil {
		return false
	}

	size := fileHeader.Size
	buffer := make([]byte, size)
	if _, err := f.Read(buffer); err != nil {
		return false
	}
	defer f.Close()

	mimetype := http.DetectContentType(buffer)
	fmt.Println(mimetype)

	for _, s := range mimetypes {
		// match value with struct filed tag
		if s == mimetype {
			return true
		}
	}

	return false
}
