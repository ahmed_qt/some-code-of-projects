package middlewarers

import (
	"fmt"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"net/http"
	"polypay/models"
)

func CheckUserAuth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		currSess, _ := session.Get("session", c)
		if userId, ok := currSess.Values["user_id"].(any); ok {
			c.Set("user_id", userId)
			user := models.User{}
			db, err := models.GetDB()
			if err != nil {
				return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
					"error": err,
				})
			}

			if err = db.Where("id = ?", userId).First(&user).Error; err != nil {
				return c.Render(http.StatusInternalServerError, "error.html", map[string]any{
					"error": err,
				})
			}
			user.LoadPictures()
			c.Set("user", user)
			fmt.Println("User is currently connected with id", userId)
			return next(c)
		}
		// echo.NewHTTPError(http.StatusUnauthorized, "test")
		return c.Redirect(http.StatusFound, "/login")
	}
}

func IsAuthenticated(c echo.Context) bool {
	currSess, _ := session.Get("session", c)
	if _, ok := currSess.Values["user_id"].(any); ok {
		return true
	}
	return false
}
