### Start
```go
?> go run .
```

### Migrate
```go
?> go run . -db_migrate
```

### Url
```shell
http://localhost:1323/
```
