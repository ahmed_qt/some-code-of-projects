## Installation

```bash
$ npm install
```



## Database configuration
edit TypeOrmModule options in `src/app.module.ts`



## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## References for used tools
- https://docs.nestjs.com/
- https://typeorm.io/
- https://www.typescriptlang.org/