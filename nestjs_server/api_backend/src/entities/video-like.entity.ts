import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('video_likes')
export class VideoLike {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  video_id: number;
}
