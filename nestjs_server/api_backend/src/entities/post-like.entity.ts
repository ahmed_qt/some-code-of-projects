import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('post_likes')
export class PostLike {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  post_id: number;
}
