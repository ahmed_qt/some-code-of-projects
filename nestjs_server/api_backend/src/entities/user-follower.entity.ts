import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users_followers')
export class UserFollower {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  follower_id: number;
}
