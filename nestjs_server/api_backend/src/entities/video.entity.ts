import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum VideoPrivacy {
  PUBLIC = 'public',
  PRIVATE = 'private',
}

@Entity('videos')
export class Video {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column({ length: 50 })
  video_title: string;

  @Column({ length: 255 })
  video_description: string;

  @Column({ length: 255 })
  video_file: string;

  @Column({ type: 'enum', enum: VideoPrivacy, default: VideoPrivacy.PUBLIC })
  privacy: string;

  @Column({ length: 50 })
  city: string;

  @Column({ length: 50 })
  state: string;

  @Column({ length: 50 })
  country: string;

  @CreateDateColumn({ name: 'created_at' })
  created_at: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updated_at: Date;
}
