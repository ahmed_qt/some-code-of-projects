import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('post_categories')
export class PostCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  post_id: number;

  @Column()
  category_id: number;
}
