import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('post_images')
export class CommentLike {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  post_id: number;

  @Column({ length: 255 })
  image_file: string;
}
