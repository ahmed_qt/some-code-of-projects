import { Exclude } from 'class-transformer';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column({ unique: true })
  email: string;

  @Column()
  @Exclude({ toPlainOnly: true })
  password: string;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column()
  birth_date: Date;

  @Column({ nullable: true })
  profile_picture: string;

  @Column('text', { nullable: true })
  description: string;

  @Column({ length: 20 })
  phone: string;

  @Column({ default: false })
  is_email_confirmed: boolean;
}
