import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('video_categories')
export class VideoCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  video_id: number;

  @Column()
  category_id: number;
}
