import {
  BadGatewayException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { EmailVerification } from 'src/entities/emailverification.entity';
import { Repository } from 'typeorm';
import { PasswordReset } from 'src/entities/password-resets.entity';
import { EmailService } from 'src/email/email.service';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { ResetPasswordCheckDto } from './dto/reset-password-check.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly emailService: EmailService,
    @InjectRepository(EmailVerification)
    private readonly emailVerificationRepository: Repository<EmailVerification>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(PasswordReset)
    private readonly passwordResetRepository: Repository<PasswordReset>,
  ) {}

  async validateUser(email: string, password: string): Promise<User> {
    const user = await this.usersService.findOne({ email });
    if (!user) {
      return null;
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return null;
    }

    return user;
  }

  async verifyAccount(code: string) {
    try {
      // check if code exist in the database
      const emailVerification = await this.emailVerificationRepository.findOne({
        code,
      });

      if (!emailVerification) {
        throw new BadGatewayException('Error verify account');
      }

      // check if user exist
      const user = await this.userRepository.findOne({
        email: emailVerification.email,
      });

      if (!user) {
        throw new BadGatewayException('Error verify account');
      }

      // set user to confirmed
      user.is_email_confirmed = true;
      this.userRepository.save(user);

      // remove emailVerification row
      this.emailVerificationRepository.delete(emailVerification);
      return { status: 1 };
    } catch (error) {
      throw new BadGatewayException('Error verify account');
    }
  }

  async sendEmailForgotPassword(email: string) {
    try {
      // check if user exist
      const user = await this.userRepository.findOne({
        email,
      });

      if (!user) {
        throw new BadGatewayException('User does not exist');
      }

      // generate random pin to use for password  reset
      const pin = Math.random().toString().substr(2, 6);

      // add passwordReset row to database
      const passwordReset = new PasswordReset();
      passwordReset.email = email;
      passwordReset.code = pin;
      this.passwordResetRepository.save(passwordReset);

      // send email
      await this.emailService.sendEmail({
        to: passwordReset.email, // list of receivers
        from: 'register@naaom.net', // sender address
        subject: `password reset`, // Subject line
        text: '', // plaintext body
        html: `use: <b>${passwordReset.code}</b> to get back to your Polymath account`, // HTML body content
      });
      return { status: 1 };
    } catch (error) {
      console.error(error);
      throw new BadGatewayException('User does not exist');
    }
  }

  async resetPasswordCheck(dto: ResetPasswordCheckDto): Promise<boolean> {
    try {
      const passwordReset = await this.passwordResetRepository.findOne({
        code: dto.code,
        email: dto.email,
      });

      if (!passwordReset) {
        throw new UnauthorizedException('Incorrect pin');
      }

      return true;
    } catch (error) {
      throw new BadGatewayException('Incorrect pin');
    }
  }

  async resetPassword(dto: ResetPasswordDto) {
    try {
      // check if pin is valid
      const checkPin = await this.resetPasswordCheck(dto);

      // check if password and confirm-password matches
      if (dto.password != dto.confirm_password) {
        throw new BadGatewayException('Password mismatch');
      }

      const user = await this.userRepository.findOne({
        email: dto.email,
      });

      if (!user) {
        throw new BadGatewayException('Password reset failed');
      }

      user.password = await bcrypt.hash(dto.password, 12);
      await this.userRepository.save(user);

      await this.passwordResetRepository.delete({ email: dto.email });

      return { status: 1 };
    } catch (error) {
      throw new BadGatewayException('Password reset failed');
    }
  }

  async login(user: User) {
    const payload = {
      email: user.email,
      sub: user.id,
    };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
