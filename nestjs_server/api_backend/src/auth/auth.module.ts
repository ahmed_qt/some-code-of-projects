import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'src/users/users.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './passport/jwt.strategy';
import { LocalStrategy } from './passport/local.strategy';
import * as dotenv from 'dotenv';
import { EmailVerification } from 'src/entities/emailverification.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/entities/user.entity';
import { PasswordReset } from 'src/entities/password-resets.entity';
import { EmailModule } from 'src/email/email.module';
dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forFeature([EmailVerification, User, PasswordReset]),
    UsersModule,
    PassportModule,
    EmailModule,
    JwtModule.register({
      secret: String(process.env.JWT_SECRET),
      signOptions: { expiresIn: '1d' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
