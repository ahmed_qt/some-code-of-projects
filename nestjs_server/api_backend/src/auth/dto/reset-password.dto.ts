import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';
import { Match } from '../../common/decorator/match.decorator';

export class ResetPasswordDto {
  @IsEmail()
  email: string;

  @IsString()
  @MinLength(4)
  @MaxLength(20)
  password: string;

  @IsString()
  @MinLength(4)
  @MaxLength(20)
  @Match('password', { message: 'password mismatch' })
  confirm_password: string;

  @IsString()
  code: string;
}
