import { IsEmail, IsString } from 'class-validator';

export class ResetPasswordCheckDto {
  @IsEmail()
  email: string;

  @IsString()
  code: string;
}
