import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from '../common/dto/create-user.dto';
import { LocalAuthGuard } from './passport/local-auth.guard';
import { AuthService } from './auth.service';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { ResetPasswordCheckDto } from './dto/reset-password-check.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {}

  @Post('register')
  async register(@Body() dto: CreateUserDto) {
    const hashPassword = await bcrypt.hash(dto.password, 12);
    dto.password = hashPassword;
    return this.usersService.create(dto);
  }

  @Get('verify/:code')
  async verifyAccount(@Param() params) {
    return this.authService.verifyAccount(params.code);
  }

  @Get('forgot-password/:email')
  async sendEmailForgotPassword(@Param() params) {
    return this.authService.sendEmailForgotPassword(params.email);
  }

  @Post('reset-password/check')
  async resetPasswordCheck(@Body() dto: ResetPasswordCheckDto) {
    const resetPassword = await this.authService.resetPasswordCheck(dto);
    if (resetPassword) {
      return { status: 1 };
    }
    return { status: -1 };
  }

  @Post('reset-password')
  async resetPassword(@Body() dto: ResetPasswordDto) {
    return this.authService.resetPassword(dto);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }
}
