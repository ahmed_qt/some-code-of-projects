import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import * as dotenv from 'dotenv';
import { BullModule } from '@nestjs/bull';
import { EmailVerification } from 'src/entities/emailverification.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forFeature([EmailVerification]),
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST,
        port: Number.parseInt(process.env.REDIS_PORT),
      },
    }),
    BullModule.registerQueue({
      name: 'mail-queue',
    }),
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
