import { BadGatewayException, Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EmailVerification } from 'src/entities/emailverification.entity';

@Injectable()
export class EmailService {
  constructor(
    @InjectRepository(EmailVerification)
    private readonly emailVerificationRepository: Repository<EmailVerification>,
    @InjectQueue('mail-queue') private readonly mailQueue: Queue,
  ) {}

  async sendEmail(data: any): Promise<boolean> {
    try {
      await this.mailQueue.add('send-email', data);
      return true;
    } catch (error) {
      console.error(error);
      throw new BadGatewayException('bad gateway');
    }
  }

  async sendEmailVerification(email: string): Promise<boolean> {
    try {
      // generate random pin to use for confirming account
      const pin = Math.random().toString().substr(2, 6);

      // add email verification row to database
      const emailVerification = new EmailVerification();
      emailVerification.email = email;
      emailVerification.code = pin;
      this.emailVerificationRepository.save(emailVerification);

      // add email to queue
      this.mailQueue.add('send-email', {
        to: emailVerification.email, // list of receivers
        from: '', // sender address
        subject: `${emailVerification.code} is your verification code`, // Subject line
        text: '', // plaintext body
        html: `To verify your account, enter this code in Polymath:\n<b>${emailVerification.code}</b>`, // HTML body content
      });
      return true;
    } catch (error) {
      throw new BadGatewayException('bad gateway');
    }
  }
}
