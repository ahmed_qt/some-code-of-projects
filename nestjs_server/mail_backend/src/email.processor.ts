import { Processor, Process, InjectQueue } from '@nestjs/bull';
import { Job, Queue } from 'bull';
import { MailerService } from '@nestjs-modules/mailer';

@Processor('mail-queue')
export class EmailProcessor {
  constructor(
    @InjectQueue('mail-queue') private readonly emailQueue: Queue,
    private readonly mailerService: MailerService,
  ) {}

  @Process('send-email')
  async sendEmailVerification(job: Job<unknown>) {
    console.log('send-email', job.data);
    return await this.mailerService.sendMail(job.data);
  }
}
