import {writable} from "svelte/store";
import jwt_decode from 'jwt-decode'
export const authStore = writable(getStorageUser())

export let authInfo = writable(null);

function getStorageUser() {
    return localStorage.getItem('access') ? JSON.parse(localStorage.getItem('access')) : {
        access: null,
    }
}

export function setStorageUser(access) {
    localStorage.setItem('access', JSON.stringify(access))
}

export function setUser(access) {
    authStore.set({access: access})
}

export function logout() {
    localStorage.clear();
    authStore.set({access: null})
}

export const isAuthenticated = writable(isAuthenticatedCheck())

function isAuthenticatedCheck() {
    let access = localStorage.getItem('access')
    return !!(access);
    // return !!(access && jwt_decode(access).exp >= Date.now() / 1000);
}


export default authStore;
