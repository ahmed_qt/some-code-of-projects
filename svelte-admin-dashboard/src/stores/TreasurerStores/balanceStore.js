import {writable} from "svelte/store";
import {getBalance} from "../../backend/treasurerActions";
import {authInfo} from '../auth';
export const balance = writable();



setInterval(() => {
    getBalance().then(response => {
        balance.set(response.data.balance)
    }).catch(error => console.log(error))
    
}, 60 * 1000); // 60 * 1000 milsec