import {writable} from "svelte/store";
import {getExpenseReasons} from "../../backend/treasurerActions";

export const expenseReasons = writable([]);


getExpenseReasons().then((items) => {
    if (items) {
        expenseReasons.set(items.data);
    }
}).catch((error) => {
    console.log(error)
});



