export const defaultURL = '';

export const baseURL = defaultURL + 'api/';
export const loginURL = baseURL + 'auth/login';
export const userInfoURL = baseURL + 'auth/me';

// Treasurer
export const vaultBalanceURL = baseURL + 'treasurer/vault_balance';
export const treasurerProfilesURL = baseURL + 'treasurer/profiles';
export const listAllExpenseTransactionsURL = baseURL + 'treasurer/list_all_expense_transactions';
export const createExpenseTransactionURL = baseURL + 'treasurer/create_expense_transaction';
export const updateExpenseTransactionURL = baseURL + 'treasurer/update_expense_transaction/transaction';
export const expenseReasonsURL = baseURL + 'treasurer/list_expense_reasons';

export const listAllPayrollSlipsURL = baseURL + 'treasurer/list_all_payroll_slips';
export const markPayrollSlipsReceivedURL = baseURL + 'treasurer/mark_payroll_received/slip/';
export const listPayrollUrl = baseURL + 'treasurer/list_payroll';

export const listPayrollSlipsURL = baseURL + 'treasurer/list_payroll_slips/payroll/';
export const createPayrollSlipsURL = baseURL + 'treasurer/create_payroll_slips';
export const listPayrollDeductionsURL = baseURL + 'treasurer/list_payroll_deductions/payroll/';
export const createPayrollDeductionsURL = baseURL + 'treasurer/create_payroll_deduction';

export const updatePayrollDeductionsURL = baseURL + 'treasurer/update_payroll_deduction/deduction/';
export const deletePayrollDeductionsURL = baseURL + 'treasurer/delete_payroll_deduction/deduction/';

// ADMIN
export const adminAddVaultBalanceURL = baseURL + 'admin/add_vault_balance';
export const adminWithdrawVaultBalanceURL = baseURL + 'admin/withdraw_vault_balance';
export const adminVaultBalanceURL = baseURL + 'admin/vault_balance';

export const adminListRolesURL = baseURL + 'admin/roles';
export const adminHolisticViewURL = baseURL + 'admin/holistic_view';
export const adminListDriversURL = baseURL + 'admin/list_drivers';
export const adminListMerchantsURL = baseURL + 'admin/list_merchants';
export const adminCreateProfileURL = baseURL + 'admin/create_profile';
export const adminDriverViewURL = baseURL + 'admin/driver/';
export const adminMerchantViewURL = baseURL + 'admin/merchant/';




// DATA
export const dataCreateOrderURL = baseURL + 'data/create_order';
export const dataListDriversURL = baseURL + 'data/list_drivers';
export const dataListMerchantsURL = baseURL + 'data/list_merchants';
export const dataListDestinationsURL = baseURL + 'data/list_destinations';


// MERCHANT
export const merchantCreateOrderURL = baseURL + 'merchant/create_order';
export const merchantlistAllOrdersURL = baseURL + 'merchant/orders';
export const merchantGetOrderDetailsURL = baseURL + 'merchant/order/';
export const merchantDeleteOrderURL = baseURL + 'merchant/order/';
export const merchantListDestinationsURL = baseURL + 'merchant/list_destinations';
export const merchantListAllTransactionsURL = baseURL + 'merchant/transactions';


// STAFF
export const staffListAllOrdersURL = baseURL + 'staff/orders';
export const staffListNewOrdersURL = baseURL + 'staff/new_orders';
export const staffListStorageOrdersURL = baseURL + 'staff/storage_orders';
export const staffListExpressOrdersURL = baseURL + 'staff/express_orders';
export const staffGetOrderURL = baseURL + 'staff/order/';
export const staffListDriversURL = baseURL + 'staff/list_drivers';
export const staffListMerchantsURL = baseURL + 'staff/list_merchants';
export const staffListOrderStatusURL = baseURL + 'staff/list_order_status';
export const staffUpdateOrderURL = baseURL + 'staff/update_order';
export const staffAssignDriverURL = baseURL + 'staff/assign_driver';




export default defaultURL;