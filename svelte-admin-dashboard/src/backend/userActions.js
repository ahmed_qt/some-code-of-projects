import {loginURL, userInfoURL} from "../constants/URL";
import {setStorageUser, setUser, isAuthenticated} from "../stores/auth";
import axios from "axios";

export const loginUser = async ({username, password}) => {
    const response = await axios.post(
        loginURL,
        {username: username, password: password})
    if (response) {
        setupUser(response)
    }
    return response
}


function setupUser (response) {
    const {access} = response.data;
    const user = {access};

    setStorageUser(user);
    setUser(user);

}



export const getUserInfo = async () => {
    if (!localStorage.getItem('access')) {
        // navigate('/login');
    } else {
        const response = await axios.get(
            userInfoURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}