import axios from "axios";
import {navigate} from 'svelte-routing';
import { staffAssignDriverURL, staffGetOrderURL, staffListAllOrdersURL, staffListDriversURL, staffListExpressOrdersURL, staffListMerchantsURL, staffListNewOrdersURL, staffListOrderStatusURL, staffListStorageOrdersURL, staffUpdateOrderURL } from "../constants/URL";



export const getOrders = async (query) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffListAllOrdersURL + "?" + new URLSearchParams(query).toString(), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const getNewOrders = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffListNewOrdersURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getExpressOrders = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffListExpressOrdersURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getStorageOrders = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffListStorageOrdersURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getDrivers = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffListDriversURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getMerchants = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffListMerchantsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getStatus = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffListOrderStatusURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getOrder = async (order_id) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            staffGetOrderURL + `${order_id}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const updateOrder = async (data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            staffUpdateOrderURL, data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const assignDriver = async (data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            staffAssignDriverURL, data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}
