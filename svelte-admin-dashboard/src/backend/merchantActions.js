import axios from "axios";
import {navigate} from 'svelte-routing';
import { merchantCreateOrderURL, merchantGetOrderDetailsURL, merchantlistAllOrdersURL, merchantListAllTransactionsURL, merchantListDestinationsURL } from "../constants/URL";


export const getOrders = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            merchantlistAllOrdersURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const getOrder = async (order) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            merchantGetOrderDetailsURL + `${order}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const deleteOrder = async (order) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.delete(
            merchantGetOrderDetailsURL + `${order}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const createOrder = async (data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            merchantCreateOrderURL, data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}



export const getDestinations = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            merchantListDestinationsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getTransactions = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            merchantListAllTransactionsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}