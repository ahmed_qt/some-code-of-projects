import axios from "axios";
import {navigate} from 'svelte-routing';
import { adminAddVaultBalanceURL, adminHolisticViewURL, adminWithdrawVaultBalanceURL, adminVaultBalanceURL } from "../constants/URL";

export const addVaultBalance = async (amount) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            adminAddVaultBalanceURL,
            {amount: parseFloat(amount)}, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}
export const withdrawVaultBalance = async (amount) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            adminWithdrawVaultBalanceURL,
            {amount: parseFloat(amount)}, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const getAdminHolisticView = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            adminHolisticViewURL,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getVaultBalance = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            adminVaultBalanceURL,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}
