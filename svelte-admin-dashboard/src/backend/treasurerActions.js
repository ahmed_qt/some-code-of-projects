import {vaultBalanceURL, treasurerProfilesURL, expenseReasonsURL, defaultURL, listAllExpenseTransactionsURL, createExpenseTransactionURL, updateExpenseTransactionURL, createPayrollSlipsURL, listAllPayrollSlipsURL, listPayrollUrl, markPayrollSlipsReceivedURL, listPayrollSlipsURL, listPayrollDeductionsURL, createPayrollDeductionsURL, updatePayrollDeductionsURL, baseURL, deletePayrollDeductionsURL} from "../constants/URL";
import axios from "axios";
import {navigate} from 'svelte-routing';

export const getBalance = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            vaultBalanceURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getProfiles = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            treasurerProfilesURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const getExpenseTransactions= async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            listAllExpenseTransactionsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const getExpenseReasons = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            expenseReasonsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}



export const addBalance = async (profile_id, amount) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        if (parseInt(amount) <= 0) {
            console.error('amount is less or equal to zero');
            return;
        }
        const url = `${baseURL}treasurer/add_balance/profile/${profile_id}`
        const response = await axios.post(
            url,
            {
                amount: amount
            },
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const deductBalance = async (profile_id, amount) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        if (parseInt(amount) <= 0) {
            console.error('amount is less or equal to zero');
            return;
        }
        const url = `${baseURL}treasurer/deduct_balance/profile/${profile_id}`
        const response = await axios.post(
            url,
            {
                amount: amount
            },
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const settleAccount = async (profile_id) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const url = `${baseURL}treasurer/settle_account/profile/${profile_id}`
        const response = await axios.post(
            url,
            {
            },
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const createExpenseTransaction = async (data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            createExpenseTransactionURL,
            data,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const updateExpenseTransaction = async (transaction, data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            updateExpenseTransactionURL + '/' + transaction,
            data,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const createPayrollSlips = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            createPayrollSlipsURL,
            {},
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}



export const getAllPayrollSlips = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            listAllPayrollSlipsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getPayrollSlips = async (payroll) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            `${listPayrollSlipsURL}${payroll}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const getPayrolls = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            listPayrollUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const markPayrollReceived = async (slip) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            markPayrollSlipsReceivedURL + slip,
            {
            },
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const getPayrollDeductions= async (payroll) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            listPayrollDeductionsURL + payroll, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const createPayrollDeductions = async (data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            createPayrollDeductionsURL,
            data,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}



export const updatePayrollDeductions = async (deduction, data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            updatePayrollDeductionsURL + deduction,
            data,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}



export const deletePayrollDeductions = async (deduction) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.delete(
            deletePayrollDeductionsURL + `${deduction}`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}
