import axios from "axios";
import {navigate} from 'svelte-routing';
import { dataCreateOrderURL, dataListDestinationsURL, dataListDriversURL, dataListMerchantsURL } from "../constants/URL";

export const getDrivers = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            dataListDriversURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getMerchants = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            dataListMerchantsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

export const getDestinations = async () => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.get(
            dataListDestinationsURL, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}


export const createOrder = async (data) => {
    if (!localStorage.getItem('access')) {
        navigate('/login');
    } else {
        const response = await axios.post(
            dataCreateOrderURL,
            data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('access')).access
                }
            })
        return response
    }
}

