import { CoreMenu } from '@core/types';

export const menu: CoreMenu[] = [
  {
    id: 'home',
    title: 'Home',
    translate: 'MENU.HOME',
    type: 'item',
    icon: 'home',
    url: '/home'
  },
  {
    id: 'profiles',
    title: 'Profiles',
    type: 'item',
    icon: 'compass',
    url: '/profiles'
  },
  {
    id: 'chat',
    title: 'Chat',
    type: 'item',
    icon: 'message-square',
    url: '/chat'
  },
  {
    id: 'shop',
    title: 'Shop',
    type: 'item',
    icon: 'shopping-cart',
    url: '/shop'
  },
  {
    id: 'jobs',
    title: 'Jobs',
    type: 'item',
    icon: 'briefcase',
    url: '/jobs'
  },

  // {
  //   id: 'profile',
  //   title: 'My Profile',
  //   type: 'item',
  //   icon: 'user',
  //   url: '/my-profile'
  // },
];
