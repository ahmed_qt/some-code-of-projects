import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ProfileComponent} from './dentwe/components/profile/profile.component';
import {AuthGuard} from './dentwe/services/auth-guard';
import {RegisterComponent} from './dentwe/auth/register/register.component';
import {LoginComponent} from './dentwe/auth/login/login.component';
import {AppConfig} from './dentwe/services/AppConfig';
import {AuthService} from './dentwe/services/auth.service';
import {ForgetPasswordComponent} from './dentwe/auth/forget-password/forget-password.component';
import {ResetPasswordComponent} from './dentwe/auth/reset-password/reset-password.component';
import {ConfirmAccountComponent} from './dentwe/auth/confirm-account/confirm-account.component';
import { NetworkComponent } from './dentwe/components/network/network.component';
import { HomeComponent } from './dentwe/components/home/home.component';
import {AccountSettingsComponent} from './dentwe/components/account-settings/account-settings.component';
import {ShowPostComponent} from './dentwe/components/show-post/show-post.component';
import { JobsComponent } from './dentwe/components/jobs/jobs.component';
import { TermsOfUseComponent } from './dentwe/components/terms-of-use/terms-of-use.component';
import { PrivacyPolicyComponent } from './dentwe/components/privacy-policy/privacy-policy.component';
import { ShopComponent } from './dentwe/components/shop/shop.component';
import { EcommerceDetailsComponent } from './dentwe/components/shop/ecommerce-details/ecommerce-details.component';
import {ShopFormComponent} from './dentwe/components/shop-form/shop-form.component';
import {JobFormComponent} from './dentwe/components/job-form/job-form.component';
import {RegisterCompleteComponent} from './dentwe/auth/register-complete/register-complete.component';

export const routes: Routes = [
    // {
    //   path: 'pages',
    //   loadChildren: () => import('./main/pages/pages.module').then(m => m.PagesModule)
    // },
    // {
    //   path: '',
    //   redirectTo: '/home',
    //   pathMatch: 'full',
    //   canActivate: [AuthGuard]
    // },
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthService]
    },
    {
        path: 'terms-of-use',
        component: TermsOfUseComponent,
    },
    {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent,
    },
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthService]
    },
    {
        path: 'post/:id',
        component: ShowPostComponent,
        canActivate: [AuthService]
    },
    {
        path: 'profiles',
        component: NetworkComponent,
        canActivate: [AuthService]

    },
    {
        path: 'clinics',
        component: NetworkComponent,
        canActivate: [AuthService]

    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthService]

    },
    {
        path: 'profile/:id',
        component: ProfileComponent,
        data: {
            type: 0
        },
        canActivate: [AuthService]
    },
    {
        path: 'my-profile',
        component: ProfileComponent,
        data: {
            type: 1
        },
        canActivate: [AuthService]
    },
    {
        path: 'chat',
        loadChildren: () => import('./dentwe/components/chat/chat.module').then(m => m.ChatModule),
        canActivate: [AuthService]

    },
    {
        path: 'shop',
        component: ShopComponent,
        canActivate: [AuthService],
    },
    {
        path: 'shop/item/:id',
        component: EcommerceDetailsComponent,
        canActivate: [AuthService],
    },
    {
        path: 'jobs',
        component: JobsComponent,
        canActivate: [AuthService],
    },
    {
        path: 'account-settings',
        canActivate: [AuthService],
        component: AccountSettingsComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'register-complete',
        component: RegisterCompleteComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'forget-password',
        component: ForgetPasswordComponent,
    },
    {
        path: 'reset-password',
        component: ResetPasswordComponent,
    },
    {
        path: 'confirm-account',
        component: ConfirmAccountComponent,
    },
    {
        path: 'term-of-use',
        component: ConfirmAccountComponent,
    },
    {
        path: 'privacy-policy',
        component: ConfirmAccountComponent,
    },



    // {
    //   path: '**',
    //   redirectTo: '/pages/miscellaneous/error' //Error 404 - Page not found
    // }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes, {useHash: false}) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
