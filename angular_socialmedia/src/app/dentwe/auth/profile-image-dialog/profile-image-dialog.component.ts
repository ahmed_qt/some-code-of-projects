import { Component, OnInit } from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';

@Component({
  selector: 'app-profile-image-dialog',
  templateUrl: './profile-image-dialog.component.html',
  styleUrls: ['./profile-image-dialog.component.scss']
})
export class ProfileImageDialogComponent implements OnInit {

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {}

  ngOnInit(): void {
  }

  save(cropper: any) {
    this.ref.close({
      'image': cropper.file,
      'x': cropper.cropData.x,
      'y': cropper.cropData.y,
      'width': cropper.cropData.width,
      'height': cropper.cropData.height
    });
  }
}
