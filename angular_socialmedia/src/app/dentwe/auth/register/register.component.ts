import { Component, OnDestroy, OnInit } from '@angular/core';
import { CoreConfigService } from '@core/services/config.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { API } from '../../services/API';
import { AuthService } from '../../services/auth.service';
import { SweetAlert } from '../../services/SweetAlert';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import {AppConfig} from '../../services/AppConfig';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  separateDialCode = false;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;

  public registerForm: UntypedFormGroup;
  public submitted = false;
  public registered = false;
  public loading = false;
  public error = null;
  public emailTakenError = null;

  private token: any = null;
  private social_user: any = null;


  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  public accountTypes = [
    {
      name: 'dentist',
      title: 'Dentist',
      cssClass: '',
      image: '/assets/images/illustration/doctor.svg',
      imageClass: 'mb-2 mt-1',
      is_collapsed: false,
      features: [
        'Create professional CV',
        'Offer or find job',
        'Be a part of the global dental community',
        'Free online store',
      ]
    },
    {
      name: 'student',
      title: 'Student',
      cssClass: '',
      image: '/assets/images/illustration/dental-care.svg',
      imageClass: 'mt-1 mb-2',
      is_collapsed: false,
      features: [
        'Create your own profile',
        'Join dental community',
        'Explore clinical and business information from experienced dentists',
        'Free online store',
      ]
    },
    {
      name: 'clinic',
      title: 'Clinic',
      cssClass: '',
      image: '/assets/images/illustration/hospital.svg',
      imageClass: 'mt-1 mb-2',
      is_collapsed: false,
      features: [
        'Introduce your clinic team',
        'Share clinical cases',
        'Schedule your clinic appointments',
        'Identify your business address',
      ]
    }
  ];

  public selectedAccountType = null;


  constructor(
    private _coreConfigService: CoreConfigService,
    private _formBuilder: UntypedFormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private api: API,
    public config: AppConfig,
    private auth: AuthService,
    private toastr: ToastrService,
    private http: HttpClient
  ) {

    this._route.queryParams.subscribe(params => {
      if (params['social_token'] != undefined) {
        this.token = params['social_token'];
        this.loading = true;
        this.blockUI.start('Loading...');
        this.api._post('v1/auth/checkToken', { token: this.token, type: 'social_confirmcode' }).subscribe(response => {
          this.social_user = response;
          this.loading = false;
          this.blockUI.stop();
        }, e => {
          this.loading = false;
          this.blockUI.stop(); // Stop blocking
          this.toastr.error('Link expired');
          // this._router.navigate(['/']);
          return;
        });

      }

    });



    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }

  selectAccountType(account) {
    window.scrollTo(0, 0);

    if (account == null) {
      this.submitted = false;
      this.registerForm.removeControl('clinic_name');
      this.registerForm.removeControl('first_name');
      this.registerForm.removeControl('last_name');
      this.selectedAccountType = null;
      return;
    }
    this.selectedAccountType = account;
    if (this.selectedAccountType.name == 'clinic') {
      this.registerForm.addControl('clinic_name', new UntypedFormControl('', Validators.required));
      if (this.social_user) {
        this.registerForm.removeControl('email');
        this.registerForm.addControl('email', new UntypedFormControl({ value: [], disabled: true}, Validators.required));
        this.registerForm.patchValue({
          email: this.social_user['email'],
          clinic_name: this.social_user['name']
        });
      }
    } else {
      this.registerForm.addControl('first_name', new UntypedFormControl('', Validators.required));
      this.registerForm.addControl('last_name', new UntypedFormControl('', Validators.required));
      if (this.social_user) {
        this.registerForm.removeControl('email');
        this.registerForm.addControl('email', new UntypedFormControl({ value: [], disabled: true}, Validators.required));
        this.registerForm.patchValue({
          email: this.social_user['email'],
          first_name: this.social_user['name'].split(' ')[0],
          last_name: this.social_user['name'].split(' ')[1]
        });
      }
    }
  }

  ngOnInit(): void {
    this.registerForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      country: ['', Validators.required],
      agree: ['', Validators.required],
    });
    this.http.get('https://geolocation-db.com/json/').subscribe(response => {
      this.registerForm.patchValue({
        country: response['country_code']
      });
    });
  }

  register(data: any) {
    if (data['phone']) {
      data['phone'] = data['phone']['e164Number'];
    }
    if (this.social_user && this.token) {
      data.email = this.registerForm.getRawValue().email;
      this.api._post('v1/auth/social/register', {token: this.token, ...data}).subscribe(res => {
        this.loading = false;
        const resData: any = res;
        this.toastr.success('Successfully registered');
        this._router.navigate(['/']);
      }, e => {
        this.toastr.error('Couldn\'nt Register');
        this.loading = false;
        this.error = e.error.error;
      });
    } else {
      this.api._post('v1/auth/register', data).subscribe(res => {
        this.loading = false;
        const resData: any = res;
        this.toastr.success('Successfully registered please check your email');
        this._router.navigate(['/register-complete']);
      }, e => {
        if (e?.error?.messages[0] == 'The email has already been taken.') {
          this.toastr.error(e?.error?.messages[0]);
          this.emailTakenError = e?.error?.messages[0];
        } else {
          this.toastr.error('Couldn\'nt Register');
        }
        this.loading = false;
        this.error = e.error.error;
      });
    }
  }

  onSubmit() {
    console.log(this.registerForm.value)
    this.submitted = true;
    this.emailTakenError = null;
    // stop here if form is invalid
    console.log(this.registerForm);
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    if (this.selectedAccountType.name === 'clinic') {
      this.register({
        first_name: this.registerForm.value.clinic_name,
        email: this.registerForm.value.email,
        phone: this.registerForm.value.phone,
        country: this.registerForm.value.country,
        account_type: this.selectedAccountType.name
      });
    } else {
      this.register({
        first_name: this.registerForm.value.first_name,
        last_name: this.registerForm.value.last_name,
        email: this.registerForm.value.email,
        phone: this.registerForm.value.phone,
        country: this.registerForm.value.country,
        account_type: this.selectedAccountType.name
      });
    }
  }

}
