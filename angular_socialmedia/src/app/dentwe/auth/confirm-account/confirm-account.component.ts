import {Component, OnDestroy, OnInit} from '@angular/core';
import {CoreConfigService} from '@core/services/config.service';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {API} from '../../services/API';
import {AuthService} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {HttpClient} from '@angular/common/http';
import {CoreLoadingScreenService} from '../../../../@core/services/loading-screen.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {DialogService} from 'primeng/dynamicdialog';
import {ProfileImageDialogComponent} from '../profile-image-dialog/profile-image-dialog.component';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.scss']
})
export class ConfirmAccountComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;

  public confirmForm: UntypedFormGroup;
  public submitted = false;
  public registered = false;
  public loading = false;
  public error = null;

  public currentYear = null;
  public currentMonth = null;
  public years = [];
  private cropperData: any = {};
  constructor(
      private _coreConfigService: CoreConfigService,
      private _formBuilder: UntypedFormBuilder,
      private _route: ActivatedRoute,
      private _router: Router,
      private api: API,
      private auth: AuthService,
      private toastr: ToastrService,
      private http: HttpClient,
      public dialogService: DialogService
  ) {

    this._route.queryParams.subscribe(params => {
      if (params['token'] == undefined) {
        this._router.navigate(['/']);
        return;
      }
      this.token = params['token'];
      this.loading = true;
      this.blockUI.start('Loading...');
      this.api._post('v1/auth/checkToken', {token: this.token, type: 'confirm'}).subscribe(response => {
        this.loading = false;
        this.blockUI.stop();
      }, e => {
        this.loading = false;
        this.blockUI.stop(); // Stop blocking
        this.toastr.error('Link expired');
        this._router.navigate(['/']);
        return;
      });
    });

    this.currentYear = new Date().getFullYear();
    this.currentMonth = new Date().getMonth();
    this.generateArrayOfYears();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }
  public gender;


  private token: any = null;
  // convenience getter for easy access to form fields

  get f() {
    return this.confirmForm.controls;
  }

  generateArrayOfYears() {
    const max = this.currentYear;
    const min = max - 100;
    const years = [];

    for (let i = max; i >= min; i--) {
      years.push(i);
    }
    this.years = years;
  }

  ngOnInit(): void {
    this.confirmForm = this._formBuilder.group({
      full_name: ['', Validators.required],
      location: ['', Validators.required],
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required],
      birth_date_year: ['', Validators.required],
      birth_date_month: ['', Validators.required],
      birth_date_day: ['', Validators.required],
      gender: ['', Validators.required],
    });
    this.http.get('https://geolocation-db.com/json/').subscribe(response => {
      this.confirmForm.patchValue({
        location: response['country_name'],
        birth_date_year: this.currentYear,
        birth_date_month: this.currentMonth,
        birth_date_day: 30,
        gender: 'male',
      });
    });
  }

  confirm(data: any) {
    const formData = new FormData();
    for (const key of Object.keys(data) ) {
      const value = data[key];
      formData.append(key, value);
    }

    for (const key of Object.keys(this.cropperData) ) {
      const value = this.cropperData[key];
      formData.append(key, value);
    }
    this.api._upload('v1/auth/confirmRegister', formData).subscribe(res => {
      this.loading = false;
      const resData: any = res;
      this.toastr.success('Your Account is Registered Successfully!');
      this._router.navigate(['/login']);

    }, e => {
      this.toastr.error('Your request is denied');
      this.loading = false;
      this.error = e.error.error;
    });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.confirmForm);
    if (this.confirmForm.invalid) {
      return;
    }
    this.loading = true;
    const birth_date = new Date(parseInt(this.confirmForm.value.birth_date_year), parseInt(this.confirmForm.value.birth_date_month) - 1, parseInt(this.confirmForm.value.birth_date_day), 0, 0, 0, 0).toISOString().slice(0, 19).replace('T', ' ');
    this.confirm({
      full_name: this.confirmForm.value.full_name,
      location: this.confirmForm.value.location,
      password: this.confirmForm.value.password,
      password_confirmation: this.confirmForm.value.password_confirmation,
      birth_date: birth_date,
      gender: this.confirmForm.value.gender,
      token: this.token
    });
  }


  pickProfileImage() {
    const ref = this.dialogService.open(ProfileImageDialogComponent, {
      header: 'Choose Profile Picture',
      width: '50%'
    });

    ref.onClose.subscribe((data: any) => {
      this.cropperData = data;
    });
  }
}
