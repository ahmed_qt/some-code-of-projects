import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { CoreConfigService } from '@core/services/config.service';
import {ActivatedRoute, Router} from '@angular/router';
import {API} from '../../services/API';
import {AuthService} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResetPasswordComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  // Public
  public coreConfig: any;
  public passwordTextType: boolean;
  public confPasswordTextType: boolean;
  public resetPasswordForm: UntypedFormGroup;
  public submitted = false;
  public loading = false;
  public error = null;


  // Private
  private _unsubscribeAll: Subject<any>;
  private token: any = null;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
      private _coreConfigService: CoreConfigService,
      private _formBuilder: UntypedFormBuilder,
      private _route: ActivatedRoute,
      private _router: Router,
      private api: API,
      private auth: AuthService,
      private toastr: ToastrService,
    ) {

    this._route.queryParams.subscribe(params => {
      if (params['token'] == undefined) {
        this._router.navigate(['/']);
        return;
      }
      this.token = params['token'];
      this.loading = true;
      this.blockUI.start('Loading...');
      this.api._post('v1/auth/checkToken', {token: this.token, type: 'reset_password'}).subscribe(response => {
        this.loading = false;
        this.blockUI.stop();
      }, e => {
        this.loading = false;
        this.blockUI.stop(); // Stop blocking
        this.toastr.error('Link expired');
        this._router.navigate(['/']);
        return;
      });
    });

    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.resetPasswordForm.controls;
  }

  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  /**
   * Toggle confirm password
   */
  toggleConfPasswordTextType() {
    this.confPasswordTextType = !this.confPasswordTextType;
  }

  resetPassword(data: any) {
    this.api._post('v1/auth/resetpassword', data).subscribe(res => {
      this.loading = false;
      const resData: any = res;
      this.toastr.success('Your password updated Successfully!');
      this._router.navigate(['/login']);

    }, e => {
      this.toastr.error('Your request is denied');
      this.loading = false;
      this.error = e.error.error;
    });
  }

  /**
   * On Submit
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.resetPasswordForm.invalid) {
      return;
    }
    this.resetPassword({
      password: this.resetPasswordForm.value.newPassword,
      password_confirmation: this.resetPasswordForm.value.confirmPassword,
      token: this.token,
    });

  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.resetPasswordForm = this._formBuilder.group({
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    });

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
