import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { CoreConfigService } from '@core/services/config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { API } from '../../services/API';
import { CoreLoadingScreenService } from '../../../../@core/services/loading-screen.service';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AppConfig } from '../../services/AppConfig';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  //  Public
  public coreConfig: any;
  public loginForm: UntypedFormGroup;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public error = '';
  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(
    private _coreConfigService: CoreConfigService,
    private _formBuilder: UntypedFormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private api: API,
    private auth: AuthService,
    private toastr: ToastrService,
    private appConfig: AppConfig
  ) {


    this._route.queryParams.subscribe(params => {
      if (params['status'] != undefined) {
          if (params['status'] == 'under_review') {
            this.toastr.info('Your Account is under review we will inform you when it\'s accepted')
            this._router.navigate(['/login']);
          } else if (params['status'] == 'email_exsist') {
            this.toastr.info('An account with this email already exsist')
            this._router.navigate(['/login']);
          } else if (params['status'] == '400') {
            this.toastr.error('Couldn\'t process your request')
            this._router.navigate(['/login']);
          }
      }

      if (params['token'] != undefined) {
        localStorage.setItem(this.appConfig.tokenName, params['token']);
        this.api.get('v1/auth/me').subscribe(user => {
          const resdata: any = user;
          this.toastr.success('Welcome back ' + resdata.user.first_name);
          this._router.navigate(['/'], { replaceUrl: true });
        }, e => {
          localStorage.removeItem(this.appConfig.tokenName);
          this.loading = false;
          this._router.navigate(['/login'], { replaceUrl: true });
        });
      }

    });


    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };

  }

  public passwordTextType: boolean;
  // Private

  private _unsubscribeAll: Subject<any>;

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  login(data: any) {
    this.auth.clearPermissions();
    this.error = null;

    this.api._post('v1/auth/login', data).subscribe(res => {
      this.loading = false;
      const resData: any = res;
      localStorage.setItem(this.appConfig.tokenName, resData.access_token);
      this.api.get('v1/auth/me').subscribe(user => {
        const resdata: any = user;
        this.toastr.success('Welcome back ' + resdata.user.first_name);
        console.log(resdata);
        this._router.navigate(['/']);
      }, e => {
        localStorage.removeItem(this.appConfig.tokenName);
        this.loading = false;
      });
    }, e => {
      console.log(e)
      if (e.error.status != undefined && e.error.status != null && e.error.status == 'resend_verification_code') {
        this.loading = false;
        localStorage.removeItem(this.appConfig.tokenName);
        this.toastr.info(e.error.message);
        return;
      }
      // this.toastr.error(e.error.error);
      this.toastr.error('The email and password you entered did not match our records. Please double-check and try again.');
      localStorage.removeItem(this.appConfig.tokenName);
      this.loading = false;
      //this.error = e.error.error;
      this.error = 'The email and password you entered did not match our records. Please double-check and try again.';
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    // Login
    this.loading = true;

    this.login({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    });
    // redirect to home page
    // setTimeout(() => {
    //   this._router.navigate(['/']);
    // }, 100);
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  socialLogin(social) {
    switch (social) {
      case 'facebook':
        window.location.href = this.appConfig.getApiUrl() + 'v1/auth/social/facebook/redirect';
        break;
      case 'linkedin':
        window.location.href = this.appConfig.getApiUrl() + 'v1/auth/social/linkedin/redirect';
        break;
      case 'google':
        window.location.href = this.appConfig.getApiUrl() + 'v1/auth/social/google/redirect';
        break;
      case 'twitter':
        window.location.href = this.appConfig.getApiUrl() + 'v1/auth/social/twitter/redirect';
        break;
      default:
        break;
    }
  }
}
