import {Component, Input, OnInit} from '@angular/core';
import {AppConfig} from '../../services/AppConfig';
import {API} from '../../services/API';

@Component({
  selector: 'app-follow-tab',
  templateUrl: './follow-tab.component.html',
  styleUrls: ['./follow-tab.component.scss']
})
export class FollowTabComponent implements OnInit {

  public data: any = [];
  public loading = false;
  public message;

  @Input() public type;
  @Input() public userId;

  constructor(
      public config: AppConfig,
      private _api: API
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this._api.post('v1/followers', {user_id: this.userId, follower_type: this.type}).subscribe(response => {
        this.loading = false;
        if (response.message != undefined && response.message == 'private') {
          this.data = [];
          this.message = response.message;
        } else {
          this.data = response;
        }
    }, error => {
      this.loading = false;
    });
  }

}
