import {AfterContentInit, ContentChild, Directive, ElementRef, HostListener, ViewChild} from '@angular/core';
import {ReactionsComponent} from './reactions/reactions.component';

@Directive({
  selector: '[appReactions]'
})
export class ReactionsDirective implements AfterContentInit {

  public reactionsVisible = false;

  @ContentChild(ReactionsComponent) reactionsComponent: ReactionsComponent;


  constructor(private el: ElementRef) {
    // el.nativeElement.style.backgroundColor = 'yellow';
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.reactionsComponent.reactionsVisible = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.reactionsComponent.reactionsVisible = false;

  }

  ngAfterContentInit(): void {
  }


}
