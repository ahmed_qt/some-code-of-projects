import { Component, OnInit } from '@angular/core';
import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';
import {API} from '../../services/API';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {AppConfig} from '../../services/AppConfig';
import { AuthService } from 'app/dentwe/services/auth.service';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.scss']
})
export class NetworkComponent implements OnInit {
  @BlockUI('users-list') blockUI: NgBlockUI;

  // public
  public contentHeader: object;

  // users
  public users = [];

  // pagination
  public page = 1;
  public totalItems;
  public per_page;

  // search ngModel
  public searchVal = '';

  public group = null;
  public users_count = null;

  // filter
  public filter = {search: null, page: 1, group: null};

  // loading
  public loading = false;

  constructor(
      private _coreSidebarService: CoreSidebarService,
      private _api: API,
      public auth: AuthService,
      private _router: Router,
      private _route: ActivatedRoute,
      public config: AppConfig,
      private toastr: ToastrService,

  ) {

    this._route.queryParams.subscribe(params => {
      console.log('change');
      this.page = 1;
      this.searchVal = '';
      this.group = null;
      if (params['page'] != undefined) {
        this.page = params['page'];
      }
      if (params['search'] != undefined) {
        this.searchVal = params['search'];
      }
      if (params['group'] != undefined) {
        this.group = params['group'];
      }
      this.loadData();
    });
  }

  /**
   * On init
   */
  ngOnInit(): void {

    this._api.post('v1/users-count', {}).subscribe(response => {
      this.users_count = response;
    }, error => {
      this.toastr.error('Error Occurred');
    });

    // content header
    this.contentHeader = {
      headerTitle: 'Network',
      actionButton: true,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Home',
            isLink: true,
            link: '/'
          },
          {
            name: 'Network',
            isLink: false
          }
        ]
      }
    };


  }

  loadData() {
    this.loading = true;
    this.blockUI.start('loading...');
    let filterData = this.filter;
    filterData.page = this.page;
    filterData.search = this.searchVal;
    filterData.group = this.group;
    this._api.post('v1/users', filterData).subscribe(response => {
      this.blockUI.stop();
      this.loading = false;
      this.users = response.data;
      this.totalItems = response.total;
      this.per_page = response.per_page;
    }, error => {
      this.blockUI.stop();
      this.loading = false;
      this.toastr.error('Error Occurred');
    });

  }

  navigate(filter: any) {
    this._router.navigate(['/profiles'], {queryParams: filter});
  }

  changePage() {
    this.filter.page = this.page;
    this.filter.search = this.searchVal ? this.searchVal : null;
    this.navigate(this.filter);
  }

  search(value) {
    if (!value) {
      this.filter.search = null;
    } else {
      this.filter.search = value;
    }
    this.navigate(this.filter);
  }

  changeCategory(group: string) {
    this.filter.group = group;
    this.filter.page = 1;
    this.filter.search = null;

    this.navigate(this.filter);

  }

  text: string;

  results: string[];

  searchx(event) {
    this._api.post('v1/users', {search: event.query}).subscribe(response => {
      this.results = response.data.map(i => {i.first_name = i.first_name + ' ' + i.last_name; return i;});
    });
    
  }

}
