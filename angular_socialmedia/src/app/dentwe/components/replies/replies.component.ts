import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {AppConfig} from '../../services/AppConfig';
import {API} from '../../services/API';
import {ConfirmationService} from 'primeng/api';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-replies',
  templateUrl: './replies.component.html',
  styleUrls: ['./replies.component.scss']
})
export class RepliesComponent implements OnInit {

  @Input() data: any;
  @Input() loadAll = false;
  @Input() nav_reply_id = null;

  public loading = false;
  public replies = [];
  public show = false;
  public replyLoading = false;
  public reply = '';
  public repliesCurrentPage = 1;
  public repliesLastPage;

  public reactions_visiable = false;

  public activeReaction: any;
  public activeReactionData: any;
  public activeReactionType: any;


  constructor(
      public auth: AuthService,
      public config: AppConfig,
      private _api: API,
      private confirmationService: ConfirmationService,
      private _toast: ToastrService,
  ) { }

  ngOnInit(): void {
    if (this.loadAll) {
      this.showReplies();
    }
  }

  postComment() {
    if (!this.reply) return;
    this._api.post('v1/comment/reply', {comment_id: this.data.id, comment: this.reply}).subscribe(res => {
      const reply = res.data;
      reply.user = this.auth.user;
      // this.comments.push(comment);
      this.reply = '';
      // this.data.comments_count += 1;
      this.loadReplies(1, true);
    });
  }

  reportComment() {
    
  }

  deleteComment(comment: any) {
    this.confirmationService.confirm({
      message: 'Delete Comment?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this._api.delete('v1/reply/' + comment.id).subscribe(res => {
          this.replies = this.replies.filter(i => i.id != comment.id);
          this.data.replies_count -= 1;
          this._toast.info('Comment Deleted');
        });
      }
    });
  }

  showReplies() {
    if (this.loading) return;
    this.show = !this.show;
    if (this.replies.length) return;
    this.loadReplies();

  }

  showReactionsDialog(reaction: any, type: any, data: any) {
    this.activeReaction = reaction;
    this.activeReactionData = data;
    this.activeReactionType = type;
    this.reactions_visiable = true;
  }


  viewMore() {
    if (this.repliesCurrentPage != this.repliesLastPage) {
      this.loadReplies(this.repliesCurrentPage + 1);
    }
  }

  private loadReplies(page = 1, hard = false) {
    this.loading = true;
    this._api.post('v1/comment/replies', {comment_id: this.data.id, page: page, load_all: this.loadAll}).subscribe((res: any) => {
      this.loading = false;
      this.repliesCurrentPage = res.current_page;
      this.repliesLastPage = res.last_page;
      this.show = true;
      if (hard) {
        this.replies = res.data;
      } else {
        this.replies = [...this.replies, ...res.data];
      }
    }, error => {
      this.loading = false;
    });
  }

  removeReplyReaction(comment: any) {
    this._api.post('v1/reply/removeReaction', {reply_id: comment.id}).subscribe(res => {
      console.log('removed');
      console.log(comment.my_reaction);
      switch (comment.my_reaction.reaction.name) {
        case 'like':
          comment.like_reactions_count -= 1;
          break;
        case 'laugh':
          comment.laugh_reactions_count -= 1;
          break;
        case 'love':
          comment.love_reactions_count -= 1;
          break;
        case 'sad':
          comment.sad_reactions_count -= 1;
          break;
        case 'angry':
          comment.angry_reactions_count -= 1;
          break;
      }
      comment.my_reaction = null;
    });
  }
}
