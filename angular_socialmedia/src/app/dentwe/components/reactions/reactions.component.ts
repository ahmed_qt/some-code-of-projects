import {Component, Input, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {API} from '../../services/API';

@Component({
  selector: 'app-reactions',
  templateUrl: './reactions.component.html',
  styleUrls: ['./reactions.component.scss'],
  animations: [
    trigger(
        'enterAnimation', [
          transition(':enter', [
            style({transform: 'translateY(10px)', opacity: 0}),
            animate('100ms', style({transform: 'translateY(0)', opacity: 1}))
          ]),
          transition(':leave', [
            style({transform: 'translateY(0)', opacity: 1}),
            animate('100ms', style({transform: 'translateY(10px)', opacity: 0}))
          ]),
        ]
    ),
  ],
})
export class ReactionsComponent implements OnInit {

  public reactionsVisible: boolean;

  @Input() styleClass = '';
  @Input() data: any;
  @Input() type: any;

  constructor(
      private _api: API
  ) { }

  ngOnInit(): void {
  }

  react(reaction_id) {
    if (this.type == 'post') {
      if (this.data.my_reaction) {
        if (this.data.my_reaction.reaction.id == reaction_id) {
          return;
        }
      }
      this._api.post('v1/post/addReaction', {post_id: this.data.id, reaction: reaction_id}).subscribe(res => {
        this.reactionsVisible = false;
        if (this.data.my_reaction != null) {
            switch (this.data.my_reaction.reaction.name) {
                case 'like':
                    this.data.like_reactions_count -= 1;
                    break;
                case 'laugh':
                    this.data.laugh_reactions_count -= 1;
                    break;
                case 'love':
                    this.data.love_reactions_count -= 1;
                    break;
                case 'sad':
                    this.data.sad_reactions_count -= 1;
                    break;
                case 'angry':
                    this.data.angry_reactions_count -= 1;
                    break;
            }
        }
        this.data.my_reaction = res.data;
        switch (res.data.reaction.name) {
                case 'like':
                  this.data.like_reactions_count += 1;
                  break;
                case 'laugh':
                  this.data.laugh_reactions_count += 1;
                  break;
                case 'love':
                  this.data.love_reactions_count += 1;
                  break;
                case 'sad':
                  this.data.sad_reactions_count += 1;
                  break;
                case 'angry':
                  this.data.angry_reactions_count += 1;
                  break;
              }
      });
    } else if (this.type == 'comment') {
        if (this.data.my_reaction) {
            if (this.data.my_reaction.reaction.id == reaction_id) {
                return;
            }
        }
        this._api.post('v1/comment/addReaction', {comment_id: this.data.id, reaction: reaction_id}).subscribe(res => {
            this.reactionsVisible = false;
            if (this.data.my_reaction != null) {
                switch (this.data.my_reaction.reaction.name) {
                    case 'like':
                        this.data.like_reactions_count -= 1;
                        break;
                    case 'laugh':
                        this.data.laugh_reactions_count -= 1;
                        break;
                    case 'love':
                        this.data.love_reactions_count -= 1;
                        break;
                    case 'sad':
                        this.data.sad_reactions_count -= 1;
                        break;
                    case 'angry':
                        this.data.angry_reactions_count -= 1;
                        break;

                }
            }
            this.data.my_reaction = res.data;
            switch (res.data.reaction.name) {
                case 'like':
                    this.data.like_reactions_count += 1;
                    break;
                case 'laugh':
                    this.data.laugh_reactions_count += 1;
                    break;
                case 'love':
                    this.data.love_reactions_count += 1;
                    break;
                case 'sad':
                    this.data.sad_reactions_count += 1;
                    break;
                case 'angry':
                    this.data.angry_reactions_count += 1;
                    break;
            }
        });
    } else if (this.type == 'reply') {
        if (this.data.my_reaction) {
            if (this.data.my_reaction.reaction.id == reaction_id) {
                return;
            }
        }
        this._api.post('v1/reply/addReaction', {reply_id: this.data.id, reaction: reaction_id}).subscribe(res => {
            this.reactionsVisible = false;
            if (this.data.my_reaction != null) {
                switch (this.data.my_reaction.reaction.name) {
                    case 'like':
                        this.data.like_reactions_count -= 1;
                        break;
                    case 'laugh':
                        this.data.laugh_reactions_count -= 1;
                        break;
                    case 'love':
                        this.data.love_reactions_count -= 1;
                        break;
                    case 'sad':
                        this.data.sad_reactions_count -= 1;
                        break;
                    case 'angry':
                        this.data.angry_reactions_count -= 1;
                        break;
                }
            }
            this.data.my_reaction = res.data;
            switch (res.data.reaction.name) {
                case 'like':
                    this.data.like_reactions_count += 1;
                    break;
                case 'laugh':
                    this.data.laugh_reactions_count += 1;
                    break;
                case 'love':
                    this.data.love_reactions_count += 1;
                    break;
                case 'sad':
                    this.data.sad_reactions_count += 1;
                    break;
                case 'angry':
                    this.data.angry_reactions_count += 1;
                    break;
            }
        });
    }

  }
}
