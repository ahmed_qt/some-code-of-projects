import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {CoreSidebarService} from '../../../../@core/components/core-sidebar/core-sidebar.service';
import {API} from '../../services/API';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppConfig} from '../../services/AppConfig';
import {ToastrService} from 'ngx-toastr';
import {BreakpointObserver, BreakpointState, LayoutModule} from '@angular/cdk/layout';
import {MenuItem} from 'primeng/api';
import {ContactService} from '../../services/contact.service';
import {ImageCropperComponent} from 'ngx-image-cropper';
import {JobFormComponent} from '../job-form/job-form.component';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  @BlockUI('users-list') blockUI: NgBlockUI;
  @ViewChild('newJobForm') newJobForm: JobFormComponent;

  // public
  public contentHeader: object;

  // pagination
  public page = 1;
  public totalItems;
  public per_page;

  // search ngModel
  public jobTitleVal = '';
  public locationVal = '';
  public jobTypeVal = '';
  public departmentVal = '';

  public job_id = null;
  public my_jobs = 0;

  // filter
  public filter = {job_title: null, location: null, job_type: null, department: null, page: 1, job_id: null, my_jobs: 0};

  // loading
  public loading = false;

  showMyJobsChip = false;

  public searchForm: UntypedFormGroup;
  get f() {
    return this.searchForm.controls;
  }

  items: MenuItem[] =  [
    {
      label: 'Post a new job',
      command: () => {
        this.newJobForm.show();
      }
    },
    {
      label: 'My Jobs',
      command: () => {
        this.filter.my_jobs = 1;
        this.filter.job_title = null;
        this.filter.location = null;
        this.filter.job_type = null;
        this.filter.department = null;
        this.filter.page = 1;
        this.navigate(this.filter);
      }
    }
  ];



  // jobs
  selectedJobIndex = null;
  isMobileScreen = false;
  jobs = [];

  constructor(
      private _coreSidebarService: CoreSidebarService,
      private _api: API,
      public auth: AuthService,
      private _router: Router,
      private _route: ActivatedRoute,
      private _formBuilder: UntypedFormBuilder,
      public config: AppConfig,
      private toastr: ToastrService,
      private breakpointObserver: BreakpointObserver,
      private _contact: ContactService,
  ) {
    // detect screen size changes
    this.breakpointObserver.observe([
      '(max-width: 768px)'
    ]).subscribe((result: BreakpointState) => {
      if (result.matches) {
       this.isMobileScreen = true;
      } else {
        this.isMobileScreen = false;
      }
    });

    this.searchForm = this._formBuilder.group({});
    this.searchForm.addControl('job_title', new UntypedFormControl(''));
    this.searchForm.addControl('department', new UntypedFormControl(''));
    this.searchForm.addControl('location', new UntypedFormControl(''));
    this.searchForm.addControl('job_type', new UntypedFormControl(''));



    this._route.queryParams.subscribe(params => {
      console.log('change');
      this.page = 1;
      this.selectedJobIndex = null;
      this.jobTitleVal = '';
      this.locationVal = '';
      this.jobTypeVal = '';
      this.departmentVal = '';
      this.job_id = null;
      this.my_jobs = 0;
      if (params['page'] != undefined) {
        this.page = params['page'];
      }

      if (params['job_title'] != undefined) {
        this.jobTitleVal = params['job_title'];
      }
      if (params['location'] != undefined) {
        this.locationVal = params['location'];
      }
      if (params['job_type'] != undefined) {
        this.jobTypeVal = params['job_type'];
      }
      if (params['department'] != undefined) {
        this.departmentVal = params['department'];
      }


      if (params['my_jobs'] != undefined && params['my_jobs'] == 1) {
        this.showMyJobsChip = true;
        this.my_jobs = 1;
      }
      // if (params['job_id'] != undefined) {
      //   this.job_id = params['job_id'];
      //   return;
      // }
      this.loadData();
    });
  }

  /**
   * On init
   */
  ngOnInit(): void {

    // content header
    this.contentHeader = {
      headerTitle: 'Jobs',
      actionButton: true,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Home',
            isLink: true,
            link: '/'
          },
          {
            name: 'Jobs',
            isLink: false
          }
        ]
      }
    };


  }

  loadData() {
    this.loading = true;
    this.blockUI.start('loading...');
    let filterData = this.filter;
    filterData.page = this.page;
    filterData.job_title = this.jobTitleVal;
    filterData.location = this.locationVal;
    filterData.job_type = this.jobTypeVal;
    filterData.department = this.departmentVal;
    filterData.job_id = this.job_id;
    filterData.my_jobs = this.my_jobs;

    this.searchForm.patchValue({
      job_title: this.jobTitleVal,
      department: this.departmentVal,
      location: this.locationVal,
      job_type: this.jobTypeVal,
    });

    this._api.post('v1/jobs/list', filterData).subscribe(response => {
      this.blockUI.stop();
      this.loading = false;
      this.jobs = response.data;
      this.totalItems = response.total;
      this.per_page = response.per_page;
    }, error => {
      this.blockUI.stop();
      this.loading = false;
      this.toastr.error('Error Occurred');
    });
  }

  navigate(filter: any) {
    this._router.navigate(['/jobs'], {queryParams: filter});
  }

  changePage() {
    this.filter.page = this.page;
    this.filter.job_title = this.jobTitleVal ? this.jobTitleVal : null;
    this.filter.location = this.locationVal ? this.locationVal : null;;
    this.filter.job_type = this.jobTypeVal ? this.jobTypeVal : null;;
    this.filter.department = this.departmentVal ? this.departmentVal : null;;
    this.navigate(this.filter);
  }

  search($event) {
    $event.preventDefault();
    // stop here if form is invalid
    if (this.searchForm.invalid) {
      return;
    }
    let data = { ...this.searchForm.value };
    console.log(data);
    this.filter.job_title = data.job_title;
    this.filter.location = data.location;
    this.filter.job_type = data.job_type;
    if (this.filter.job_type == "Dentist") {
      this.filter.department = data.department;
    } else {
      this.filter.department = null;
    }
    this.navigate(this.filter);
  }

  changeJob(job_index: number, job) {
    this.selectedJobIndex = job_index;
  }

  text: string;

  results: string[];
  applyJobDialog: any = false;
  closeJobDialog: any = false;

  applyJob() {
    this._contact.sendMessage(this.jobs[this.selectedJobIndex]['user']);
  }

  close() {
    this.newJobForm.showDialog = false;
    this.loadData();
  }

  closeJob() {

    this.loading = true;
    this.blockUI.start('loading...');
    this._api.post('v1/jobs/close', {job_id: this.jobs[this.selectedJobIndex]['id']}).subscribe(response => {
      this.blockUI.stop();
      this.loading = false;
      this.closeJobDialog = false;
      this.loadData();
    }, error => {
      this.blockUI.stop();
      this.loading = false;
      this.toastr.error('Error Occurred');
    });


  }


  allJobs() {
    this.showMyJobsChip = false;
    this.filter.my_jobs = 0;
    this.filter.job_title = null;
    this.filter.location = null;
    this.filter.job_type = null;
    this.filter.department = null;
    this.filter.page = 1;
    this.navigate(this.filter);
  }

  autocompleteJobTitles = (async ($event) => {
    return new Promise((resolve, rejects) => {
      this._api.post('v1/jobs/autocomplete/job-title', {query: $event.query}).subscribe(res => {
        resolve(res.map(i => i.name));
      }, err => {
        rejects(err);
      });
    });
  }).bind(this);

  autocompleteDepartments = (async ($event) => {
    return new Promise((resolve, rejects) => {
      this._api.post('v1/jobs/autocomplete/job-departments', {query: $event.query}).subscribe(res => {
        resolve(res.map(i => i.name));
      }, err => {
        rejects(err);
      });
    });
  }).bind(this);

  autocompleteJobLocations = (async ($event) => {
    return new Promise((resolve, rejects) => {
      this._api.post('v1/jobs/autocomplete/locations', {query: $event.query}).subscribe(res => {
        resolve(res.map(i => i.name));
      }, err => {
        rejects(err);
      });
    });
  }).bind(this);
}
