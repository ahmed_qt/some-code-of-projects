import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {API} from '../../services/API';
import {AppConfig} from '../../services/AppConfig';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-shop-form',
  templateUrl: './shop-form.component.html',
  styleUrls: ['./shop-form.component.scss']
})
export class ShopFormComponent implements OnInit {

  @Output() onComplete: EventEmitter<any> = new EventEmitter<any>();
  showDialog = false;


  public data: any;
  public item: any;
  public deleted_item_images = [];
  public loading = false;
  public infoForm: UntypedFormGroup;
  public submitted = false;
  public error = null;


  get f() {
    return this.infoForm.controls;
  }

  public imagesFiles = [];

  constructor(
      private _api: API,
      private _formBuilder: UntypedFormBuilder,
      public config: AppConfig,
      private _toastr: ToastrService,
      public auth: AuthService,
  ) {

  }


  show(): void {
    this.deleted_item_images = [];
    this.imagesFiles = [];
    this.item = null;
    this.infoForm.reset();
    this.showDialog = true;
    this.submitted = false;
  }

  load(id: any): void {
    this.imagesFiles = [];
    this.deleted_item_images = [];
    this.showDialog = true;
    this.loading = true;
    this.submitted = false;
    this._api.get(`v1/shop/items/${id}`).subscribe(response => {
      this.loading = false;
      this.item = response;
      this.infoForm.patchValue({
        item_name: this.item.item_name,
        item_description: this.item.item_description,
        department: this.item.department,
        price: this.item.price,
        warranty: this.item.warranty,
        location: this.item.location,
        condition: this.item.condition,
        quantity: this.item.quantity,
        brand: this.item.brand,
        manufacturer: this.item.manufacturer,
      });

    });
  }

  ngOnInit() {
    this.infoForm = this._formBuilder.group({});
    this.infoForm.addControl('item_name', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('item_description', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('department', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('price', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('warranty', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('location', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('condition', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('quantity', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('brand', new UntypedFormControl('', Validators.required));
    this.infoForm.addControl('manufacturer', new UntypedFormControl('', Validators.required));
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.infoForm.invalid) {
      return;
    }

    if (!this.imagesFiles.length) {
      if (this.item) {
        if (!this.item.images.length) {
          return;
        }
      } else {
        return;
      }
    }

    this.loading = true;


    if (this.item) {
      // update
      const data = new FormData();
      for (const key in this.infoForm.value ) {
        data.append(key, this.infoForm.value[key]);
      }
      data.append('images_count', String(this.imagesFiles.length));
      this.imagesFiles.forEach((value, i) => {
        data.append('image' + i, value.file);
      });

      data.append('deleted_images_count', String(this.deleted_item_images.length));
      this.deleted_item_images.forEach((value, i) => {
        data.append('deleted_image' + i, value.id);
      });


      this._api.upload(`v1/shop/items/${this.item.id}`, data).subscribe(res => {
        this.loading = false;
        this.showDialog = false;
        this._toastr.success('operation succeeded');
        this.onComplete.emit(true);
      }, e => {
        this._toastr.error('operation failed');
        this.loading = false;
        this.showDialog = false;
        this.error = e.error.error;
      });


    } else {
      // new
      const data = new FormData();
      for (const key in this.infoForm.value ) {
        data.append(key, this.infoForm.value[key]);
      }
      data.append('images_count', String(this.imagesFiles.length));
      this.imagesFiles.forEach((value, i) => {
        data.append('image' + i, value.file);
      });

      this._api.upload('v1/shop', data).subscribe(res => {
        this.loading = false;
        this._toastr.success('operation succeeded');
        this.onComplete.emit(true);
        this.showDialog = false;
      }, e => {
        this._toastr.error('operation failed');
        this.loading = false;
        this.error = e.error.error;
        this.showDialog = false;
      });
    }

  }

  onHide() {
    this.loading = false;
  }


  autocompleteItemLocations = (async ($event) => {
    return new Promise((resolve, rejects) => {
      this._api.post('v1/shop/autocomplete/locations', {query: $event.query}).subscribe(res => {
        resolve(res.map(i => i.name));
      }, err => {
        rejects(err);
      });
    });
  }).bind(this);


  autocompleteItemBrands = (async ($event) => {
    return new Promise((resolve, rejects) => {
      this._api.post('v1/shop/autocomplete/brands', {query: $event.query}).subscribe(res => {
        resolve(res.map(i => i.name));
      }, err => {
        rejects(err);
      });
    });
  }).bind(this);


  autocompleteDepartments = (async ($event) => {
    return new Promise((resolve, rejects) => {
      this._api.post('v1/shop/autocomplete/shop-departments', {query: $event.query}).subscribe(res => {
        resolve(res.map(i => i.name));
      }, err => {
        rejects(err);
      });
    });
  }).bind(this);
  
  
  autocompleteItemManufacturer = (async ($event) => {
    return new Promise((resolve, rejects) => {
      this._api.post('v1/shop/autocomplete/manufacturers', {query: $event.query}).subscribe(res => {
        resolve(res.map(i => i.name));
      }, err => {
        rejects(err);
      });
    });
  }).bind(this);


  onImagesChange($event: any) {
    this.imagesFiles = [];
    for (const i of $event.target.files) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagesFiles.push({url: reader.result as string, file: i});
      };
      reader.readAsDataURL(i);
    }
  }

  removeFile(file: any) {
    this.imagesFiles = this.imagesFiles.filter(i => {
      if (i.url == file.url) {
        return;
      }
      return i;
    });
  }

  removeItemImage(image: any) {
    this.deleted_item_images.push(image);
    this.item.images = this.item.images.filter(i => {
      if (i.id == image.id) {
        return;
      }
      return i;
    });
  }

  onAddMoreImagesChange($event: any) {
    for (const i of $event.target.files) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagesFiles.push({url: reader.result as string, file: i});
      };
      reader.readAsDataURL(i);
    }

  }

}
