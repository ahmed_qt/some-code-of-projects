import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppConfig} from '../../services/AppConfig';
import {AuthService} from '../../services/auth.service';
import {API} from '../../services/API';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {

  public newPostDialog = false;

  @Input() type;
  @Input() user;
  @Output() onPostComplete: EventEmitter<any> = new EventEmitter<any>();


  constructor(
      public config: AppConfig,
      public auth: AuthService,
      private _api: API,
  ) { }


  showPostDialog() {
    this.newPostDialog = true;
  }

  ngOnInit(): void {
  }

}
