import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CoreSidebarService} from '../../../../../@core/components/core-sidebar/core-sidebar.service';
import {ContactService} from '../../../services/contact.service';
import {AppConfig} from '../../../services/AppConfig';
import {SocketService} from '../../../services/socket.service';
import {AuthService} from '../../../services/auth.service';
import {API} from '../../../services/API';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
    selector: 'app-chat-content',
    templateUrl: './chat-content.component.html',
    styleUrls: ['./chat-content.component.scss']
})
export class ChatContentComponent implements OnInit, OnDestroy {

    @Input()
    public chat;

    public loading = false;
    @BlockUI('block') blockUI: NgBlockUI;

    // Decorator
    @ViewChild('scrollMe') scrollMe: ElementRef;
    scrolltop: number = null;

    // Public
    public activeChat: Boolean;
    public chatUser;
    public chatMessage = '';

    public messages = [];
    public sendingImage = false;
    
    public isEmojiPickerVisible = false;


    constructor(
        private _coreSidebarService: CoreSidebarService,
        public contact: ContactService,
        public config: AppConfig,
        private _socket: SocketService,
        public auth: AuthService,
        private _api: API
    ) {
    }


    updateChat() {
        console.log(this.chatMessage);
        if (!this.chatMessage) {
            return;
        }
        this._socket.sendMessage({
            to: this.chatUser.id,
            message: this.chatMessage
        });
        this.chatMessage = '';
    }

    ngOnInit(): void {
      this.blockUI.start('Loading...');
        this._api.post('v1/chat/get', {
            from_id: this.chat.from_id,
            to_id: this.chat.to_id,
        }).subscribe(res => {
            // this.loading = false;
            this.blockUI.stop();
            this.messages = res.data.reverse();
            setTimeout(() => {
                this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
            }, 100);

            this._socket.socket.on('private message', (data: any) => {
                if ((data.from_id == this.chat.from_id && data.to_id == this.chat.to_id) || (data.from_id == this.chat.to_id && data.to_id == this.chat.from_id)) {
                    if (data.from_id != this.auth.user.id) {
                        const audio = new Audio();
                        audio.play();
                        audio.src = '/assets/sounds/notification.mp3';
                        audio.play();
                    }
                    this.messages.push(data);
                    setTimeout(() => {
                        this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
                    }, 100);
                }

            });


        });


        this.contact.onSelectContactChange.subscribe(res => {
            if (res) {
                // console.log(res);
                this.chatUser = res.to_user;
                this.chatMessage = '';
                this.activeChat = true;
                setTimeout(() => {
                    this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
                }, 100);
            } else {
                this.activeChat = false;
            }


        });

        this.contact.onNewMessage.subscribe(res => {
            setTimeout(() => {
                this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
            }, 100);
        });

        // this._chatService.onUserProfileChange.subscribe(response => {
        //   this.userProfile = response;
        // });
    }


    toggleSidebar(name) {
        this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
    }

    ngOnDestroy(): void {
        this._socket.socket.off('private message');
    }


    sendImage($event: any, input) {
        console.log('start');

        this.sendingImage = true;
        setTimeout(() => {
            this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
        }, 100);
        const data = new FormData();
        data.append('user_id', this.chatUser.id);
        data.append('image', $event.target.files[0]);
        this._api.upload('v1/chat/image', data).subscribe(res => {
            this.sendingImage = false;
        });
        // const reader = new FileReader();
        // reader.onload = (event: any) => {
        //     const base64 = event.target.result.replace(/.*base64,/, '');
        //     console.log(base64);
        //     console.log('done');
        //     this._socket.sendPhoto({
        //         to: this.chatUser.id,
        //         image: base64
        //     });
        //     input.value = null;
        //     setTimeout(() => {
        //         this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
        //     }, 0);
        //
        // };
        // reader.readAsDataURL($event.target.files[0]);
    }

    addEmoji($event: any) {
        const text = `${$event.emoji.native}`;

        this.chatMessage += text;

        
    }
}
