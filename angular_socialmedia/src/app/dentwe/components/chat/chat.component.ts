import {Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ContactService} from '../../services/contact.service';
import {AppConfig} from '../../services/AppConfig';
import {SocketService} from '../../services/socket.service';
import {AuthService} from '../../services/auth.service';
import {CoreSidebarService} from '../../../../@core/components/core-sidebar/core-sidebar.service';
import {ChatService} from './chat.service';
import {ActivatedRoute} from '@angular/router';
import {API} from '../../services/API';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: { class: 'chat-application' }
})
export class ChatComponent implements OnInit, OnDestroy {


  public searchText;
  public selectedIndex = null;
  public userProfile;

  public contacts_visible = false;
  public contacts = [];


  // Decorator
  @ViewChild('scrollMe') scrollMe: ElementRef;
  scrolltop: number = null;

  // Public
  public activeChat: Boolean;


  constructor(
      private _coreSidebarService: CoreSidebarService,

      public contact: ContactService,
      public config: AppConfig,
      private _socket: SocketService,
      public auth: AuthService,
      private _route: ActivatedRoute,
      private _api: API

  ) {
    this._route.queryParams.subscribe(params => {
      if (params['chat_id'] != null) {
        const interval = setInterval(() => {
          if (this.contact.contacts.length) {
            this.contacts_visible = false;

            const chat_id = parseInt(params['chat_id']);
            this.openChat(this.contact.contacts.filter(i => {
              if (i.id == chat_id) return i;
            })[0]);
            this.selectedIndex = chat_id;
            clearInterval(interval);

          }

        }, 100);

      }
    });
  }




  openChat(contact) {
    this.contact.onSelectContactChange.next(contact);
  }
  setIndex(index: number) {
    console.log(index);
    this.selectedIndex = index;
  }

  ngOnInit(): void {
    this._api.post('v1/mycontacts', {}).subscribe(res => {
      this.contacts = res;
    });


    this.contact.onSelectContactChange.subscribe(res => {
      if (res) {
        this.activeChat = true;
        setTimeout(() => {
          this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
        }, 0);

      } else {
        this.activeChat = false;

      }


    });

    this.contact.onNewMessage.subscribe(res => {
      setTimeout(() => {
        this.scrolltop = this.scrollMe?.nativeElement.scrollHeight;
      }, 0);
    });

    }


  toggleSidebar(name) {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  ngOnDestroy(): void {
    this.contact.onSelectContactChange.next(null);

  }


  showContact() {
    this.contacts_visible = true;
  }

  hideContact() {
    this.contacts_visible = false;

  }

  createChat(contact: any) {
    this.contact.sendMessage(contact);
    this.hideContact();
  }
}
