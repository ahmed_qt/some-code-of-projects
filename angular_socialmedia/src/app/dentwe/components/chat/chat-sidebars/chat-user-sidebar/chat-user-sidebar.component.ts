import { Component, OnInit } from '@angular/core';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';

import { ChatService } from '../../chat.service';
import {AuthService} from '../../../../services/auth.service';
import {AppConfig} from '../../../../services/AppConfig';

import { Pipe, PipeTransform } from '@angular/core';
import {SocketService} from '../../../../services/socket.service';

@Pipe({
  name: 'filterX'
})
export class FilterPipe implements PipeTransform {

  transform(contacts: any, term: any): any {
    if (term === undefined) return contacts;
    // return updates people array
    return contacts.filter(function(contact) {
      const name = contact.first_name + ' ' + contact.last_name + ' ' + contact.full_name;
      if (name.toLowerCase().includes(term.toLowerCase())) {
        return contact;
      }
    });
  }

}

@Component({
  selector: 'app-chat-user-sidebar',
  templateUrl: './chat-user-sidebar.component.html',

})
export class ChatUserSidebarComponent implements OnInit {
  // Public
  public userProfile;

  /**
   * Constructor
   *
   * @param {ChatService} _chatService
   * @param {CoreSidebarService} _coreSidebarService
   */
  constructor(
      public auth: AuthService,
      public config: AppConfig,
      private _socket: SocketService,
      private _chatService: ChatService,
      private _coreSidebarService: CoreSidebarService
  ) {}

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle Sidebar
   *
   * @param name
   */
  toggleSidebar(name) {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  /**
   * Update User Status
   */
  updateUserStatus(status) {
    this._socket.socket.emit('update online status', {status});
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.userProfile = this._chatService.userProfile;
  }
}
