import { Component, OnInit } from '@angular/core';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';
import { AppConfig } from 'app/dentwe/services/AppConfig';
import { ContactService } from 'app/dentwe/services/contact.service';

import { ChatService } from '../../chat.service';

@Component({
  selector: 'app-chat-active-sidebar',
  templateUrl: './chat-active-sidebar.component.html'
})
export class ChatActiveSidebarComponent implements OnInit {
  // Public
  public chatUser;

  constructor(
    private _coreSidebarService: CoreSidebarService,
    public contact: ContactService,
    public config: AppConfig
    ) {}


  toggleSidebar(name) {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  ngOnInit(): void {
    this.contact.onSelectContactChange.subscribe(res => {
      if (res) {
        this.chatUser = res.to_user;
      }
    });
  }
}
