import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { CoreSidebarModule } from '@core/components';
import { CoreCommonModule } from '@core/common.module';

import {ChatUserSidebarComponent, FilterPipe} from './chat-sidebars/chat-user-sidebar/chat-user-sidebar.component';
import { ChatActiveSidebarComponent } from './chat-sidebars/chat-active-sidebar/chat-active-sidebar.component';
import { ChatComponent } from './chat.component';
import { ChatService } from './chat.service';
import {TimeagoModule} from 'ngx-timeago';
import { ChatContentComponent } from './chat-content/chat-content.component';
import {BlockUIModule} from 'ng-block-ui';
import {LightgalleryModule} from 'lightgallery/angular';
import {SkeletonModule} from 'primeng/skeleton';
import { MessageComponent } from './message/message.component';
import {EmojiModule} from '@ctrl/ngx-emoji-mart/ngx-emoji';
import {PickerModule} from '@ctrl/ngx-emoji-mart';

// routing
const routes: Routes = [
  {
    path: '**',
    component: ChatComponent,
    resolve: {
      chatData: ChatService
    },
    data: { animation: 'chat' }
  }
];

@NgModule({
  declarations: [
    ChatComponent,
    ChatUserSidebarComponent,
    ChatActiveSidebarComponent,
    ChatContentComponent,
    FilterPipe,
    MessageComponent
  ],
  imports: [
    EmojiModule,
    PickerModule,
    SkeletonModule,
    LightgalleryModule,
    BlockUIModule.forRoot(),
    CommonModule,
    CoreSidebarModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    PerfectScrollbarModule,
    NgbModule,
    TimeagoModule
  ],
  providers: [ChatService]
})
export class ChatModule {}
