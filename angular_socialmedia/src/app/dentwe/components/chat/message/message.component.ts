import {Component, Input, OnInit} from '@angular/core';
import {ContactService} from '../../../services/contact.service';
import {AppConfig} from '../../../services/AppConfig';
import {SocketService} from '../../../services/socket.service';
import {AuthService} from '../../../services/auth.service';
import {API} from '../../../services/API';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {


  @Input() chatRef: any;

  constructor(
      public contact: ContactService,
      public config: AppConfig,
      private _socket: SocketService,
      public auth: AuthService,
      private _api: API
  ) { }

  ngOnInit(): void {
    if (!this.chatRef.seen && this.chatRef.from_id != this.auth.user.id) {
      this._socket.socket.emit('seen message', {message: this.chatRef});
    }
  }

}
