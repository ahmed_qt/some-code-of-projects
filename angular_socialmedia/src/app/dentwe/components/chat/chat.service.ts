import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class ChatService {
  public contacts: any[];
  public chats: any[];
  public userProfile;
  public isChatOpen: Boolean;
  public chatUsers: any[];
  public selectedChat;
  public selectedChatUser;

  public onContactsChange: BehaviorSubject<any>;
  public onChatsChange: BehaviorSubject<any>;
  public onSelectedChatChange: BehaviorSubject<any>;
  public onSelectedChatUserChange: BehaviorSubject<any>;
  public onChatUsersChange: BehaviorSubject<any>;
  public onChatOpenChange: BehaviorSubject<Boolean>;
  public onUserProfileChange: BehaviorSubject<any>;

  constructor(private _httpClient: HttpClient) {
    this.isChatOpen = false;
    this.onContactsChange = new BehaviorSubject([]);
    this.onChatsChange = new BehaviorSubject([]);
    this.onSelectedChatChange = new BehaviorSubject([]);
    this.onSelectedChatUserChange = new BehaviorSubject([]);
    this.onChatUsersChange = new BehaviorSubject([]);
    this.onChatOpenChange = new BehaviorSubject(false);
    this.onUserProfileChange = new BehaviorSubject([]);
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise<void>((resolve, reject) => {
      Promise.all([
        this.getContacts(),
        this.getChats(),
        this.getUserProfile(),
        this.getActiveChats(),
        this.getChatUsers()
      ]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get Contacts
   */
  getContacts(): Promise<any[]> {
    const url = `api/chat-contacts`;

    return new Promise((resolve, reject) => {
      // this._httpClient.get(url).subscribe((response: any) => {
        this.contacts = ChatFakeData.contacts;
        this.onContactsChange.next(this.contacts);
        resolve(this.contacts);
      // }, reject);
    });
  }

  /**
   * Get Chats
   */
  getChats(): Promise<any[]> {
    const url = `api/chat-chats`;

    return new Promise((resolve, reject) => {
      // this._httpClient.get(url).subscribe((response: any) => {
        this.chats = ChatFakeData.chats;
        this.onChatsChange.next(this.chats);

        resolve(this.chats);
      // }, reject);
    });
  }

  /**
   * Get User Profile
   */
  getUserProfile(): Promise<any[]> {
    const url = `api/chat-profileUser`;

    return new Promise((resolve, reject) => {
      // this._httpClient.get(url).subscribe((response: any) => {
        this.userProfile = ChatFakeData.profileUser;
        this.onUserProfileChange.next(this.userProfile);
        resolve(this.userProfile);
      // }, reject);
    });
  }

  /**
   * Get Selected Chat User
   *
   * @param userId
   */
  getSelectedChatUser(userId) {
    const selectUser = this.contacts.find(contact => contact.id === userId);
    this.selectedChatUser = selectUser;

    this.onSelectedChatUserChange.next(this.selectedChatUser);
  }

  /**
   * Get Active Chats
   */
  getActiveChats() {
    const chatArr = this.chats.filter(chat => {
      return this.contacts.some(contact => {
        return contact.id === chat.userId;
      });
    });
  }

  /**
   * Get Chat Users
   */
  getChatUsers() {
    const contactArr = this.contacts.filter(contact => {
      return this.chats.some(chat => {
        return chat.userId === contact.id;
      });
    });
    this.chatUsers = contactArr;
    this.onChatUsersChange.next(this.chatUsers);
  }

  /**
   * Selected Chats
   *
   * @param id
   */
  selectedChats(id) {
    const selectChat = this.chats.find(chat => chat.userId === id);

    // If Chat is Avaiable of Selected Id
    if (selectChat !== undefined) {
      this.selectedChat = selectChat;

      this.onSelectedChatChange.next(this.selectedChat);
      this.getSelectedChatUser(id);
    }
    // Else Create New Chat
    else {
      const newChat = {
        userId: id,
        unseenMsgs: 0
      };
      this.onSelectedChatChange.next(newChat);
      this.getSelectedChatUser(id);
    }
  }

  /**
   * Create New Chat
   *
   * @param id
   * @param chat
   */
  createNewChat(id, chat) {
    const newChat = {
      userId: id,
      unseenMsgs: 0,
      chat: [chat]
    };

    if (chat.message !== '') {
      return new Promise<void>((resolve, reject) => {
        // this._httpClient.post('api/chat-chats/', { ...newChat }).subscribe(() => {
          this.getChats();
          this.getChatUsers();
          this.getSelectedChatUser(id);
          this.openChat(id);
          resolve();
        // }, reject);
      });
    }
  }

  /**
   * Open Chat
   *
   * @param id
   */
  openChat(id) {
    this.isChatOpen = true;
    this.onChatOpenChange.next(this.isChatOpen);
    this.selectedChats(id);
  }

  /**
   * Update Chat
   *
   * @param chats
   */
  updateChat(chats) {
    return new Promise<void>((resolve, reject) => {
      // this._httpClient.post('api/chat-chats/' + chats.id, { ...chats }).subscribe(() => {
        this.getChats();
        resolve();
      // }, reject);
    });
  }

  /**
   * Update User Profile
   *
   * @param userProfileRef
   */
  updateUserProfile(userProfileRef) {
    this.userProfile = userProfileRef;
    this.onUserProfileChange.next(this.userProfile);
  }
}


export class ChatFakeData {
  public static profileUser = {
    id: 11,
    avatar: 'assets/images/portrait/small/avatar-s-11.jpg',
    fullName: 'John Doe',
    role: 'admin',
    about:
        'Dessert chocolate cake lemon drops jujubes. Biscuit cupcake ice cream bear claw brownie brownie marshmallow.',
    status: 'online',
    settings: {
      isTwoStepAuthVerificationEnabled: true,
      isNotificationsOn: false
    }
  };
  public static contacts = [
    {
      id: 1,
      fullName: 'Felecia Rower',
      role: 'Frontend Developer',
      about: 'Cake pie jelly jelly beans. Marzipan lemon drops halvah cake. Pudding cookie lemon drops icing',
      avatar: 'assets/images/avatars/1.png',
      status: 'offline'
    },
    {
      id: 2,
      fullName: 'Adalberto Granzin',
      role: 'UI/UX Designer',
      about:
          'Toffee caramels jelly-o tart gummi bears cake I love ice cream lollipop. Sweet liquorice croissant candy danish dessert icing. Cake macaroon gingerbread toffee sweet.',
      avatar: 'assets/images/avatars/2.png',
      status: 'busy'
    }

  ];

  public static chats = [
    {
      id: 1,
      userId: 2,
      unseenMsgs: 2,
      chat: [
        {
          message: 'Hi',
          time: 'Mon Dec 10 2018 07:45:00 GMT+0000 (GMT)',
          senderId: 11
        },
        {
          message: 'Hello. How can I help You?',
          time: 'Mon Dec 11 2018 07:45:15 GMT+0000 (GMT)',
          senderId: 2
        },
        {
          message: 'Can I get details of my last transaction I made last month?',
          time: 'Mon Dec 11 2018 07:46:10 GMT+0000 (GMT)',
          senderId: 11
        },
        {
          message: 'We need to check if we can provide you such information.',
          time: 'Mon Dec 11 2018 07:45:15 GMT+0000 (GMT)',
          senderId: 2
        },
        {
          message: 'I will inform you as I get update on this.',
          time: 'Mon Dec 11 2018 07:46:15 GMT+0000 (GMT)',
          senderId: 2
        },
        {
          message: 'If it takes long you can mail me at my mail address.',
          time: 'dayBeforePreviousDay',
          senderId: 11
        }
      ]
    },
    {
      id: 2,
      userId: 1,
      unseenMsgs: 0,
      chat: [
        {
          message: "How can we help? We're here for you!",
          time: 'Mon Dec 10 2018 07:45:00 GMT+0000 (GMT)',
          senderId: 11
        },
        {
          message: 'Hey John, I am looking for the best admin template. Could you please help me to find it out?',
          time: 'Mon Dec 10 2018 07:45:23 GMT+0000 (GMT)',
          senderId: 1
        },
        {
          message: 'It should be Bootstrap 4 compatible.',
          time: 'Mon Dec 10 2018 07:45:55 GMT+0000 (GMT)',
          senderId: 1
        },
        {
          message: 'Absolutely!',
          time: 'Mon Dec 10 2018 07:46:00 GMT+0000 (GMT)',
          senderId: 11
        },
        {
          message: 'Modern admin is the responsive bootstrap 4 admin template.!',
          time: 'Mon Dec 10 2018 07:46:05 GMT+0000 (GMT)',
          senderId: 11
        },
        {
          message: 'Looks clean and fresh UI.',
          time: 'Mon Dec 10 2018 07:46:23 GMT+0000 (GMT)',
          senderId: 1
        },
        {
          message: "It's perfect for my next project.",
          time: 'Mon Dec 10 2018 07:46:33 GMT+0000 (GMT)',
          senderId: 1
        },
        {
          message: 'How can I purchase it?',
          time: 'Mon Dec 10 2018 07:46:43 GMT+0000 (GMT)',
          senderId: 1
        },
        {
          message: 'Thanks, from ThemeForest.',
          time: 'Mon Dec 10 2018 07:46:53 GMT+0000 (GMT)',
          senderId: 11
        },
        {
          message: 'I will purchase it for sure. 👍',
          time: 'previousDay',
          senderId: 1
        }
      ]
    }
  ];
}
