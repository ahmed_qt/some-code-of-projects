import {Component, OnInit, ViewChild, Input, ElementRef, AfterViewInit} from '@angular/core';
import Cropper from 'cropperjs';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent implements OnInit {
  @ViewChild('image', { static: false })
  public imageElement: ElementRef;

  @Input('aspectRatio')
  private aspectRatio: number;

  // @Input('src')
  public imageSource: string;

  public imageDestination: string;
  private cropper: Cropper;


  public file: File;

  public cropData = {
    width: 0,
    height: 0,
    x: 0,
    y: 0
  };

  public constructor() {
    this.imageDestination = '';
  }

  initCropperjs() {
    console.log('init');
    console.log(this.imageElement.nativeElement);
    if (this.cropper) {
      this.cropper.replace(this.imageSource);
      return;
    }
    this.cropper = new Cropper(this.imageElement.nativeElement, {
      zoomable: true,
      scalable: false,
      aspectRatio: this.aspectRatio,
      crop: (event) => {
        this.cropData.width = event.detail.width;
        this.cropData.height = event.detail.height;
        this.cropData.x = event.detail.x;
        this.cropData.y = event.detail.y;
        // console.log('x ', event.detail.x);
        // console.log('y ', event.detail.y);
        // console.log('width ', event.detail.width);
        // console.log('height ', event.detail.height);
        // console.log('rotate ', event.detail.rotate);
        // console.log('scaleX ', event.detail.scaleX);
        // console.log('scaleY ', event.detail.scaleY);
        // const canvas = this.cropper.getCroppedCanvas();
        // this.imageDestination = canvas.toDataURL('image/png');
      }
    });
  }

  public ngOnInit() { }

  fileChanged($event) {
    const files = $event.target.files;
    if (files && files.length) {
      this.imageSource = URL.createObjectURL(files[0]);
      this.imageElement.nativeElement.src = URL.createObjectURL(files[0]);
      this.file = files[0];
      this.initCropperjs();
    }
  }
}
