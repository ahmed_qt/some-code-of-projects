import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder} from '@angular/forms';
import {CoreSidebarService} from '../../../../@core/components/core-sidebar/core-sidebar.service';
import {API} from '../../services/API';
import {AppConfig} from '../../services/AppConfig';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {ContactService} from '../../services/contact.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-reactions',
  templateUrl: './view-reactions.component.html',
  styleUrls: ['./view-reactions.component.scss']
})
export class ViewReactionsComponent implements OnInit {

  @Input() type: string;
  @Input() data: any;
  @Input() active: any = 'like';


  public likeArray: any = [];
  public laughArray: any = [];
  public loveArray: any = [];
  public sadArray: any = [];
  public angryArray: any = [];

  constructor(
      public auth: AuthService,
      private _api: API,
      public config: AppConfig,
  ) { }

  ngOnInit(): void {
    this._api.post('v1/reactions', {type: this.type, id: this.data.id}).subscribe(res => {
      for (const i of res) {
        if (i['reaction']['name'] == 'like') {
          this.likeArray.push(i['user']);
        } else if (i['reaction']['name'] == 'laugh') {
          this.laughArray.push(i['user']);
        } else if (i['reaction']['name'] == 'love') {
          this.loveArray.push(i['user']);
        } else if (i['reaction']['name'] == 'sad') {
        this.sadArray.push(i['user']);
        } else if (i['reaction']['name'] == 'angry') {
        this.angryArray.push(i['user']);
        }
      }
    });
  }

}
