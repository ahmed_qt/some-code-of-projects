import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import { AppConfig } from 'app/dentwe/services/AppConfig';
import { AuthService } from 'app/dentwe/services/auth.service';
import {API} from '../../services/API';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFromComponent implements OnInit, OnDestroy {

  @Input() type;
  @Input() user;
  @Input() post;

  @Output() onPostComplete: EventEmitter<any> = new EventEmitter<any>();

  public loading = false;
  public isEmojiPickerVisible = false;
  public imagesFiles = [];
  public videoFile;

  postPrivacy = [
    {
      code: 1,
      name: 'Public',
      icon: 'pi pi-globe'
    },
    {
      code: 2,
      name: 'Followers',
      icon: 'pi pi-users'
    },
    {
      code: 3,
      name: 'Only me',
      icon: 'pi pi-lock'
    },
  ];

  selectedPrivacy: any = this.postPrivacy[0];

  public postType = 'text'; // text, photo, video, feeling
  public postText = '';
  public postFeeling = '';
  public youtubeLink = '';

  constructor(
    public config: AppConfig,
    public auth: AuthService,
    private _api: API,
  ) {
  }

  ngOnInit(): void {
    if (this.post && this.type == 'edit') {
      this.postText = this.post.post_text;
      this.selectedPrivacy = this.postPrivacy.filter(i => i.code == this.post.post_privacy)[0];
    }
  }

  changePostType(type) {
    if (this.postType == type) {
      this.postType = 'text';
      return;
    }
    this.postType = type;
  }

  ngOnDestroy(): void {
    this.postType = 'text';
  }

  onImagesChange($event: any) {
    this.imagesFiles = [];
    for (const i of $event.target.files) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagesFiles.push({url: reader.result as string, file: i});
      };
      reader.readAsDataURL(i);
    }
  }

  removeFile(file: any) {
    this.imagesFiles = this.imagesFiles.filter(i => {
      if (i.url == file.url) {
        return;
      }
      return i;
    });
  }

  onAddMoreImagesChange($event: any) {
    for (const i of $event.target.files) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagesFiles.push({url: reader.result as string, file: i});
      };
      reader.readAsDataURL(i);
    }

  }

  onVideoChange($event: any) {
    this.videoFile = $event.target.files[0];
  }

  doPost() {
    const data = new FormData();
    this.loading = true;
    data.append('post_text', this.postText);
    data.append('post_type', this.postType);
    data.append('post_privacy', this.selectedPrivacy.code);
    if (this.type == 'user') {
      data.append('recipient_id', this.user.id);
    }
    if (this.postType == 'photo') {
      data.append('images_count', String(this.imagesFiles.length));
      this.imagesFiles.forEach((value, i) => {
        data.append('image' + i, value.file);
      });
    } else if (this.postType == 'video') {
      data.append('video', this.youtube_parser(this.youtubeLink));
    } else if (this.postType == 'feeling') {
      data.append('feeling', this.postFeeling);
    }
    this._api.upload('v1/post', data).subscribe(res => {
      this.loading = false;
      this.postText = '';
      this.imagesFiles = [];
      this.youtubeLink = '';
      this.postFeeling = '';
      this.onPostComplete.emit(true);
    });
  }

  youtube_parser(url) {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    const match = url.match(regExp);
    return (match && match[7].length == 11) ? match[7] : false;
  }

  validate() {
    if (this.postType == 'photo') {
      if (this.imagesFiles.length) {
        return false;
      }
      return true;
    } else if (this.postType == 'video') {
      if (this.youtube_parser(this.youtubeLink)) {
        return false;
      }
      return true;
    } else if (this.postType == 'feeling') {
      if (this.postFeeling) {
        return false;
      }
      return true;
    } else {
      if (this.postText) {
        return false;
      }
      return true;
    }
  }

  editPost() {
    this.loading = true;

    this._api.put('v1/post', {
      post_id: this.post.id,
      post_text: this.postText,
      post_privacy: this.selectedPrivacy.code
    }).subscribe(res => {
      this.loading = false;
      this.onPostComplete.emit(res.data);
    });
  }

  sharePost() {
    this.loading = true;

    this._api.post('v1/post/share', {
      post_id: this.post.id,
      post_text: this.postText,
      post_privacy: this.selectedPrivacy.code
    }).subscribe(res => {
      this.loading = false;
      this.onPostComplete.emit(res.data);
    });
  }


    addEmoji($event: any) {
      const text = `${$event.emoji.native}`;

      this.postText += text;
    }
}
