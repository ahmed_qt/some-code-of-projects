import {AfterViewInit, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {AuthService} from '../../services/auth.service';
import {AppConfig} from '../../services/AppConfig';
import {API} from '../../services/API';
import {SocketService} from '../../services/socket.service';
import {ToastrService} from 'ngx-toastr';
import {NgScrollbar} from 'ngx-scrollbar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('scroll') scrollbarRef: NgScrollbar;


  // public
  public contentHeader: object;

  public userSuggestions = [];


  public posts = [];
  public loading = false;
  public loadingMore = false;
  public postsCurrentPage = 1;
  public postsLastPage;


  // private
  private _unsubscribeAll: Subject<any>;


  constructor(
      public config: AppConfig,
      public auth: AuthService,
      private _api: API,
      private _socket: SocketService,
      private _toast: ToastrService,

  ) {
    this._unsubscribeAll = new Subject();
  }

  ngAfterViewInit(): void {
    this.scrollbarRef.scrolled.subscribe(e => {
      const d: any = e.target;
      // console.log(d.scrollTop);
      // console.log(d.offsetHeight);
      // console.log(d.scrollHeight);
      const pos = d.scrollTop + d.offsetHeight;
      const max = d.scrollHeight;
      if (pos >= max )   {
        // action here
        if (this.loadingMore) {
          return;
        }
        if (this.postsCurrentPage != this.postsLastPage) {
          this.loadPosts(this.postsCurrentPage + 1);
        }
      }
    });
  }

  /**
   * On init
   */

  loadPosts(page = 1, hard = false) {
    this.loadingMore = true;
    if (hard) {
      this.posts = [];
    }
    this._api.get('v1/posts' + '?page=' + page).subscribe(res => {
      this.loadingMore = false;
      this.postsCurrentPage = res.current_page;
      this.postsLastPage = res.last_page;
      if (hard) {
        this.posts = res.data;
      } else {
        this.posts = [...this.posts, ...res.data];
      }
    }, e => {
      this.loadingMore = false;
    });
  }


  ngOnInit(): void {

    // load suggestions
    this._api.get('v1/suggestions/users').subscribe(response => {
      this.userSuggestions = response;
    });

    // this.loading = true;
    this.loadPosts(1, true);


    // content header
    this.contentHeader = {
      headerTitle: 'Home',
      actionButton: true,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Home',
            isLink: false
          }
        ]
      }
    };
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    const max = document.documentElement.scrollHeight;
    if (pos === max )   {
      // action here
      if (this.loadingMore) {
        return;
      }
      if (this.postsCurrentPage != this.postsLastPage) {
        this.loadPosts(this.postsCurrentPage + 1);
      }
    }
  }

  deletePost(post) {
    this._api.delete('v1/post/' + post.id).subscribe(res => {
      this.posts = this.posts.filter(i => i.id != post.id);
      this._toast.info('Post Deleted');
    });
  }

  onScroll($event: any) {
    console.log($event)
    const pos = ($event.srcElement.scrollTop || $event.srcElement.scrollTop) + $event.srcElement.offsetHeight;
    const max = $event.srcElement.scrollHeight;
    if (pos === max )   {
      // action here
      if (this.loadingMore) {
        return;
      }
      if (this.postsCurrentPage != this.postsLastPage) {
        this.loadPosts(this.postsCurrentPage + 1);
      }
    }
  }
}
