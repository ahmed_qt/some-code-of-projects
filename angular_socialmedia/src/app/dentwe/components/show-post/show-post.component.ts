import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AppConfig} from '../../services/AppConfig';
import {AuthService} from '../../services/auth.service';
import {API} from '../../services/API';
import {SocketService} from '../../services/socket.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styleUrls: ['./show-post.component.scss']
})
export class ShowPostComponent implements OnInit {

  public error = null;
  public loading = false;
  public post = null;

  @Input() type: any = '';
  @Input() post_id: any;


  constructor(
      private _route: ActivatedRoute,
      public config: AppConfig,
      public auth: AuthService,
      private _api: API,
      private _socket: SocketService,
      private _toast: ToastrService,
  ) { }

  ngOnInit(): void {

    if (this.type == 'share') {
      this.error = null;
      this.post = null;
      this.load(this.post_id);
      return;
    }
    this._route.params.subscribe(params => {
        if (params['id'] != undefined) {
          this.error = null;
          this.post = null;
          this.load(params['id']);
        } else {
          this.error = '';
        }
    });
  }

  load(id: any) {
    this.loading = true;
    this._api.get('v1/post/' + id).subscribe(response => {
      this.loading = false;
      this.post = response;
    }, error => {
      this.error = 1;
      this.loading = false;
    });
  }

  deletePost(post) {
    this._api.delete('v1/post/' + post.id).subscribe(res => {
      this._toast.info('Post Deleted');
    });
  }

}
