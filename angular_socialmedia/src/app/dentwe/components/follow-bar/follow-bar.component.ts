import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {API} from '../../services/API';
import {AppConfig} from '../../services/AppConfig';
import {FollowService} from '../../services/follow.service';

@Component({
  selector: 'app-follow-bar',
  templateUrl: './follow-bar.component.html',
  styleUrls: ['./follow-bar.component.scss']
})
export class FollowBarComponent implements OnInit {

  public loading = false;
  public acceptLoading = false;
  public denyLoading = false;

  @Input() user: any;

  constructor(
      public auth: AuthService,
      public api: API,
      public config: AppConfig,
      public follow: FollowService,
  ) {}


  ngOnInit(): void {
  }


  accept(request_id) {
    this.loading = true;
    this.acceptLoading = true;
    this.api.post('v1/followrequest/accept', {request_id}).subscribe(response => {
      // this.follow.followRequests = response;
      this.acceptLoading = false;
      this.follow.followrequests_count = response;
      this.user.followed_me.status = 1;
    }, error => {
      this.acceptLoading = false;
    });
  }

  deny(request_id) {
    this.loading = true;
    this.denyLoading = true;
    this.api.post('v1/followrequest/deny', {request_id}).subscribe(response => {
      // this.follow.followRequests = response;
      this.denyLoading = false;
      this.follow.followrequests_count = response;
      this.user.followed_me = null;
    }, error => {
      this.denyLoading = false;
    });
  }

}
