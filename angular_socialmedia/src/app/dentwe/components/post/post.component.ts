import {AfterViewChecked, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { AppConfig } from 'app/dentwe/services/AppConfig';
import { AuthService } from 'app/dentwe/services/auth.service';
import lgZoom from 'lightgallery/plugins/zoom';
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgFullscreen from 'lightgallery/plugins/fullscreen';
import lgAutoPlay from 'lightgallery/plugins/autoplay';
import lgMediumZoom from 'lightgallery/plugins/mediumZoom';
import lgShare from 'lightgallery/plugins/share';


import { BeforeSlideDetail } from 'lightgallery/lg-events';

import { ViewEncapsulation } from "@angular/core";
import {ConfirmationService} from 'primeng/api';
import {API} from '../../services/API';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class PostComponent implements OnInit {

  @Input() data: any;
  @Input() type: any;
  @Input() loadAll = false;

  public show_comments = false;
  public sharePostDialog = false;
  public editPostDialog = false;

  public commentsLoading = false;
  public comments = [];
  public comment = '';
  public commentsCurrentPage = 1;
  public commentsLastPage;


  public reactions_visiable = false;

  @Output() onPostDelete: EventEmitter<any> = new EventEmitter<any>();

  public nav_comment_id = null;
  public nav_reply_id = null;

  public activeReaction: any;
  public activeReactionData: any;
  public activeReactionType: any;



  constructor(
    public auth: AuthService,
    public config: AppConfig,
    private _api: API,
    private confirmationService: ConfirmationService,
    private _toast: ToastrService,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      if (params['comment_id']) {
        this.nav_comment_id = params['comment_id'];
        this.nav_reply_id = null;

        let int = setInterval(() => {
          setTimeout(() => {
            clearInterval(int);
          }, 60000);
          const el = document.getElementById('comment-' + this.nav_comment_id);
          if (el) {
            el.scrollIntoView({behavior: 'smooth'});
            clearInterval(int);
          }

        }, 100);
      } else if (params['reply_id']) {
        this.nav_comment_id = null;
        this.nav_reply_id = params['reply_id'];
        let int = setInterval(() => {
          setTimeout(() => {
            clearInterval(int);
          }, 60000);
          const el = document.getElementById('reply-' + this.nav_reply_id);
          if (el) {
            el.scrollIntoView({behavior: 'smooth'});
            clearInterval(int);
          }

        }, 100);
      }
    });


  }


  settings = {

    counter: false,
    plugins: [lgZoom, lgThumbnail, lgFullscreen, lgAutoPlay, lgShare],
    thumbnail: true,

  };
  onBeforeSlide = (detail: BeforeSlideDetail): void => {
    const { index, prevIndex } = detail;
    console.log(index, prevIndex);
  }

  // public reactionsGroup = {
  //   smile: [],
  //   laugh: [],
  //   love: [],
  //   sad: [],
  //   cry: [],
  //   idea: [],
  // };

  ngOnInit(): void {


    if (this.loadAll) {
      this.show_comments = true;
      this.loadComments(1, true);
    }

    // for (const i of this.data.reactions) {
    //   switch (i.reaction.name) {
    //     case 'like':
    //       this.reactionsGroup.smile.push(i);
    //       break;
    //     case 'laugh':
    //       this.reactionsGroup.laugh.push(i);
    //       break;
    //     case 'love':
    //       this.reactionsGroup.love.push(i);
    //       break;
    //     case 'sad':
    //       this.reactionsGroup.sad.push(i);
    //       break;
    //     case 'cry':
    //       this.reactionsGroup.cry.push(i);
    //       break;
    //     case 'idea':
    //       this.reactionsGroup.idea.push(i);
    //       break;
    //   }
    // }

  }

  deletePost() {
    this.confirmationService.confirm({
      message: 'Delete Post?',
      icon: 'pi pi-exclamation-triangle',
      accept: () =>{
        this.onPostDelete.emit(this.data);
      }
    });
  }

  reportPost() {

  }

  showComments() {
    this.show_comments = !this.show_comments;
    if (this.comments.length) return;
    this.loadComments();
  }

  loadComments(page = 1, hard = false) {
    this.commentsLoading = true;
    this._api.post('v1/post/comments', {post_id: this.data.id, page: page, load_all: this.loadAll}).subscribe((res: any) => {
      this.commentsLoading = false;
      this.commentsCurrentPage = res.current_page;
      this.commentsLastPage = res.last_page;
      if (hard) {
        this.comments = res.data;
      } else {
        this.comments = [...this.comments, ...res.data];
      }
      setTimeout(() => {
        // this.scrollToBottom();
      }, 0);
    });
  }

  postComment() {
    if (!this.comment) return;
    this._api.post('v1/post/comment', {post_id: this.data.id, comment: this.comment}).subscribe(res => {
      const comment = res.data;
      comment.user = this.auth.user;
      // this.comments.push(comment);
      this.comment = '';
      // this.data.comments_count += 1;
      this.loadComments(1, true);
    });
  }

  viewMore() {
    if (this.commentsCurrentPage != this.commentsLastPage) {
      this.loadComments(this.commentsCurrentPage + 1);
    }
  }

  scrollToBottom(): void {
      window.scrollTo(0, document.body.scrollHeight);
  }

    removeReaction(reaction) {
        this._api.post('v1/post/removeReaction', {post_id: this.data.id}).subscribe(res => {
          console.log('removed');
          this.data.my_reaction = null;
          switch (reaction.reaction.name) {
            case 'like':
              this.data.like_reactions_count -= 1;
              break;
            case 'laugh':
              this.data.laugh_reactions_count -= 1;
              break;
            case 'love':
              this.data.love_reactions_count -= 1;
              break;
            case 'sad':
              this.data.sad_reactions_count -= 1;
              break;
            case 'angry':
              this.data.angry_reactions_count -= 1;
              break;
          }
        });
    }

  showReactionsDialog(reaction: any, type: any, data: any) {
    this.activeReaction = reaction;
    this.activeReactionData = data;
    this.activeReactionType = type;
    this.reactions_visiable = true;
  }

  deleteComment(comment: any) {
    this.confirmationService.confirm({
      message: 'Delete Comment?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this._api.delete('v1/comment/' + comment.id).subscribe(res => {
          this.comments = this.comments.filter(i => i.id != comment.id);
          this.data.replies_count -= 1;
          this._toast.info('Comment Deleted');
        });
      }
    });
  }

  removeCommentReaction(comment: any) {
    this._api.post('v1/comment/removeReaction', {comment_id: comment.id}).subscribe(res => {
      console.log('removed');
      switch (comment.my_reaction.reaction.name) {
        case 'like':
          comment.like_reactions_count -= 1;
          break;
        case 'laugh':
          comment.laugh_reactions_count -= 1;
          break;
        case 'love':
          comment.love_reactions_count -= 1;
          break;
        case 'sad':
          comment.sad_reactions_count -= 1;
          break;
        case 'angry':
          comment.angry_reactions_count -= 1;
          break;
      }
      comment.my_reaction = null;
    });
  }

  onEditComplete($event: any) {
    this.data.post_text = $event.post_text;
    this.data.post_privacy = $event.post_privacy;
  }

  shareLink() {

      const shareData = {
        title: this.data.post_text,
        text: this.data.post_text,
        url: 'https://app.dentwe.com/#/post/' + this.data.id
      };

      try {
        navigator.share(shareData).then(() => {});
      } catch (err: any) {
      }
    }
}
