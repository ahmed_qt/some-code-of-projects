import {Component, Input, OnInit} from '@angular/core';
import {API} from '../../services/API';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-follow-btn',
  templateUrl: './follow-btn.component.html',
  styleUrls: ['./follow-btn.component.scss']
})
export class FollowBtnComponent implements OnInit {

  @Input() public type;
  @Input() public user;

  public loading = false;

  constructor(
      private _api: API,
      public auth: AuthService,
      private _toastr: ToastrService,
  ) { }

  ngOnInit(): void {

  }

  follow() {
    if (this.loading) return;
    this.loading = true;
    this._api.post('v1/user/follow', {user_id: this.user.id}).subscribe(response => {
      this.loading = false;
      this.user.following = response.status;
      this.user.following_count = response.data.following_count;
      this.user.follower_count = response.data.follower_count;
    }, error => {
      this.loading = false;
      this._toastr.error('Error Occurred');
    });
  }

  unfollow() {
    if (this.loading) return;
    this.loading = true;
    this._api.post('v1/user/unfollow', {user_id: this.user.id}).subscribe(response => {
      this.loading = false;
      this.user.following = response.status;
      this.user.following_count = response.data.following_count;
      this.user.follower_count = response.data.follower_count;
    }, error => {
      this.loading = false;
      this._toastr.error('Error Occurred');
    });
  }

}
