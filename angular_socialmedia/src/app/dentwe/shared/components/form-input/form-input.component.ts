import { HttpClient } from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';

@Component({
  selector: 'form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss']
})
export class FormInputComponent implements OnInit {

  separateDialCode = false;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;

  @Input() form: UntypedFormGroup;
  @Input() title: any;
  @Input() key: any;
  @Input() placeholder: any;
  @Input() submitted: any;
  @Input() type: any;
  @Input() data: any;
  @Input() optionLabel: any;
  @Input() optionValue: any;
  @Input() allowDropDown = false;
  @Input() allowMultiple = false;
  
  @Input() filterMethod: any;
  filterData: any;
  
  get f() {
    return this.form.controls;
  }

  constructor(
  ) { }

  ngOnInit(): void {
  }

  async completeMethod($event) {
    this.filterData = await this.filterMethod($event);
  }

}
