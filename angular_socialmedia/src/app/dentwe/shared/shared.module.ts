import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
import { ChipsModule } from 'primeng/chips';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { FormInputComponent } from './components/form-input/form-input.component';
import { CalendarModule } from 'primeng/calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import { HttpClientModule } from '@angular/common/http';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import {InputNumberModule} from 'primeng/inputnumber';



@NgModule({
  declarations: [FormInputComponent, FileUploadComponent],
  imports: [
    InputNumberModule,
    CommonModule,
    DropdownModule,
    ChipsModule,
    AutoCompleteModule,
    FormsModule,
    ReactiveFormsModule,
    NgxIntlTelInputModule,
    CalendarModule,
    MultiSelectModule,
    HttpClientModule
  ],
  exports: [FormInputComponent]
})
export class SharedModule { }
