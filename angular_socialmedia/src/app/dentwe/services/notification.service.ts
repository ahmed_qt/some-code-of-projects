import { Injectable } from '@angular/core';
import {API} from './API';
import {AppConfig} from './AppConfig';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public notifications_count = null;
  public notifications = [];


  constructor(
      private api: API,
      private _config: AppConfig,
  ) {
    setInterval(() => {
      this.getNotificationsCount();
    }, 30000);
  }

  public getNotificationsCount() {
    if (this._config.isTokenExpired()) {
      return;
    }
    this.api.get('v1/notifications_count').subscribe(response => {
      this.notifications_count = response;
    });
  }
}
