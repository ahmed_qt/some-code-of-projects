import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

declare let swal;

interface ConfirmArguments {
    title: string;
    type?: string;
    text: string;
    confirmButtonText?: any;
}

interface AlertArguemtns {
    title?: string;
    text: string;
    type?: string;
}

@Injectable({
    providedIn: 'root'
})
export class SweetAlert {

    private response: Observable<any>;


    /**
     * Prompt for input
     * @method prompt
     * @param {String} title : title to be shown on prompt
     * @return {Observable}
     */
    prompt(title: string): any {
        this.response = new Observable(observer => {
            swal({
                title: title,
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                animation: false
            }).then(
                function(text) {
                    observer.next(text);
                    observer.complete();
                },
                function() {
                    // observer.error('canceled');
                    observer.complete();
                }
            );
        });
        return this.response;
    }



    /**
     * Confirm action with Yes or No buttons
     * @method confirm
     * @param {String} title : title to be shown on prompt
     * @return {Observable}
     */
    confirm(arg: ConfirmArguments): any {
        this.response = new Observable(observer => {
            swal({
                title: arg.title,
                text: arg.text,
                type: arg.type,
                showCancelButton: true,
                animation: false,
                confirmButtonText: arg.confirmButtonText || 'Ok',
            }).then(
                function() {
                    observer.next('ok');
                    observer.complete();
                },
                function() {
                    // observer.error('canceled');
                    observer.complete();
                }
            );
        });
        return this.response;
    }


    /**
     * Show alert dialog
     * @method alert
     * @param {AlertArguments} arg : Arguments {title,text,type}
     * @return {void}
     */
    alert(arg: AlertArguemtns): void {
        swal(
            arg.title || '',
            arg.text,
            arg.type || 'default'
        );
    }

}
