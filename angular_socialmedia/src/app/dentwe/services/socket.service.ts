import { Injectable } from '@angular/core';
import { io, Socket } from 'socket.io-client';
import {AppConfig} from './AppConfig';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from './auth.service';
import {MessageService} from 'primeng/api';
import {NotificationService} from './notification.service';
import {FollowService} from './follow.service';
import {ContactService} from './contact.service';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  public socket: Socket;

  constructor(
      private _config: AppConfig,
      private _toast: ToastrService,
      private _notification: NotificationService,
      private _follow: FollowService,
      private _contact: ContactService,
      private messageService: MessageService
  ) {
      this.socket = io(_config.getWebsocketUrl(), {autoConnect: false});
      this.socket.on('connect_error', (err: any) => {
      console.error(err.message);
      console.error(err.data);
    });
    this.socket.onAny((event, ...args) => {
      // console.log(event, args);
    });

      this.socket.on('contacts', (data: any) => {
          // console.log('contacts', data);
          if (this._contact.contacts.length) {
              const audio = new Audio();
              audio.src = '/assets/sounds/notification.mp3';
              audio.play();
          }
          this._contact.contacts = data.contacts;

          console.log(data)

          this._contact.not_read_contacts = data.contacts.filter(i => {
              if (!i.message_id) {
                  return;
              }
              if (!i.message.seen && i.message.from_id != data.user_id) {
                  return i;
              }
          });
      });


    this.socket.on('private notification', (data: any) => {
        console.log(data);
        this._notification.getNotificationsCount();
        if (data.type == 'follow_request') {
            this._follow.getFollowRequestsCount();
        }
        const audio = new Audio();
        audio.src = '/assets/sounds/notification.mp3';
        audio.play();
        this.messageService.add(
              {
                severity: 'custom',
                summary: 'Custom',
                closable: false,
                detail: 'Message Content',
                data: data,
                life: 5000
              });
        });

  }

  public connect() {
    this.socket.auth = {token: localStorage.getItem(this._config.tokenName)};
    this.socket.connect();
  }

  public sent(user_id) {
    this.socket.emit('private message', {to: user_id});
  }

  public sendMessage(data) {
    this.socket.emit('private message', data);
  }

  public sendPhoto(data) {
    this.socket.emit('private photo', data);
  }

  disconnect() {
    this.socket.disconnect();
  }
}
