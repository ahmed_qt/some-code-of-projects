import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';

import 'hammerjs';
import {NgbCollapseModule, NgbModule, NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {ToastrModule} from 'ngx-toastr'; // For auth after login toast
import {SplitterModule} from 'primeng/splitter';

import {CoreModule} from '@core/core.module';
import {CoreCommonModule} from '@core/common.module';
import {CoreSidebarModule, CoreThemeCustomizerModule} from '@core/components';

import {coreConfig} from 'app/app-config';

import {AppComponent} from 'app/app.component';
import {LayoutModule} from 'app/layout/layout.module';
import {RegisterComponent} from './dentwe/auth/register/register.component';
import {LoginComponent} from './dentwe/auth/login/login.component';
import {ProfileComponent} from './dentwe/components/profile/profile.component';
import {ContentHeaderModule} from './layout/components/content-header/content-header.module';
import {AuthService} from './dentwe/services/auth.service';
import {AuthGuard} from './dentwe/services/auth-guard';
import {API} from './dentwe/services/API';
import {LogService} from './dentwe/services/LogService';
import {AppConfig} from './dentwe/services/AppConfig';
import {AppRoutingModule} from './app.routing';
import {SweetAlert} from './dentwe/services/SweetAlert';
import {ForgetPasswordComponent} from './dentwe/auth/forget-password/forget-password.component';
import {ResetPasswordComponent} from './dentwe/auth/reset-password/reset-password.component';
import {ConfirmAccountComponent} from './dentwe/auth/confirm-account/confirm-account.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule, HashLocationStrategy, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {BlockUIModule} from 'ng-block-ui';
import {NetworkComponent} from './dentwe/components/network/network.component';
import {HomeComponent} from './dentwe/components/home/home.component';
import {FollowBtnComponent} from './dentwe/components/follow-btn/follow-btn.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import {PostFromComponent} from './dentwe/components/post-form/post-form.component';
import {AccountSettingsComponent} from './dentwe/components/account-settings/account-settings.component';
import {GalleriaModule} from 'primeng/galleria';
import {FollowTabComponent} from './dentwe/components/follow-tab/follow-tab.component';
import {ImageCropperComponent} from './dentwe/components/image-cropper/image-cropper.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {SocketService} from './dentwe/services/socket.service';
import {ToastModule} from 'primeng/toast';
import {ConfirmationService, MessageService} from 'primeng/api';
import {NotificationService} from './dentwe/services/notification.service';
import {FollowService} from './dentwe/services/follow.service';
import {ContactService} from './dentwe/services/contact.service';
import {FeatherModule} from 'angular-feather';
import {Clock, UserX, UserPlus, UserCheck} from 'angular-feather/icons';
import {PostComponent} from './dentwe/components/post/post.component';
import {LightgalleryModule} from 'lightgallery/angular';
import {DialogModule} from 'primeng/dialog';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {DropdownModule} from 'primeng/dropdown';
import {TooltipModule} from 'primeng/tooltip';
import {TimeagoClock, TimeagoModule} from 'ngx-timeago';
import {interval, Observable} from 'rxjs';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ReactionsDirective} from './dentwe/components/reactions.directive';
import {ReactionsComponent} from './dentwe/components/reactions/reactions.component';
import {NewPostComponent} from './dentwe/components/new-post/new-post.component';
import {ShowPostComponent} from './dentwe/components/show-post/show-post.component';
import {SkeletonModule} from 'primeng/skeleton';
import {RepliesComponent} from './dentwe/components/replies/replies.component';
import {PickerModule} from '@ctrl/ngx-emoji-mart';
import {EmojiModule} from '@ctrl/ngx-emoji-mart/ngx-emoji';
import {FollowBarComponent} from './dentwe/components/follow-bar/follow-bar.component';
import {SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';
import {ViewReactionsComponent} from './dentwe/components/view-reactions/view-reactions.component';
import {CalendarModule} from 'primeng/calendar';
import {MultiSelectModule} from 'primeng/multiselect';
import {ChipsModule} from 'primeng/chips';
import {JobsComponent} from './dentwe/components/jobs/jobs.component';
import {NgxIntlTelInputModule} from 'ngx-intl-tel-input';
import {TermsOfUseComponent} from './dentwe/components/terms-of-use/terms-of-use.component';
import {PrivacyPolicyComponent} from './dentwe/components/privacy-policy/privacy-policy.component';
import {InfoModule} from './dentwe/info/info.module';
import {DentistModule} from './dentwe/info/dentist/dentist.module';
import {ShopComponent} from './dentwe/components/shop/shop.component';
import {EcommerceDetailsComponent} from './dentwe/components/shop/ecommerce-details/ecommerce-details.component';
import {EcommerceSidebarComponent} from './dentwe/components/shop/sidebar/sidebar.component';
import {NouisliderModule} from 'ng2-nouislider';
import {JobFormComponent} from './dentwe/components/job-form/job-form.component';
import {ShopFormComponent} from './dentwe/components/shop-form/shop-form.component';
import {MenuModule} from 'primeng/menu';
import {MenuItem} from 'primeng/api';
import {SharedModule} from './dentwe/shared/shared.module';
import {ChipModule} from 'primeng/chip';
import {CheckboxModule} from 'primeng/checkbox';
import {RadioButtonModule} from 'primeng/radiobutton';
import {RatingModule} from 'primeng/rating';
import {SliderModule} from 'primeng/slider';
import { RegisterCompleteComponent } from './dentwe/auth/register-complete/register-complete.component';
import { RedZoomModule } from 'ngx-red-zoom';
import {NgScrollbarModule} from 'ngx-scrollbar';
import { ProfileImageDialogComponent } from './dentwe/auth/profile-image-dialog/profile-image-dialog.component';
import {DialogService, DynamicDialogModule} from 'primeng/dynamicdialog';

const icons = {
    Clock,
    UserX,
    UserPlus,
    UserCheck
};

// ticks every 30s
export class MyClock extends TimeagoClock {
    tick(then: number): Observable<number> {
        return interval(30000);
    }
}

@NgModule({
    declarations: [EcommerceDetailsComponent, AppComponent, EcommerceSidebarComponent, RegisterComponent, LoginComponent, ProfileComponent, ForgetPasswordComponent, ResetPasswordComponent, ConfirmAccountComponent, NetworkComponent, HomeComponent, FollowBtnComponent, PostFromComponent, AccountSettingsComponent, FollowTabComponent, ImageCropperComponent, PostComponent, ReactionsDirective, ReactionsComponent, NewPostComponent, ShowPostComponent, RepliesComponent, FollowBarComponent, ViewReactionsComponent, JobsComponent, ShopComponent, TermsOfUseComponent, PrivacyPolicyComponent, JobFormComponent, ShopFormComponent, RegisterCompleteComponent, ProfileImageDialogComponent],
    providers: [
        ConfirmationService,
        DialogService,
        {provide: LocationStrategy, useClass: PathLocationStrategy},
        AuthService, AuthGuard, API, LogService,
        AppConfig, SweetAlert, SocketService, MessageService, NotificationService,
        FollowService, ContactService],
    imports: [
        DynamicDialogModule,
        NgScrollbarModule,
        RedZoomModule,
        SliderModule,
        RatingModule,
        CheckboxModule,
        RadioButtonModule,
        ChipModule,
        CommonModule,
        DialogModule,
        NgbProgressbarModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmDialogModule,
        MenuModule,
        SplitterModule,
        NouisliderModule,
        InfoModule,
        NgxIntlTelInputModule,
        ChipsModule,
        MultiSelectModule,
        CalendarModule,
        SwiperModule,
        EmojiModule,
        PickerModule,
        SkeletonModule,
        TimeagoModule.forRoot(
            {
                clock: {provide: TimeagoClock, useClass: MyClock},
            }
        ),
        ConfirmDialogModule,
        TooltipModule,
        LightgalleryModule,
        PerfectScrollbarModule,
        DropdownModule,
        FeatherModule.pick(icons),
        DialogModule,
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot(),
        ToastrModule.forRoot(),
        BlockUIModule.forRoot(),
        // NgBootstrap
        NgbCollapseModule,
        NgSelectModule,
        FormsModule,
        NgbModule,
        ContentHeaderModule,
        ToastrModule.forRoot(),
        NgbCollapseModule,
        ImageCropperModule,

        GalleriaModule,
        AutoCompleteModule,
        ToastModule,

        // Core modules
        CoreModule.forRoot(coreConfig),
        CoreCommonModule,
        CoreSidebarModule,
        CoreThemeCustomizerModule,

        // App modules
        LayoutModule,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
