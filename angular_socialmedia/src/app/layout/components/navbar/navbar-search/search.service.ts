import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  // Public
  public search = '';
  public apiData = [];
  public onApiDataChange: BehaviorSubject<any>;
  public onIsBookmarkOpenChange: BehaviorSubject<any>;

  /**
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    this.onApiDataChange = new BehaviorSubject('');
    this.onIsBookmarkOpenChange = new BehaviorSubject(false);
    this.getSearchData();
  }

  searchData = [
    {
      groupTitle: 'Contacts',
      searchLimit: 4,
      data: [
        {
          title: 'Mia Davis',
          email: 'miadavis@teleworm.us',
          img: 'assets/images/portrait/small/avatar-s-8.jpg',
          date: '01/03/2020'
        },
        {
          title: 'Norris Carrière',
          email: 'NorrisCarriere@rhyta.com',
          img: 'assets/images/portrait/small/avatar-s-3.jpg',
          date: '07/03/2020'
        },
        {
          title: 'Charlotte Gordon',
          email: 'CharlotteGordon@jourrapide.com',
          img: 'assets/images/portrait/small/avatar-s-26.jpg',
          date: '14/03/2020'
        },
        {
          title: 'Robert Nash',
          email: 'RobertNash@dayrep.com',
          img: 'assets/images/portrait/small/avatar-s-25.jpg',
          date: '21/03/2020'
        }
      ]
    }
  ];

  /**
   * Get Search Data
   */
  getSearchData(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiData = this.searchData;
      this.onApiDataChange.next(this.apiData);
      resolve(this.apiData);
      // this._httpClient.get('api/search-data').subscribe((response: any) => {
      //   this.apiData = response;
      //   this.onApiDataChange.next(this.apiData);
      //   resolve(this.apiData);
      // }, reject);
    });
  }
}
