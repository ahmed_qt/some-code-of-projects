import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';

import { SearchService } from 'app/layout/components/navbar/navbar-search/search.service';
import {API} from '../../../../dentwe/services/API';
import {AppConfig} from '../../../../dentwe/services/AppConfig';
import {debounce, debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-navbar-search',
  templateUrl: './navbar-search.component.html'
})
export class NavbarSearchComponent implements OnInit {
  // Public
  public searchText = '';
  public openSearchRef = false;
  public activeIndex = 0;
  public apiData;
  public contacts = [];
  public pageSearchLimit;


  filterTextChanged: Subject<string> = new Subject<string>();


  // Decorators
  @ViewChild('openSearch') private _inputElement: ElementRef;
  @ViewChild('pageList') private _pageListElement: ElementRef;

  @HostListener('keydown.escape') fn() {
    this.removeOverlay();
    this.openSearchRef = false;
    this.searchText = '';
  }
  @HostListener('document:click', ['$event']) clickout(event) {
    if (event.target.className === 'content-overlay') {
      this.removeOverlay();
      this.openSearchRef = false;
      this.searchText = '';
    }
  }

  constructor(
    @Inject(DOCUMENT) private document,
    private _elementRef: ElementRef,
    private router: Router,
    public _searchService: SearchService,
    public config: AppConfig,
    private _api: API
  ) {}


  removeOverlay() {
    this.document.querySelector('.app-content').classList.remove('show-overlay');
  }

  toggleSearch() {
    this._searchService.onIsBookmarkOpenChange.next(false);
    this.removeOverlay();
    this.openSearchRef = !this.openSearchRef;
    this.activeIndex = 0;
    setTimeout(() => {
      this._inputElement.nativeElement.focus();
    });

    if (this.openSearchRef === false) {
      this.document.querySelector('.app-content').classList.remove('show-overlay');
      this.searchText = '';
    }
  }

  searchUpdate(event) {
    const val = event.target.value.toLowerCase();
    this.onFilterTextChanged(event.target.value.toLowerCase());
    if (val !== '') {
      this.document.querySelector('.app-content').classList.add('show-overlay');
    } else {
      this.document.querySelector('.app-content').classList.remove('show-overlay');
    }
  }

  onFilterTextChanged(filterText: string) {
    if (this.filterTextChanged.observers.length === 0) {
      this.filterTextChanged
          .pipe(debounceTime(500), distinctUntilChanged())
          .subscribe(filterQuery => {
            this._api.post('v1/users', {search: this.searchText}).subscribe(response => {
              this.apiData = response.data;
              this.contacts = this.apiData;
            });
          });
    }
    this.filterTextChanged.next(filterText);
  }

  ngOnInit(): void {
  }
}
