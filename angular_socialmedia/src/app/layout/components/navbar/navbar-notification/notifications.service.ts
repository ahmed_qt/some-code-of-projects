import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import {API} from '../../../../dentwe/services/API';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  // Public
  public apiData;
  /**
   *
   * @param {HttpClient} _httpClient
   */
  constructor(
      private _httpClient: HttpClient,
      private _api: API
  ) {
    this.getNotificationsData();
  }

  /**
   * Get Notifications Data
   */
  getNotificationsData(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      let data = {
        messages: [
          // {
          //   image: 'assets/images/illustration/notification.svg',
          //   text: 'test',
          // },
          // {
          //   image: 'assets/images/icons/speaker.svg',
          //   text: 'New message received',
          // },
          // {
          //   image: 'assets/images/portrait/small/avatar-s-12.jpg',
          //   text: 'Revised Order 👋 checkout',
          // }
        ]
      };
      this.apiData = data;
      resolve(this.apiData);
      return;
      this._api.get('v1/notifications').subscribe((response: any) => {
        let data = {
          messages: [
            // {
            //   image: 'assets/images/illustration/notification.svg',
            //   text: 'test',
            // },
            // {
            //   image: 'assets/images/icons/speaker.svg',
            //   text: 'New message received',
            // },
            // {
            //   image: 'assets/images/portrait/small/avatar-s-12.jpg',
            //   text: 'Revised Order 👋 checkout',
            // }
          ]
        };
        this.apiData = data;
        resolve(this.apiData);
      }, reject);
    });
  }
}
