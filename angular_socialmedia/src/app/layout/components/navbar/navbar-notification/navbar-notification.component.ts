import { Component, OnInit } from '@angular/core';
import { AppConfig } from 'app/dentwe/services/AppConfig';
import { AuthService } from 'app/dentwe/services/auth.service';

import { NotificationsService } from 'app/layout/components/navbar/navbar-notification/notifications.service';
import {API} from '../../../../dentwe/services/API';
import {NotificationService} from '../../../../dentwe/services/notification.service';
import {Router} from '@angular/router';
import {not} from 'rxjs/internal-compatibility';


@Component({
  selector: 'app-navbar-notification',
  templateUrl: './navbar-notification.component.html'
})
export class NavbarNotificationComponent implements OnInit {
  // Public

  public loading = false;

  constructor(
    public notification: NotificationService,
    public api: API,
    public config: AppConfig,
    private _router: Router
    ) {}

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
  }

  readAll() {
    // this.notificationsService.apiData.messages = [];
    // this._notificationsService.
  }

  openNotification() {
    this.loading = true;
    this.api.get('v1/notifications').subscribe(response => {
      this.notification.notifications = response;
      this.notification.notifications_count = 0;
      this.loading = false;
    });
  }

    navigate(notification: any) {
        switch (notification.type) {
          case 'following_request':
            this._router.navigate(['/profile/', notification.notifier.id]);
            break;
          case 'follow_request':
            this._router.navigate(['/profile/', notification.notifier.id]);
            break;
          case 'following':
            this._router.navigate(['/profile/', notification.notifier.id]);
            break;
          case 'view_profile':
            this._router.navigate(['/profile/', notification.notifier.id]);
            break;
          case 'profile_post':
            this._router.navigate(['/post/', notification.post_id]);
            break;
          case 'post_comment':
            this._router.navigate(['/post/', notification.comment.post_id], {queryParams: {comment_id: notification.comment_id}});
            break;
          case 'comment_reply':
            this._router.navigate(['/post/', notification.reply.comment.post_id], {queryParams: {reply_id: notification.reply_id}});
            break;
          case 'post_reaction':
            this._router.navigate(['/post/', notification.post_id]);
            break;
          case 'comment_reaction':
            this._router.navigate(['/post/', notification.comment.post_id], {queryParams: {comment_id: notification.comment_id}});
            break;
          case 'reply_reaction':
            this._router.navigate(['/post/', notification.reply.comment.post_id], {queryParams: {reply_id: notification.reply_id}});
            break;
        }
    }
}
