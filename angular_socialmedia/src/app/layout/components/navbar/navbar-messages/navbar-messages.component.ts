import { Component, OnInit } from '@angular/core';
import { AppConfig } from 'app/dentwe/services/AppConfig';
import { AuthService } from 'app/dentwe/services/auth.service';

import { NotificationsService } from 'app/layout/components/navbar/navbar-notification/notifications.service';
import {API} from '../../../../dentwe/services/API';
import {FollowService} from '../../../../dentwe/services/follow.service';
import {ContactService} from '../../../../dentwe/services/contact.service';

@Component({
  selector: 'app-navbar-messages',
  templateUrl: './navbar-messages.component.html'
})
export class NavbarMessagesComponent implements OnInit {
  // Public

  public loading = false;
  public acceptLoading = false;
  public denyLoading = false;

  constructor(
      public auth: AuthService,
      public api: API,
      public config: AppConfig,
      public contact: ContactService,
  ) {
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
  }

  readAll() {
    // this.notificationsService.apiData.messages = [];
    // this._notificationsService.
  }

  openNotification() {

  }

}
