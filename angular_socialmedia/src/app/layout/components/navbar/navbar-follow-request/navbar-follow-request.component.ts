import { Component, OnInit } from '@angular/core';
import { AppConfig } from 'app/dentwe/services/AppConfig';
import { AuthService } from 'app/dentwe/services/auth.service';

import { NotificationsService } from 'app/layout/components/navbar/navbar-notification/notifications.service';
import {API} from '../../../../dentwe/services/API';
import {FollowService} from '../../../../dentwe/services/follow.service';

@Component({
  selector: 'app-navbar-follow-request',
  templateUrl: './navbar-follow-request.component.html'
})
export class NavbarFollowRequestComponent implements OnInit {
  // Public

  public loading = false;
  public acceptLoading = false;
  public denyLoading = false;

  constructor(
    public auth: AuthService,
    public api: API,
    public config: AppConfig,
    public follow: FollowService,
  ) {}

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
  }

  readAll() {
    // this.notificationsService.apiData.messages = [];
    // this._notificationsService.
  }

  openNotification() {
    this.loading = true;
    this.api.get('v1/followrequests').subscribe(response => {
      this.follow.followRequests = response;
      this.loading = false;
    });
  }

  acceptRequest(request_id) {
    this.acceptLoading = true;
    this.api.post('v1/followrequest/accept', {request_id}).subscribe(response => {
      this.follow.followRequests = response;
      this.acceptLoading = false;
      this.follow.followrequests_count = response;
      this.openNotification();
    }, error => {
      this.acceptLoading = false;
    });
  }

  denyRequest(request_id) {
    this.denyLoading = true;
    this.api.post('v1/followrequest/deny', {request_id}).subscribe(response => {
      this.follow.followRequests = response;
      this.denyLoading = false;
      this.follow.followrequests_count = response;
      this.openNotification();
    }, error => {
      this.denyLoading = false;
    });
  }
}
