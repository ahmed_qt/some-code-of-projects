import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {
    PerfectScrollbarConfigInterface,
    PerfectScrollbarModule,
    PERFECT_SCROLLBAR_CONFIG
} from 'ngx-perfect-scrollbar';

import {CoreCommonModule} from '@core/common.module';
import {CoreTouchspinModule} from '@core/components/core-touchspin/core-touchspin.module';

import {NavbarComponent} from 'app/layout/components/navbar/navbar.component';
import {NavbarSearchComponent} from 'app/layout/components/navbar/navbar-search/navbar-search.component';

import {NavbarNotificationComponent} from 'app/layout/components/navbar/navbar-notification/navbar-notification.component';
import {TimeagoModule} from 'ngx-timeago';
import {TimeagoClock} from 'ngx-timeago';
import {Observable, interval} from 'rxjs';
import {NavbarFollowRequestComponent} from './navbar-follow-request/navbar-follow-request.component';
import {Clock, UserCheck, UserPlus, UserX} from 'angular-feather/icons';
import {FeatherModule} from 'angular-feather';
import {NavbarMessagesComponent} from './navbar-messages/navbar-messages.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
    wheelPropagation: false
};

// ticks every 30s
export class MyClock extends TimeagoClock {
    tick(then: number): Observable<number> {
        return interval(30000);
    }
}

const icons = {
    Clock,
    UserX,
    UserPlus,
    UserCheck
};

@NgModule({
    declarations: [NavbarMessagesComponent, NavbarComponent, NavbarSearchComponent, NavbarNotificationComponent,
        NavbarFollowRequestComponent],
    imports: [
        FeatherModule.pick(icons),
        RouterModule, NgbModule, CoreCommonModule, PerfectScrollbarModule, CoreTouchspinModule
        , TimeagoModule.forRoot(
            {
                clock: {provide: TimeagoClock, useClass: MyClock},
            }
        )
    ],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ],
    exports: [NavbarComponent]
})
export class NavbarModule {
}
