import requests
from requests.models import Response

HEADERS = {
    'content-type':'application/json',
}

class EarthlinkClient():
    def __init__(self):
        self.base_url = 'https://rapi.earthlink.iq/api/reseller/'
        self.session = requests.Session()
        self.session.headers = HEADERS

    def get(self, route) -> Response:
        url = self.base_url + route
        response = self.session.get(url)
        return response

    def post(self, route, data) -> Response:
        url = self.base_url + route
        response = self.session.post(url, json=data)
        return response