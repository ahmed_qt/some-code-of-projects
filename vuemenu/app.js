new Vue({
    el: '#menu',
    data: {
        mobileNavOpen: false,
        hovered: false,
        activeMenu: null,
        showMegaMenu: false,
        megaMenuData: null,
        activeMegaMenuItemsItem: null,
        tabdata: null,
        logo: {
            src: 'logo.png',
            href: '/',
            width: 105,
            alt: ''
        },
        items: [
            {
                text: 'Services',
                mobile: false,
                type: 'simple-menu',
                hasIcon: true,
                hasChildren: true,
                children: [
                    {
                        text: 'service 1',
                        href: '#s1',
                    },
                    {
                        text: 'service 2',
                        href: '#s2',
                    },
                ]
            },
            {
                text: 'Products',
                mobile: false,
                type: 'simple-menu',
                hasIcon: true,
                hasChildren: true,
                children: [
                    {
                        text: 'product 1',
                        href: '#p1',
                    },
                    {
                        text: 'product 2',
                        href: '#p2',
                    },
                ]
            },
            {
                text: 'Mega',
                href: '#link',
                mobile: false,
                type: 'mega-menu',
                hasIcon: true,
                hasChildren: false,
                megaMenuData: {
                    items: [
                        {
                            text: 'SAS',
                            href: '#sas',
                            data: null
                        },
                        {
                            text: 'SAS Redious',
                            href: '#sas',
                            data: null
                        },
                        {
                            text: 'Share Box',
                            href: '#sas',
                            data: null
                        },
                        {
                            text: 'Media Vault',
                            href: '#sas',
                            data: null
                        },
                    ]
                }
            },
            {
                text: 'Mega 2',
                href: '#link',
                mobile: false,
                type: 'mega-menu',
                hasIcon: true,
                hasChildren: false,
                megaMenuData: {
                    items: [
                        {
                            text: 'baka',
                            href: '#baka',
                            data: {
                                title: {
                                    text: 'Baka!',
                                    href: '#'
                                },
                                left: [
                                    {
                                        text: 'Quicklinks',
                                        type: 'text',
                                        bold: false,
                                    },
                                    {
                                        text: 'Link 1',
                                        type: 'link',
                                        href: '#'
                                    },
                                    {
                                        text: 'Link 2',
                                        type: 'link',
                                        href: '#'
                                    },
                                ],
                                right: [
                                    {
                                        text: 'Case study: Unilever Europe',
                                        type: 'text',
                                        bold: true,
                                    },
                                    {
                                        src: 'https://www.ibm.com/images/v18/busconsultingfc.jpg',
                                        alt: 'image alt',
                                        type: 'image',
                                    },
                                    {
                                        text: 'Link 3',
                                        type: 'link',
                                        href: '#'
                                    },
                                ]
                            },
                        },
                        {
                            text: 'baka 2',
                            href: '#baka',
                            data: {
                                title: {
                                    text: 'Baka 2!',
                                    href: '#'
                                },
                                left: [
                                ],
                                right: [
                                    {
                                        src: 'https://www.ibm.com/images/v18/analyticsfc.jpg',
                                        alt: 'image alt',
                                        type: 'image',
                                    },
                                ]
                            },
                        }
                    ]
                }
            },
            {
                text: 'Support (US)',
                href: '#link',
                mobile: false,
                type: 'link',
                hasIcon: false,
                hasChildren: false,
            },
            {
                text: 'Careers (US)',
                href: '#link',
                mobile: false,
                type: 'link',
                hasIcon: false,
                hasChildren: false,
            }
        ],
        mobileMenuItems: [
            {
                text: 'item 1',
                href: '#'
            },
            {
                text: 'item 2',
                href: '#'
            },
            {
                text: 'item 3',
                href: '#'
            },
            {
                text: 'item 4',
                href: '#'
            },
            {
                text: 'item 5',
                href: '#'
            },
        ],
    },
    methods: {
        megamenuLeave() {
            this.hovered = false;
            setTimeout(() => {
                if (!this.hovered) {
                    this.showMegaMenu = false;
                    this.activeMenu = null;

                }
            }, 800);
        }
    }
})